import { IObjetVM } from './objet';
import { SortilegeVM } from './sortilege';

export interface IPersonnage {
    Id: number,
    Nom: string,
    Pv: number,
    Attaque: number,
    Defense: number,
    Dialogue: string,
    Type: number,
    Probabilite: number,
    ObjetIds: number[],
    Objets: IObjetVM[],
    ObjetsBarre: IObjetVM[],
    Sortileges: SortilegeVM[],
    SortilegesBarre: SortilegeVM[],
    ChapitreIds: number[],
    SortilegeIds: number[]
}

export interface IPersonnageVM {
    Id: number,
    Nom: string
}
