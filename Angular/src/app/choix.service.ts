import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IChoix } from './choix';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChoixService {

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    private choixUrl = 'https://isimafia.azurewebsites.net/api/Choix';

    getChoixs(): Observable<IChoix[]> {
        return this.http.get<IChoix[]>(this.choixUrl)
    }

    getChoix(id: number): Observable<IChoix> {
        const url = `${this.choixUrl}/${id}`;
        return this.http.get<IChoix>(url);
    }

    updateChoix(choix: unknown, id: number): Observable<any> {
        const url = `${this.choixUrl}/${id}`;
        return this.http.put(url, choix, this.httpOptions);
    }

    addChoix(choix: unknown): Observable<IChoix> {
        return this.http.post<IChoix>(this.choixUrl, choix, this.httpOptions);
    }

    deleteChoix(choix: IChoix | number): Observable<IChoix> {
        const id = typeof choix === 'number' ? choix : choix.Id;
        const url = `${this.choixUrl}/${id}`;

        return this.http.delete<IChoix>(url, this.httpOptions);
    }

    constructor(private http: HttpClient) { }
}
