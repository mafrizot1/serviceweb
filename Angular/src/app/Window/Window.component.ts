import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Objet, IObjet } from 'src/app/objet';
import { IJoueur } from '../joueur';
import { IChapitre } from '../chapitre';
import { GameService } from '../game.service';
import { ChapitreService } from '../chapitre.service'
import { IEvenement } from '../evenement';
import { Sortilege } from '../sortilege';
import { IChoixVM } from '../choix';
import { IPersonnage } from '../personnage';
import { IIndice } from '../indice';


@Component({
	selector: 'app-Window',
	templateUrl: './Window.component.html',
	styleUrls: ['./Window.component.scss']
})

export class WindowComponent implements OnInit {
	//pictures = pics;
	objets = [];
	//inventaire = [];
	evenements : IEvenement[];
  sortileges: Sortilege[];
  choix: IChoixVM[];
  histoire: string;
    message = '';
    monstre: IPersonnage;
    inventaire: IObjet[];
    PV: number;
    Attaque: number;
    Defense: number;
  joueur: IJoueur;
    chapitre: IChapitre;
    numChapitre: number;
    indices: IIndice[];
    Victoire: any;
    GameOver: any;
	private loaded: Promise<boolean>;

	constructor(
		private route: ActivatedRoute,
		private gameService: GameService
	) {	}
	
	getJoueur(): void {
      this.gameService.continueGame(this.route.snapshot.paramMap.get('pseudo')).subscribe(game => {
          this.joueur = game.joueur;
			this.chapitre = game.chapitreVM;
      this.evenements = new Array;
          for (let evenement of game.chapitreVM.Evenements) {
              var i = 0;
              for (let evenement2 of game.joueur.Evenements) {
                  if (evenement.Id == evenement2.Id) {
                      i = 1;
                  }
              }
              if (i == 0) {
                  this.evenements.push(evenement);
              }
          }
			this.sortileges = game.joueur.Sortileges;
      this.histoire = game.chapitreVM.Histoire;
          this.choix = game.chapitreVM.Choix;
          this.monstre = game.monstre;
          this.inventaire = game.joueur.Objets;
          this.PV = game.joueur.PV;
          this.Attaque = game.joueur.Attaque;
          this.Defense = game.joueur.Defense;
          this.numChapitre = game.chapitreVM.Numero;
          this.indices = game.joueur.Indices;
          this.Victoire = game.Victoire;
          this.GameOver = game.GameOver;
          console.log(game.Victoire);
			//this.loaded = Promise.resolve(true);
			//location.reload();
		});
   }

    clickEvent(eventid: number, type: string): void {
        this.gameService.evenementGame(this.joueur.Id, eventid)
            .subscribe(game => {
                this.getJoueur();
                this.message = `Vous avez trouvé un ${type}`
                //this.loaded = Promise.resolve(true);
                //location.reload();
            })
    }

    clickChoix(choixid: number): void {
        this.gameService.choixGame(this.joueur.Id, choixid)
            .subscribe(game => {
                this.message = '';
                this.getJoueur();//Met à jour la page sans la recharger
                //this.loaded = Promise.resolve(true);
                //location.reload();
            })
    }

    attaquerSort(sortid: number, monstre: IPersonnage): void {
        this.gameService.attaquerSort(this.joueur.Id, sortid, monstre)
            .subscribe(game => {
                this.message = game.message;
                this.joueur = game.joueur;
                this.chapitre = game.chapitreVM;
                this.evenements = new Array;
                for (let evenement of game.chapitreVM.Evenements) {
                    var i = 0;
                    for (let evenement2 of game.joueur.Evenements) {
                        if (evenement.Id == evenement2.Id) {
                            i = 1;
                        }
                    }
                    if (i == 0) {
                        this.evenements.push(evenement);
                    }
                }
                this.sortileges = game.joueur.Sortileges;
                this.histoire = game.chapitreVM.Histoire;
                this.choix = game.chapitreVM.Choix;
                this.monstre = game.monstre;
                this.inventaire = game.joueur.Objets;
                this.PV = game.joueur.PV;
                this.Attaque = game.joueur.Attaque;
                this.Defense = game.joueur.Defense;
                this.numChapitre = game.chapitreVM.Numero;
                this.indices = game.joueur.Indices;
                this.Victoire = game.Victoire;
                this.GameOver = game.GameOver;
                //this.loaded = Promise.resolve(true);
                //location.reload();
            })
    }

    attaquerObjet(objetid: number, monstre: IPersonnage): void {
        this.gameService.attaquerObjet(this.joueur.Id, objetid, monstre)
            .subscribe(game => {
                this.message = game.message;
                this.joueur = game.joueur;
                this.chapitre = game.chapitreVM;
                this.evenements = new Array;
                for (let evenement of game.chapitreVM.Evenements) {
                    var i = 0;
                    for (let evenement2 of game.joueur.Evenements) {
                        if (evenement.Id == evenement2.Id) {
                            i = 1;
                        }
                    }
                    if (i == 0) {
                        this.evenements.push(evenement);
                    }
                }
                this.sortileges = game.joueur.Sortileges;
                this.histoire = game.chapitreVM.Histoire;
                this.choix = game.chapitreVM.Choix;
                this.monstre = game.monstre;
                this.inventaire = game.joueur.Objets;
                this.PV = game.joueur.PV;
                this.Attaque = game.joueur.Attaque;
                this.Defense = game.joueur.Defense;
                this.numChapitre = game.chapitreVM.Numero;
                this.indices = game.joueur.Indices;
                this.Victoire = game.Victoire;
                this.GameOver = game.GameOver;
                //this.loaded = Promise.resolve(true);
                //location.reload();
            })
    }

    attaquer(monstre: IPersonnage): void {
        this.gameService.attaquer(this.joueur.Id, monstre)
            .subscribe(game => {
                this.message = game.message;
                this.joueur = game.joueur;
                this.chapitre = game.chapitreVM;
                this.evenements = new Array;
                for (let evenement of game.chapitreVM.Evenements) {
                    var i = 0;
                    for (let evenement2 of game.joueur.Evenements) {
                        if (evenement.Id == evenement2.Id) {
                            i = 1;
                        }
                    }
                    if (i == 0) {
                        this.evenements.push(evenement);
                    }
                }
                this.sortileges = game.joueur.Sortileges;
                this.histoire = game.chapitreVM.Histoire;
                this.choix = game.chapitreVM.Choix;
                this.monstre = game.monstre;
                this.inventaire = game.joueur.Objets;
                this.PV = game.joueur.PV;
                this.Attaque = game.joueur.Attaque;
                this.Defense = game.joueur.Defense;
                this.numChapitre = game.chapitreVM.Numero;
                this.indices = game.joueur.Indices;
                this.Victoire = game.Victoire;
                this.GameOver = game.GameOver;
                //this.loaded = Promise.resolve(true);
                //location.reload();
            })
    }

	// A chaque rechargement
	ngOnInit() {
		this.getJoueur();
	}
}
