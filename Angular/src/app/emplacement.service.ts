import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IEmplacement } from './emplacement';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class EmplacementService {

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    private emplacementUrl = 'https://isimafia.azurewebsites.net/api/Emplacement';

    getEmplacements(): Observable<IEmplacement[]> {
        return this.http.get<IEmplacement[]>(this.emplacementUrl)
    }

    getEmplacement(id: number): Observable<IEmplacement> {
        const url = `${this.emplacementUrl}/${id}`;
        return this.http.get<IEmplacement>(url);
    }

    updateEmplacement(emplacement: unknown, id: number): Observable<any> {
        const url = `${this.emplacementUrl}/${id}`;
        return this.http.put(url, emplacement, this.httpOptions);
    }

    addEmplacement(emplacement: unknown): Observable<IEmplacement> {
        return this.http.post<IEmplacement>(this.emplacementUrl, emplacement, this.httpOptions);
    }

    deleteEmplacement(emplacement: IEmplacement | number): Observable<IEmplacement> {
        const id = typeof emplacement === 'number' ? emplacement : emplacement.Id;
        const url = `${this.emplacementUrl}/${id}`;

        return this.http.delete<IEmplacement>(url, this.httpOptions);
    }

    constructor(private http: HttpClient) { }
}
