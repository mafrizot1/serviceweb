import { Component, OnInit, Input } from '@angular/core';
import { IIndice } from '../indice';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { IndiceService } from '../indice.service';

@Component({
    selector: 'app-admindice-detail',
    templateUrl: './admindice-detail.component.html',
    styleUrls: ['./admindice-detail.component.scss']
})
export class AdmindiceDetailComponent implements OnInit {

    @Input() indice: IIndice;

    constructor(
        private route: ActivatedRoute,
        private indiceService: IndiceService,
        private location: Location
    ) { }

    getIndice(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.indiceService.getIndice(id)
            .subscribe(indice => this.indice = indice);
    }

    save(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.indiceService.updateIndice(this.indice, id)
            .subscribe(() => this.goBack());
    }

    ngOnInit() {
        this.getIndice();
    }

    delete(indice: IIndice): void {
        this.indiceService.deleteIndice(indice)
            .subscribe(() => this.goBack());
    }

    goBack(): void {
        this.location.back();
    }

}

