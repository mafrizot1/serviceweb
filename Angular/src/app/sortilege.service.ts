import { Injectable } from '@angular/core';
import { Sortilege } from './sortilege';
import { SORTILEGES } from './mock-admsortileges';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SortilegeService {

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    private sortilegesUrl = 'https://isimafia.azurewebsites.net/api/Sortilege'; //mettre '/assets/sortileges.json' pour des tests  en local avec un fichier .json

    getSortileges(): Observable<Sortilege[]> {
        return this.http.get<Sortilege[]>(this.sortilegesUrl)
    }

    getSortilege(id: number): Observable<Sortilege> {
        const url = `${this.sortilegesUrl}/${id}`;
        return this.http.get<Sortilege>(url);
        //return of(SORTILEGES.find(sortilege => sortilege.Id === id));
    }

    updateSortilege(sortilege: any, id: number): Observable<any> {
        const url = `${this.sortilegesUrl}/${id}`;
        return this.http.put(url, sortilege, this.httpOptions);
    }

    addSortilege(sortilege: unknown): Observable<Sortilege> {
        return this.http.post<Sortilege>(this.sortilegesUrl, sortilege, this.httpOptions);
    }

    deleteSortilege(sortilege: Sortilege | number): Observable<Sortilege> {
        const id = typeof sortilege === 'number' ? sortilege : sortilege.Id;
        const url = `${this.sortilegesUrl}/${id}`;

        return this.http.delete<Sortilege>(url, this.httpOptions);
    }

    constructor(private http: HttpClient) { }
}
