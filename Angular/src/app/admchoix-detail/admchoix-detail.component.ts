import { Component, OnInit } from '@angular/core';
import { IChapitre } from '../chapitre';
import { ChapitreService } from '../chapitre.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { IChoix } from '../choix'
import { ChoixService } from '../choix.service'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-admchoix-detail',
  templateUrl: './admchoix-detail.component.html',
  styleUrls: ['./admchoix-detail.component.scss']
})
export class AdmchoixDetailComponent implements OnInit {

    choix: IChoix;
    chapitres: IChapitre[];
    selectedChapitre = null;
    selectedChapitrePrec = null;
    dropdownSettings: IDropdownSettings;

    constructor(private chapitreService: ChapitreService, private choixService: ChoixService, private location: Location, private route: ActivatedRoute) { }

    getChoix(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.choixService.getChoix(id)
            .subscribe(choix => {
                this.choix = choix;
                this.selectedChapitre = new Array;
                this.selectedChapitre.push(choix.ProchainChapitre);
                this.selectedChapitrePrec = new Array;
                this.selectedChapitrePrec.push(choix.ChapitrePrec);
            });
    }

    getChapitres(): void {
        this.chapitreService.getChapitres()
            .subscribe(chapitres => this.chapitres = chapitres);
    }

    nullChapP(): void {
        this.selectedChapitrePrec = null;
    }

    nullChap(): void {
        this.selectedChapitre = null;
    }

    save(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        var choix = { Contenu: this.choix.Contenu, ChapitrePrecId: this.selectedChapitrePrec[0].Id, ChapitreId: this.selectedChapitre[0].Id };
        this.choixService.updateChoix(choix, id)
            .subscribe(() => this.goBack());
    }

    delete(choix: IChoix): void {
        this.choixService.deleteChoix(choix)
            .subscribe(() => this.goBack());
    }

    ngOnInit() {
        this.getChapitres();
        this.getChoix();
        this.dropdownSettings = {
            singleSelection: true,
            idField: 'Id',
            textField: 'Numero',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
    }

    goBack(): void {
        this.location.back();
    }

}
