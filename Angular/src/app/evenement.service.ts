import { Injectable } from '@angular/core';
import { IEvenement } from './evenement';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EvenementService {

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    private evenementsUrl = 'https://isimafia.azurewebsites.net/api/Evenement';

    getEvenements(): Observable<IEvenement[]> {
        return this.http.get<IEvenement[]>(this.evenementsUrl)
    }

    getEvenement(id: number): Observable<IEvenement> {
        const url = `${this.evenementsUrl}/${id}`;
        return this.http.get<IEvenement>(url);
    }

    updateEvenement(evenement: unknown, id: number): Observable<any> {
        const url = `${this.evenementsUrl}/${id}`;
        return this.http.put(url, evenement, this.httpOptions);
    }

    addEvenement(evenement: unknown): Observable<IEvenement> {
        return this.http.post<IEvenement>(this.evenementsUrl, evenement, this.httpOptions);
    }

    deleteEvenement(evenement: IEvenement | number): Observable<IEvenement> {
        const id = typeof evenement === 'number' ? evenement : evenement.Id;
        const url = `${this.evenementsUrl}/${id}`;

        return this.http.delete<IEvenement>(url, this.httpOptions);
    }

    constructor(private http: HttpClient) { }
}
