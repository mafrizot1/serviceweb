import { Component, OnInit } from '@angular/core';
import { IPersonnage } from '../personnage';
import { IObjet } from '../objet';
import { Sortilege } from '../sortilege';
import { IChapitre } from '../chapitre';
import { IEvenement } from '../evenement';
import { IEmplacement } from '../emplacement';
import { PersonnageService } from '../personnage.service';
import { ObjetService } from '../objet.service';
import { SortilegeService } from '../sortilege.service';
import { EvenementService } from '../evenement.service';
import { ChapitreService } from '../chapitre.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { IIndice } from '../indice';
import { IndiceService } from '../indice.service';

@Component({
  selector: 'app-admevenements',
  templateUrl: './admevenements.component.html',
  styleUrls: ['./admevenements.component.scss']
})
export class AdmevenementsComponent implements OnInit {

    evenements: IEvenement[];
    objets: IObjet[];
    emplacements: IEmplacement[];
    chapitres: IChapitre[];
    sortileges: Sortilege[];
    monstres: IPersonnage[];
    indices: IIndice[];
    selectedObjet = null;
    selectedSortilege = null;
    selectedMonstre = null;
    selectedIndice = null;
    emplacementH: number;
    emplacementL: number;
    emplacementX: number;
    emplacementY: number;
    selectedChapitre: IChapitre;
    dropdownSettings1: IDropdownSettings;
    dropdownSettings2: IDropdownSettings;
    dropdownSettings3: IDropdownSettings;

    constructor(private personnageService: PersonnageService,
        private objetService: ObjetService,
        private sortilegeService: SortilegeService,
        private evenementService: EvenementService,
        private chapitreService: ChapitreService,
        private indiceService: IndiceService
    ) { }

    getObjets(): void {
        this.objetService.getObjets()
            .subscribe(objets => this.objets = objets);
    }

    getSortileges(): void {
        this.sortilegeService.getSortileges()
            .subscribe(sortileges => this.sortileges = sortileges);
    }

    getChapitres(): void {
        this.chapitreService.getChapitres()
            .subscribe(chapitres => this.chapitres = chapitres);
    }

    getMonstres(): void {
        this.personnageService.getMonstres()
            .subscribe(personnages => this.monstres = personnages);
    }

    getIndices(): void {
        this.indiceService.getIndices()
            .subscribe(indices => { this.indices = indices; console.log(this.indices); });
    }

    getEvenements(): void {
        this.evenementService.getEvenements()
            .subscribe(evenements => this.evenements = evenements);
    }

    

    add(desc: string): void {
        desc = desc.trim();
        if (!desc) { return; }
        var event = {};
        if (this.selectedObjet) {
            event = {
                Description: desc, Emplacement: { Hauteur: this.emplacementH, Largeur: this.emplacementL, X: this.emplacementX, Y: this.emplacementY }, Chapitre_Id: this.selectedChapitre[0].Id, Objet_Id: this.selectedObjet[0].Id
            };
        } else if (this.selectedMonstre) {
            event = { Description: desc, Emplacement: { Hauteur: this.emplacementH, Largeur: this.emplacementL, X: this.emplacementX, Y: this.emplacementY }, Chapitre_Id: this.selectedChapitre[0].Id, Monstre_Id: this.selectedMonstre[0].Id };
        } else if (this.selectedSortilege) {
            event = { Description: desc, Emplacement: { Hauteur: this.emplacementH, Largeur: this.emplacementL, X: this.emplacementX, Y: this.emplacementY }, Chapitre_Id: this.selectedChapitre[0].Id, Sortilege_Id: this.selectedSortilege[0].Id };
        } else if (this.selectedIndice) {
            event = { Description: desc, Emplacement: { Hauteur: this.emplacementH, Largeur: this.emplacementL, X: this.emplacementX, Y: this.emplacementY }, Chapitre_Id: this.selectedChapitre[0].Id, Indice_Id: this.selectedIndice[0].Id };
        }
            this.evenementService.addEvenement(event)
            .subscribe(evenement => {
                location.reload();
            });
    }

    nullChap(): void {
        this.selectedChapitre = null;
    }

    nullAll(): void {
        this.selectedObjet = null;
        this.selectedSortilege = null;
        this.selectedMonstre = null;
        this.selectedIndice = null;
    }

    ngOnInit() {
        this.getIndices();
        this.getMonstres();
        this.getObjets();
        this.getSortileges();
        this.getChapitres();
        this.getEvenements();
        this.dropdownSettings1 = {
            singleSelection: true,
            idField: 'Id',
            textField: 'Nom',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
        this.dropdownSettings2 = {
            singleSelection: true,
            idField: 'Id',
            textField: 'Numero',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
        this.dropdownSettings3 = {
            singleSelection: true,
            idField: 'Id',
            textField: 'Id',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
    }

}
