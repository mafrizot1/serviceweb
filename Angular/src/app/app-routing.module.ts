import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdmsortilegesComponent } from './admsortileges/admsortileges.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdmsortilegeDetailComponent } from './admsortilege-detail/admsortilege-detail.component';
import { WindowComponent } from './Window/Window.component';
import { AdmobjetsComponent } from './admobjets/admobjets.component';
import { AdmpersonnagesComponent } from './admpersonnages/admpersonnages.component';
import { AdmobjetsDetailComponent } from './admobjets-detail/admobjets-detail.component';
import { AdmpersonnageDetailComponent } from './admpersonnage-detail/admpersonnage-detail.component';
import { AdmchapitresComponent } from './admchapitres/admchapitres.component';
import { AdmchapitreDetailComponent } from './admchapitre-detail/admchapitre-detail.component';
import { AdmevenementsComponent } from './admevenements/admevenements.component';
import { AdmevenementDetailComponent } from './admevenement-detail/admevenement-detail.component';
import { AdmchoixComponent } from './admchoix/admchoix.component';
import { AdmchoixDetailComponent } from './admchoix-detail/admchoix-detail.component';
import { AdmemplacementsComponent } from './admemplacements/admemplacements.component';
import { AdmemplacementDetailComponent } from './admemplacement-detail/admemplacement-detail.component';
import { AdmindicesComponent } from './admindices/admindices.component';
import { AdmindiceDetailComponent } from './admindice-detail/admindice-detail.component';
import { GameComponent } from './game/game.component';
import { AdmjoueursComponent } from './admjoueurs/admjoueurs.component';


const routes: Routes = [
    { path: 'sortileges', component: AdmsortilegesComponent },
    { path: 'sortileges/detail/:id', component: AdmsortilegeDetailComponent },
    { path: 'objets/detail/:id', component: AdmobjetsDetailComponent },
    { path: 'personnages/detail/:id', component: AdmpersonnageDetailComponent },
    { path: 'window/:pseudo', component: WindowComponent },
    { path: 'objets', component: AdmobjetsComponent },
    { path: 'personnages', component: AdmpersonnagesComponent },
    { path: '', redirectTo: '/game', pathMatch: 'full' },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'chapitres', component: AdmchapitresComponent },
    { path: 'evenements', component: AdmevenementsComponent },
    { path: 'chapitres/detail/:id', component: AdmchapitreDetailComponent },
    { path: 'evenements/detail/:id', component: AdmevenementDetailComponent },
    { path: 'choix', component: AdmchoixComponent },
    { path: 'choix/detail/:id', component: AdmchoixDetailComponent },
    { path: 'emplacements', component: AdmemplacementsComponent },
    { path: 'emplacements/detail/:id', component: AdmemplacementDetailComponent },
    { path: 'indices', component: AdmindicesComponent },
    { path: 'joueurs', component: AdmjoueursComponent },
    { path: 'indices/detail/:id', component: AdmindiceDetailComponent },
    { path: 'game', component: GameComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
