import { Injectable } from '@angular/core';
import { IPersonnage } from './personnage';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PersonnageService {

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    private personnagesUrl = 'https://isimafia.azurewebsites.net/api/Personnage';
    private monstresUrl = 'https://isimafia.azurewebsites.net/api/Personnage';

    getPersonnages(): Observable<IPersonnage[]> {
        return this.http.get<IPersonnage[]>(this.personnagesUrl)
    }

    getPersonnage(id: number): Observable<IPersonnage> {
        const url = `${this.personnagesUrl}/${id}`;
        return this.http.get<IPersonnage>(url);
    }

    getMonstres(): Observable<IPersonnage[]> {
        return this.http.get<IPersonnage[]>(this.monstresUrl)
    }

    updatePersonnage(personnage: unknown, id: number): Observable<any> {
        const url = `${this.personnagesUrl}/${id}`;
        return this.http.put(url, personnage, this.httpOptions);
    }

    addPersonnage(personnage: unknown): Observable<IPersonnage> {
        return this.http.post<IPersonnage>(this.personnagesUrl, personnage, this.httpOptions);
    }

    deletePersonnage(personnage: IPersonnage | number): Observable<IPersonnage> {
        const id = typeof personnage === 'number' ? personnage : personnage.Id;
        const url = `${this.personnagesUrl}/${id}`;

        return this.http.delete<IPersonnage>(url, this.httpOptions);
    }

    constructor(private http: HttpClient) { }
}
