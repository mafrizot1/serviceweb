export class Objet {
    private Name: string;
    private URL: string;
    private Height: any;
    private Width: any;

    constructor(Name: string, URL: string, Height: any, Width: any) {
        this.Name = Name || "image";
        this.URL = URL || "";
        this.Height = Height || 0;
        this.Width = Width || 0; 
    }
}

export interface IObjet {
    Id: number,
    Nom: string,
    Description: string,
    Url: string,
    Puissance: number,
    Type: string
}

export interface IObjetVM {
    Id: number,
    Nom : string
}

