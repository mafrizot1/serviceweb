import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgxTrimDirectiveModule } from 'ngx-trim-directive';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WindowComponent } from './Window/Window.component';
import { FormsModule } from '@angular/forms';
import { ObjectComponent } from './object/object.component';
import { AdmsortilegesComponent } from './admsortileges/admsortileges.component';
import { AdmsortilegeDetailComponent } from './admsortilege-detail/admsortilege-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdmobjetsComponent } from './admobjets/admobjets.component';
import { AdmobjetsDetailComponent } from './admobjets-detail/admobjets-detail.component';
import { AdmpersonnagesComponent } from './admpersonnages/admpersonnages.component';
import { AdmpersonnageDetailComponent } from './admpersonnage-detail/admpersonnage-detail.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AdmchapitresComponent } from './admchapitres/admchapitres.component';
import { AdmchapitreDetailComponent } from './admchapitre-detail/admchapitre-detail.component';
import { AdmevenementsComponent } from './admevenements/admevenements.component';
import { AdmevenementDetailComponent } from './admevenement-detail/admevenement-detail.component';
import { AdmchoixComponent } from './admchoix/admchoix.component';
import { AdmchoixDetailComponent } from './admchoix-detail/admchoix-detail.component';
import { AdmemplacementsComponent } from './admemplacements/admemplacements.component';
import { AdmemplacementDetailComponent } from './admemplacement-detail/admemplacement-detail.component';
import { AdmindicesComponent } from './admindices/admindices.component';
import { AdmindiceDetailComponent } from './admindice-detail/admindice-detail.component';
import { GameComponent } from './game/game.component';
import { AdmjoueursComponent } from './admjoueurs/admjoueurs.component';

@NgModule({
  declarations: [ AppComponent, WindowComponent, ObjectComponent, AdmsortilegesComponent, AdmsortilegeDetailComponent, DashboardComponent, AdmobjetsComponent, AdmobjetsDetailComponent, AdmpersonnagesComponent, AdmpersonnageDetailComponent, AdmchapitresComponent, AdmchapitreDetailComponent, AdmevenementsComponent, AdmevenementDetailComponent, AdmchoixComponent, AdmchoixDetailComponent, AdmemplacementsComponent, AdmemplacementDetailComponent, AdmindicesComponent, AdmindiceDetailComponent, GameComponent, AdmjoueursComponent],
    imports: [BrowserModule, AppRoutingModule, HttpClientModule, NgxTrimDirectiveModule, FormsModule, NgMultiSelectDropDownModule.forRoot()],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
