import { IChapitre } from './chapitre';

export interface IChoix {
    ChapitreId: number,
    ProchainChapitre: IChapitre,
    ChapitrePrec: IChapitre,
    ChapitrePrecId: number,
    Id: number,
    Contenu: string
}

export interface IChoixVM {
    Id: number,
    Contenu: string
}
