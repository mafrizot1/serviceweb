import { Component, OnInit, Input } from '@angular/core';
import { IJoueur } from '../joueur';
import { GameService } from '../game.service';
import { JoueurService } from '../joueur.service';

import { Injectable } from '@angular/core';
import { IGame } from '../game';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { IObjet } from '../objet';
import { IChapitre } from '../chapitre';

@Component({
	selector: 'app-game',
	templateUrl: './game.component.html',
	styleUrls: ['./game.component.scss']
})

export class GameComponent implements OnInit {
	joueur: IJoueur;
	chapitre: IChapitre;
	alert: string;
	
	constructor(
        private gameService: GameService,
        private joueurService: JoueurService,
		private http: HttpClient
	) {}

	lancerJeu(joueurNom: string): void {
		joueurNom = joueurNom.trim();
		if (joueurNom) {
            this.joueurService.addJoueur({ Nom: joueurNom.trim() }).subscribe(
				objet => { 
					location.replace("/window/" + joueurNom.trim()); 
				},
				error => {
					switch (error.status) {
						case 400:
							this.alert = "Ce pseudo est déjà utilisé";
							break;
						default:
							this.alert = "Impossible de charger la partie";
					}
				}
			);
		}
	}

	continueGame(joueurNom: string): void {
		if (joueurNom) {
            this.gameService.continueGame(joueurNom.trim()).subscribe((game) => {
					location.replace("/window/" + game.joueur.Nom);
				},
				error => {
					switch (error.status) {
						case 404:
							this.alert = "Aucune partie pour ce pseudo";
							break;
						default:
							this.alert = "Impossible de reprendre la partie";
					}
				}
			);
		}
	}

	ngOnInit(): void { }
}
