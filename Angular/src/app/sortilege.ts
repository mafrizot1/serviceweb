export interface Sortilege {
    Id: number;
    Nom: string;
    Description: string;
    Effet: string;
    Puissance: number;
    Type: number;
}

export interface SortilegeVM {
    Id: number,
    Nom: string
}
