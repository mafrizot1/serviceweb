import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IChapitre } from './chapitre';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChapitreService {

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    private chapitresUrl = 'https://isimafia.azurewebsites.net/api/Chapitre';

    getChapitres(): Observable<IChapitre[]> {
        return this.http.get<IChapitre[]>(this.chapitresUrl)
    }

    getChapitre(id: number): Observable<IChapitre> {
        const url = `${this.chapitresUrl}/${id}`;
        return this.http.get<IChapitre>(url);
    }

    updateChapitre(chapitre: unknown, id: number): Observable<any> {
        const url = `${this.chapitresUrl}/${id}`;
        return this.http.put(url, chapitre, this.httpOptions);
    }

    addChapitre(chapitre: unknown): Observable<IChapitre> {
        return this.http.post<IChapitre>(this.chapitresUrl, chapitre, this.httpOptions);
    }

    deleteChapitre(chapitre: IChapitre | number): Observable<IChapitre> {
        const id = typeof chapitre === 'number' ? chapitre : chapitre.Id;
        const url = `${this.chapitresUrl}/${id}`;

        return this.http.delete<IChapitre>(url, this.httpOptions);
    }

    constructor(private http: HttpClient) { }
}
