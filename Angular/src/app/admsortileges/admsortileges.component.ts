import { Component, OnInit } from '@angular/core';
import { Sortilege } from '../sortilege';
//import { SORTILEGES } from '../mock-admsortileges';
import { SortilegeService } from '../sortilege.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-admsortileges',
  templateUrl: './admsortileges.component.html',
  styleUrls: ['./admsortileges.component.scss']
})
export class AdmsortilegesComponent implements OnInit {

    sortileges: Sortilege[];
    types = [
        {
            id: 1,
            text: 'Utilitaire'
        },
        {
            id: 2,
            text: 'Offensif'
        },
        {
            id: 3,
            text: 'Defensif'
        }
    ];
    selectedType = null;
    sortilegeNom: string;
    sortilegeDescription: string;
    sortilegeEffet: string;
    sortilegePuissance: number;
    dropdownSettings: IDropdownSettings;

    constructor(private sortilegeService: SortilegeService) { }

    getSortileges(): void {
        this.sortilegeService.getSortileges()
            .subscribe(sortileges => this.sortileges = sortileges);
    }

    add(): void {
        this.sortilegeNom = this.sortilegeNom.trim();
        if (!this.sortilegeNom) { return; }
        var sort = { Nom: this.sortilegeNom, Description: this.sortilegeDescription, Effet: this.sortilegeEffet, Puissance: this.sortilegePuissance, Type: this.selectedType[0].id}
        this.sortilegeService.addSortilege(sort)
            .subscribe(sortilege => {
                location.reload();
            });
    }

    nullType(): void {
        this.selectedType = null;
    }

    ngOnInit() {
        this.getSortileges();
        this.dropdownSettings = {
            singleSelection: true,
            idField: 'id',
            textField: 'text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
    }

}
