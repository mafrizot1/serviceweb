import { Sortilege } from './sortilege';

export const SORTILEGES: Sortilege[] = [
    { Id: 17, Nom: 'Endoloris', Description:'cc', Effet:'cc2', Puissance:2, Type : 3 },
    { Id: 18, Nom: 'Sectum Sempra', Description: 'cc', Effet: 'cc2', Puissance: 2, Type: 3  }
];
