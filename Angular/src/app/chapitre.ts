import { IChoixVM } from './choix';
import { IEvenement } from './evenement';

export interface IChapitre {
    Histoire: string,
    Id: number,
    Numero: number,
    Evenements: IEvenement[],
    Choix: IChoixVM[],
    ChoixPrec: IChoixVM[],
}

export interface IChapitreVM {
    Id: number,
    Numero: number
}