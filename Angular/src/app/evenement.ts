import { IChapitreVM } from './chapitre'
import { IObjet, IObjetVM } from './objet'
import { IPersonnageVM } from './personnage'
import { SortilegeVM } from './sortilege'
import { IEmplacement } from './emplacement'

export interface IEvenement {
    Id: number,
    Description: string,
    Emplacement_Id: number,
    Emplacement: IEmplacement,
    Chapitre_Id: number,
    Chapitre: IChapitreVM,
    Indice_Id: number,
    Objet_Id: number,
    Monstre_Id: number,
    Sortilege_Id: number,
    Indice: IObjetVM,
    Monstre: IPersonnageVM,
    Objet: IObjet,
    Sortilege: SortilegeVM
}

export interface IEvenementVM {
    Id: number,
    Description: string,
    Type: string
}
