import { Component, OnInit, Input } from '@angular/core';
import { IPersonnage } from '../personnage';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { PersonnageService } from '../personnage.service';
import { IObjetVM,IObjet } from '../objet';
import { SortilegeVM,Sortilege } from '../sortilege';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ObjetService } from '../objet.service';
import { SortilegeService } from '../sortilege.service';

@Component({
  selector: 'app-admpersonnage-detail',
  templateUrl: './admpersonnage-detail.component.html',
  styleUrls: ['./admpersonnage-detail.component.scss']
})
export class AdmpersonnageDetailComponent implements OnInit {

    @Input() personnage: IPersonnage;
    selectedObjets: IObjetVM[] = [];
    selectedSortileges: SortilegeVM[] = [];
    selectedType = [];
    dropdownSettings: IDropdownSettings;
    dropdownSettings2: IDropdownSettings;
    objets: IObjet[];
    sortileges: Sortilege[];
    types = [
        {
            id: 0,
            text: 'Monstre'
        },
        {
            id: 1,
            text: 'PNJ'
        }
    ];
    

    constructor(
        private route: ActivatedRoute,
        private personnageService: PersonnageService,
        private objetService: ObjetService,
        private sortilegeService: SortilegeService,
        private location: Location
    ) { }

    getPersonnage(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.personnageService.getPersonnage(id)
            .subscribe(personnage => {
                this.personnage = personnage;
                this.selectedObjets = personnage.Objets;
                this.selectedSortileges = personnage.Sortileges;
                this.selectedType.push(this.types[personnage.Type]);
            });
        
        
    }

    getObjets(): void {
        this.objetService.getObjets()
            .subscribe(objets => this.objets = objets);
    }

    getSortileges(): void {
        this.sortilegeService.getSortileges()
            .subscribe(sortileges => this.sortileges = sortileges);
    }

    save(): void {
        var res;
        const id = +this.route.snapshot.paramMap.get('id');
        var ObjetIds = new (Array);
        for (let obj of this.selectedObjets) {
                res = ObjetIds.push(obj.Id);
        }
        var SortilegeIds = new (Array);
        for (let sort of this.selectedSortileges) {
            res = SortilegeIds.push(sort.Id);
        }
        var perso = {
            Nom: this.personnage.Nom,
            Pv: this.personnage.Pv,
            Attaque: this.personnage.Attaque,
            Defense: this.personnage.Defense,
            Dialogue: this.personnage.Dialogue,
            Type: this.selectedType[0].id,
            Probabilite: this.personnage.Probabilite,
            ObjetIds: ObjetIds,
            SortilegeIds: SortilegeIds
        }
        this.personnageService.updatePersonnage(perso, id)
            .subscribe(() => this.goBack());
    }

    delete(personnage: IPersonnage): void {
        this.personnageService.deletePersonnage(personnage)
            .subscribe(() => this.goBack());
    }

    nullType(): void {
        this.selectedType = null;
    }

    /* A supprimer quand on est sur que ma nouvelle méthode marche (normalement le cas) */
    /*retirerobj(objet: IObjetVM): void {
        var res;
        const id = +this.route.snapshot.paramMap.get('id');
        this.personnage.ObjetIds = new (Array);
        for (let obj of this.personnage.Objets) {
            if (obj.Id !== objet.Id) {
                res = this.personnage.ObjetIds.push(obj.Id);
            }
        }
        this.personnage.SortilegeIds = new (Array);
        for (let sort of this.personnage.Sortileges) {
            res = this.personnage.SortilegeIds.push(sort.Id);
        }
        this.personnageService.updatePersonnage(this.personnage, id)
            .subscribe(() => location.reload());
    }

    ajouterobj(objet: IObjetVM): void {
        var res;
        const id = +this.route.snapshot.paramMap.get('id');
        this.personnage.ObjetIds = new (Array);
        for (let obj of this.personnage.Objets) {
            res = this.personnage.ObjetIds.push(obj.Id);
        }
        this.personnage.SortilegeIds = new (Array);
        for (let sort of this.personnage.Sortileges) {
            res = this.personnage.SortilegeIds.push(sort.Id);
        }
        res = this.personnage.ObjetIds.push(objet.Id);
        this.personnageService.updatePersonnage(this.personnage, id)
            .subscribe(() => location.reload());
    }

    retirersort(sortilege: SortilegeVM): void {
        var res;
        const id = +this.route.snapshot.paramMap.get('id');
        this.personnage.SortilegeIds = new (Array);
        for (let sort of this.personnage.Sortileges) {
            if (sort.Id !== sortilege.Id) {
                res = this.personnage.SortilegeIds.push(sort.Id);
            }
        }
        this.personnage.ObjetIds = new (Array);
        for (let obj of this.personnage.Objets) {
            res = this.personnage.ObjetIds.push(obj.Id);
        }
        this.personnageService.updatePersonnage(this.personnage, id)
            .subscribe(() => location.reload());
    }

    ajoutersort(sortilege: SortilegeVM): void {
        var res;
        const id = +this.route.snapshot.paramMap.get('id');
        this.personnage.ObjetIds = new (Array);
        for (let obj of this.personnage.Objets) {
            res = this.personnage.ObjetIds.push(obj.Id);
        }
        this.personnage.SortilegeIds = new (Array);
        for (let sort of this.personnage.Sortileges) {
            res = this.personnage.SortilegeIds.push(sort.Id);
        }
        res = this.personnage.SortilegeIds.push(sortilege.Id);
        this.personnageService.updatePersonnage(this.personnage, id)
            .subscribe(() => location.reload());
    }*/

    ngOnInit() {
        this.getPersonnage();
        this.getObjets();
        this.getSortileges();
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'Id',
            textField: 'Nom',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
        this.dropdownSettings2 = {
            singleSelection: true,
            idField: 'id',
            textField: 'text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
    }

    goBack(): void {
        this.location.back();
    }

}
