import { Component, OnInit } from '@angular/core';
import { IEmplacement } from '../emplacement';
import { EmplacementService } from '../emplacement.service';

@Component({
  selector: 'app-admemplacements',
  templateUrl: './admemplacements.component.html',
  styleUrls: ['./admemplacements.component.scss']
})
export class AdmemplacementsComponent implements OnInit {

    emplacements: IEmplacement[];

  constructor(private emplacementService: EmplacementService) { }

    getEmplacements(): void {
        this.emplacementService.getEmplacements()
            .subscribe(emplacements => this.emplacements = emplacements);
    }

    add(h: number, l: number, x: number, y: number): void {
        if (!h) { return; }
        var empl = {Hauteur:h,Largeur:l,X:x,Y:y}
        this.emplacementService.addEmplacement(empl)
            .subscribe(empl => {
                location.reload();
            });
    }

    ngOnInit() {
        this.getEmplacements();
    }
}
