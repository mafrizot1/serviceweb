import { Component, OnInit } from '@angular/core';
import { IChapitre } from '../chapitre';
import { ChapitreService } from '../chapitre.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { IChoix } from '../choix'
import { ChoixService } from '../choix.service'

@Component({
  selector: 'app-admchoix',
  templateUrl: './admchoix.component.html',
  styleUrls: ['./admchoix.component.scss']
})
export class AdmchoixComponent implements OnInit {

    choix: IChoix[];
    chapitres: IChapitre[];
    selectedChapitre = null;
    selectedChapitrePrec = null;
    dropdownSettings: IDropdownSettings;

    constructor(private chapitreService: ChapitreService, private choixService: ChoixService) { }

    getChapitres(): void {
        this.chapitreService.getChapitres()
            .subscribe(chapitres => this.chapitres = chapitres);
    }

    getChoix(): void {
        this.choixService.getChoixs()
            .subscribe(choix => { this.choix = choix; console.log(this.choix); });
        
    }

    nullChapP(): void {
        this.selectedChapitrePrec = null;
    }

    nullChap(): void {
        this.selectedChapitre = null;
    }

    add(cont: string): void {
        cont = cont.trim();
        if (!cont) { return; }
        var choix = {Contenu: cont, ChapitrePrecId: this.selectedChapitrePrec[0].Id, ChapitreId: this.selectedChapitre[0].Id};
        this.choixService.addChoix(choix)
            .subscribe(choix => {
                location.reload();
            });
    }

    ngOnInit() {
        this.getChapitres();
        this.getChoix();
        
        this.dropdownSettings = {
            singleSelection: true,
            idField: 'Id',
            textField: 'Numero',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
    }

}
