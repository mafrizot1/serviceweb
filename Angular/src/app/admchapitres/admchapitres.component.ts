import { Component, OnInit } from '@angular/core';
import { IChapitre } from '../chapitre';
import { ChapitreService } from '../chapitre.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-admchapitres',
  templateUrl: './admchapitres.component.html',
  styleUrls: ['./admchapitres.component.scss']
})
export class AdmchapitresComponent implements OnInit {

    chapitres: IChapitre[];
    dropdownSettings: IDropdownSettings;

    constructor(private chapitreService: ChapitreService,
    ) { }


    getChapitres(): void {
        this.chapitreService.getChapitres()
            .subscribe(chapitres => this.chapitres = chapitres);
    }

    add(histoire: string, numero: number): void {
        histoire = histoire.trim();
        if (!numero) { return; }
       /* var ChoixIds = new Array;
        for (let item of this.selectedChoix) {
            ChoixIds.push(item.Id);
        }*/
        var chapitre = { Histoire: histoire, Numero: numero}
        this.chapitreService.addChapitre(chapitre)
            .subscribe(chapitre => {
                location.reload();
            });
    }

    ngOnInit() {
        this.getChapitres();
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'Id',
            textField: 'Nom',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
    }

}
