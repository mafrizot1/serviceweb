export interface IEmplacement {
    Id: number,
    Hauteur: number,
    Largeur: number,
    X: number,
    Y: number
}
