import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IIndice } from './indice';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class IndiceService {

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    private indicesUrl = 'https://isimafia.azurewebsites.net/api/Indice';

    getIndices(): Observable<IIndice[]> {
        return this.http.get<IIndice[]>(this.indicesUrl)
    }

    getIndice(id: number): Observable<IIndice> {
        const url = `${this.indicesUrl}/${id}`;
        return this.http.get<IIndice>(url);
    }

    updateIndice(indice: IIndice, id: number): Observable<any> {
        const url = `${this.indicesUrl}/${id}`;
        return this.http.put(url, indice, this.httpOptions);
    }

    addIndice(indice: unknown): Observable<IIndice> {
        return this.http.post<IIndice>(this.indicesUrl, indice, this.httpOptions);
    }

    deleteIndice(indice: IIndice | number): Observable<IIndice> {
        const id = typeof indice === 'number' ? indice : indice.Id;
        const url = `${this.indicesUrl}/${id}`;

        return this.http.delete<IIndice>(url, this.httpOptions);
    }

    constructor(private http: HttpClient) { };

}
