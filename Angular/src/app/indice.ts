export interface IIndice {
    Id: number,
    Nom: string,
    Description: string,
    Url: string,
    Hauteur: number,
    Largeur: number,
    X: number,
    Y: number
}
