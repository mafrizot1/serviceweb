import { Component, OnInit, Input } from '@angular/core';
import { Sortilege } from '../sortilege';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { SortilegeService } from '../sortilege.service';

@Component({
  selector: 'app-admsortilege-detail',
  templateUrl: './admsortilege-detail.component.html',
  styleUrls: ['./admsortilege-detail.component.scss']
})
export class AdmsortilegeDetailComponent implements OnInit {
    @Input() sortilege: Sortilege;
    types = [
        {
            id: 1,
            text: 'Utilitaire'
        },
        {
            id: 2,
            text: 'Offensif'
        },
        {
            id: 3,
            text: 'Defensif'
        }
    ];
    selectedType = null;
    dropdownSettings: IDropdownSettings;

    constructor(
        private route: ActivatedRoute,
        private sortilegeService: SortilegeService,
        private location: Location
    ) { }

    getSortilege(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.sortilegeService.getSortilege(id)
            .subscribe(sortilege => {
                this.sortilege = sortilege;
                this.selectedType = new Array;
                this.selectedType.push(this.types[(sortilege.Type) - 1]);
            });
    }

    save(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        var sort = {
            Nom: this.sortilege.Nom,
            Description: this.sortilege.Description,
            Type: this.selectedType[0].id,
            Puissance: this.sortilege.Puissance,
            Effet: this.sortilege.Effet,
        }
        this.sortilegeService.updateSortilege(sort,id)
            .subscribe(() => this.goBack());
    }

    ngOnInit() {
        this.getSortilege();
        this.dropdownSettings = {
            singleSelection: true,
            idField: 'id',
            textField: 'text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
    }

    nullType(): void {
        this.selectedType = null;
    }

    delete(sortilege: Sortilege): void {
        this.sortilegeService.deleteSortilege(sortilege)
            .subscribe(() => this.goBack());
    }

    goBack(): void {
        this.location.back();
    }

}
