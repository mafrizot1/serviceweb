import { Component, OnInit, Input } from '@angular/core';
import { IObjet } from '../objet';
import { IObjetVM } from '../objet';
import { ObjetService } from '../objet.service';
import { WindowComponent } from '../Window/Window.component';

@Component({
  selector: 'app-object',
  templateUrl: './object.component.html',
  styleUrls: ['./object.component.scss']
})
export class ObjectComponent implements OnInit {

  @Input() Name: string;
  @Input() URL: string;
  @Input() Height: any;
  @Input() Width: any;

  constructor(private objetService: ObjetService) { }

  //TODO :  ajouter l'objet à l'inventaire
  clickObjet (): void {
    
  }


  ngOnInit() {  }
}