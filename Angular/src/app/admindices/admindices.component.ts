import { Component, OnInit } from '@angular/core';
import { IIndice } from '../indice';
import { IndiceService } from '../indice.service';

@Component({
    selector: 'app-admindices',
    templateUrl: './admindices.component.html',
    styleUrls: ['./admindices.component.scss']
})
export class AdmindicesComponent implements OnInit {

    indices: IIndice[];

    constructor(private indiceService: IndiceService) { }

    getIndices(): void {
        this.indiceService.getIndices()
            .subscribe(indices => this.indices = indices);
    }

    add(nom: string, description: string, url: string): void {
        nom = nom.trim();
        if (!nom) { return; }
        var indice = { Nom: nom, Description:description, Url:url }
        this.indiceService.addIndice(indice)
            .subscribe(indice => {
                location.reload();
            });
    }

    ngOnInit() {
        this.getIndices();

    }

}
