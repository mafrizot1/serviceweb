import { Component, OnInit, Input } from '@angular/core';
import { IPersonnage, IPersonnageVM } from '../personnage';
import { IObjetVM, IObjet } from '../objet';
import { Sortilege,SortilegeVM } from '../sortilege';
import { IChapitre } from '../chapitre';
import { IEvenement } from '../evenement';
import { Location } from '@angular/common';
import { PersonnageService } from '../personnage.service';
import { ObjetService } from '../objet.service';
import { SortilegeService } from '../sortilege.service';
import { EvenementService } from '../evenement.service';
import { ChapitreService } from '../chapitre.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ActivatedRoute } from '@angular/router';
import { IEmplacement } from '../emplacement';
import { IIndice } from '../indice';
import { EmplacementService } from '../emplacement.service';
import { IndiceService } from '../indice.service';

@Component({
  selector: 'app-admevenement-detail',
  templateUrl: './admevenement-detail.component.html',
  styleUrls: ['./admevenement-detail.component.scss']
})
export class AdmevenementDetailComponent implements OnInit {

    objets: IObjet[];
    chapitres: IChapitre[];
    sortileges: Sortilege[];
    monstres: IPersonnage[];
    emplacements = null;
    indices: IIndice[];
    selectedObjet = null;
    selectedSortilege = null;
    selectedMonstre = null;
    selectedChapitre = null;
    selectedIndice = null;
    selectedEmplacement = null;
    dropdownSettings1: IDropdownSettings;
    dropdownSettings2: IDropdownSettings;
    dropdownSettings3: IDropdownSettings;
    evenement: IEvenement;

    constructor(private route: ActivatedRoute,
        private personnageService: PersonnageService,
        private objetService: ObjetService,
        private sortilegeService: SortilegeService,
        private evenementService: EvenementService,
        private chapitreService: ChapitreService,
        private emplacementService: EmplacementService,
        private indiceService: IndiceService,
        private location: Location
    ) { }

    getEvenement(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.evenementService.getEvenement(id)
            .subscribe(evenement => {
                this.evenement = evenement;
                if (evenement.Objet) {
                    this.selectedObjet = new Array;
                    this.selectedObjet.push(evenement.Objet);
                }
                if (evenement.Sortilege) {
                    this.selectedSortilege = new Array;
                    this.selectedSortilege.push(evenement.Sortilege);
                }
                if (evenement.Monstre) {
                    this.selectedMonstre = new Array;
                    this.selectedMonstre.push(evenement.Monstre);
                }
                if (evenement.Indice) {
                    this.selectedIndice = new Array;
                    this.selectedIndice.push(evenement.Indice);
                }
                if (evenement.Chapitre) {
                    this.selectedChapitre = new Array;
                    this.selectedChapitre.push(evenement.Chapitre);
                }
                if (evenement.Emplacement) {
                    this.selectedEmplacement = new Array;
                    this.selectedEmplacement.push({ Id: evenement.Emplacement.Id, Nom: 'H:' + evenement.Emplacement.Hauteur + ',L:' + evenement.Emplacement.Largeur + ',X:' + evenement.Emplacement.X + ',Y:' + evenement.Emplacement.Y });
                }
            });
    }

    getObjets(): void {
        this.objetService.getObjets()
            .subscribe(objets => this.objets = objets);
    }

    getSortileges(): void {
        this.sortilegeService.getSortileges()
            .subscribe(sortileges => this.sortileges = sortileges);
    }

    getChapitres(): void {
        this.chapitreService.getChapitres()
            .subscribe(chapitres => this.chapitres = chapitres);
    }

    getMonstres(): void {
        this.personnageService.getMonstres()
            .subscribe(personnages => this.monstres = personnages);
    }

    getIndices(): void {
        this.indiceService.getIndices()
            .subscribe(indices => { this.indices = indices; console.log(this.indices); });
    }

    getEmplacements(): void {
        this.emplacementService.getEmplacements()
            .subscribe(emplacements => {
            this.emplacements = new Array;
                for (let emplacement of emplacements) {
                    this.emplacements.push({ Id: emplacement.Id, Nom: 'H:' + emplacement.Hauteur + ',L:' + emplacement.Largeur + ',X:' + emplacement.X + ',Y:' + emplacement.Y })
                }
            });
    }

    nullAll(): void {
        this.selectedObjet = null;
        this.selectedSortilege = null;
        this.selectedMonstre = null;
        this.selectedIndice = null;
    }

    nullEmp(): void {
        this.selectedEmplacement = null;
    }

    nullChap(): void {
        this.selectedChapitre = null;
    }

    save(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        var event;
        if (this.selectedObjet) {
            event = { Description: this.evenement.Description, Emplacement_Id: this.selectedEmplacement[0].Id, Chapitre_Id: this.selectedChapitre[0].Id, Objet_Id: this.selectedObjet[0].Id };
        } else if (this.selectedMonstre) {
            event = { Description: this.evenement.Description, Emplacement_Id: this.selectedEmplacement[0].Id, Chapitre_Id: this.selectedChapitre[0].Id, Monstre_Id: this.selectedMonstre[0].Id };
        } else if (this.selectedSortilege) {
            event = { Description: this.evenement.Description, Emplacement_Id: this.selectedEmplacement[0].Id, Chapitre_Id: this.selectedChapitre[0].Id, Sortilege_Id: this.selectedSortilege[0].Id };
        } else if (this.selectedIndice) {
            event = { Description: this.evenement.Description, Emplacement_Id: this.selectedEmplacement[0].Id, Chapitre_Id: this.selectedChapitre[0].Id, Indice_Id: this.selectedIndice[0].Id };
        }
        this.evenementService.updateEvenement(event, id)
            .subscribe(() => this.goBack());
    }

    delete(evenement: IEvenement): void {
        this.evenementService.deleteEvenement(evenement)
            .subscribe(() => this.goBack());
    }

    ngOnInit() {
        this.getEvenement();
        this.getMonstres();
        this.getObjets();
        this.getSortileges();
        this.getChapitres();
        this.getIndices();
        this.getEmplacements();
        this.dropdownSettings1 = {
            singleSelection: true,
            idField: 'Id',
            textField: 'Nom',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
        this.dropdownSettings2 = {
            singleSelection: true,
            idField: 'Id',
            textField: 'Numero',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
        this.dropdownSettings3 = {
            singleSelection: true,
            idField: 'Id',
            textField: 'Id',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 3,
            allowSearchFilter: true
        };
    }

    goBack(): void {
        this.location.back();
    }

}
