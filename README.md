# Projet de Service Web (ZZ2) - Harry Potter

## Membres de l'équipe :
* Mathis FRIZOT
* Flavio RANCHON
* Ilyace BENJELLOUN
* Aymeric LAISSUS
* Aurélie MARCUZZO

## Lancement de l'appliation
* Décompresser le fichier ZIP
* Les fichiers correspondant au backend ne sont pas utilisés car nous les avons mit en ligne sur Azure. Il n'est donc pas utile de lancer l'API.
* Si vous voulez tout de même utiliser le backend en local, l'angular ne le permet pas mais vous pouvez utiliser Swagger.
    * Ouvrir la solution dans Visual Studio avec le fichier `ServiceWeb.sln` se trouvant dans le dossier `./App/`
    * Définir le projet `API` comme projet de démarrage
    > Si une erreur apparaît, indiquant que l'application ne trouve pas \roslyn\csc.exe :
    > Lancer la commande : `Update-Package Microsoft.CodeDom.Providers.DotNetCompilerPlatform -r` dans le package manager console
* Lancer l'application C# *(Backend)* :
    * `CTRL + F5` pour lancer en mode **Release**
    * `F5` pour lancer en mode **Debug**
    > Si vous souhaitez utiliser seulement le backend, il est possible de gérer les données avec **Swagger** en rajoutant "/Swagger" dans l'URL.
* Lancer l'application Angular *(Frontend)* :
    * Se placer dans le dossier Angular
    * `npm install`
    * `ng serve --open`
    > La page web devrait s'ouvrir automatiquement.\
    Si ce n'est pas le cas, aller sur cet [URL](http://localhost:4200/)