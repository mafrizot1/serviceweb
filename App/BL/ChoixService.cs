﻿using System;
using System.Collections.Generic;
using DAL.Repositories;
using DTO;

namespace BL
{
    public class ChoixService
    {
        /// <summary>
        /// Returns the list of all Choices in the database
        /// </summary>
        /// <returns></returns>
        public static IList<ChoixDto> GetAllChoix()
        {
            using (ChoixRepository _choixRepo = new ChoixRepository())
            {
                return _choixRepo.GetAllChoix();
            }
        }

        /// <summary>
        /// Returns the Choice with the given id the database
        /// </summary>
        /// <param name="id">The id of the Choice to get</param>
        /// <returns></returns>
        public static ChoixDto GetChoix(int id)
        {
            using (ChoixRepository _choixRepo = new ChoixRepository())
            {
                return _choixRepo.GetChoix(id);
            }
        }

        /// <summary>
        /// Adds a new Choice to the database
        /// </summary>
        /// <param name="choix">The new Choice to add</param>
        public static void AddChoix(ChoixDto choix)
        {
            if (choix != null)
            {
                using (ChoixRepository _choixRepo = new ChoixRepository())
                {
                    _choixRepo.AddChoix(choix);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(choix), "Choix ne peut pas être null");
            }
        }

        /// <summary>
        /// Updates an existing Choice in the database
        /// </summary>
        /// <param name="choix">The Choice to update</param>
        public static void UpdateChoix(ChoixDto choix)
        {
            if (choix != null)
            {
                using (ChoixRepository _choixRepo = new ChoixRepository())
                {
                    _choixRepo.UpdateChoix(choix);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(choix), "Choix ne peut pas être null");
            }
        }

        /// <summary>
        /// Deletes the Choice with the given id from the database
        /// </summary>
        /// <param name="id">The id of the Choice to delete</param>
        public static void DeleteChoix(int id)
        {
            if (id > 0)
            {
                using (ChoixRepository _choixRepo = new ChoixRepository())
                {
                    _choixRepo.DeleteChoix(id);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(id), "Id ne peut pas être > 0");
            }
        }

        /// <summary>
        /// Returns the Chapter with the given id tin the database
        /// </summary>
        /// <param name="id">the id of the Chapter to get</param>
        /// <returns></returns>
        public static ChapitreDto GetChapitre(int id)
        {
            using (ChapitreRepository _chapitreRepo = new ChapitreRepository())
            {
                return _chapitreRepo.GetChapitre(id);
            }
        }
    }
}
