﻿using System;
using System.Collections.Generic;
using DAL.Repositories;
using DTO;

namespace BL
{
    public class EmplacementService
    {
        /// <summary>
        /// Returns the list of all Emplacements in the database
        /// </summary>
        /// <returns></returns>
        public static IList<EmplacementDto> GetAllEmplacement()
        {
            using (EmplacementRepository _emplacementRepo = new EmplacementRepository())
            {
                return _emplacementRepo.GetAllEmplacement();
            }
        }

        /// <summary>
        /// Return the Emplacement witht the given id in the database
        /// </summary>
        /// <param name="id">The id of the Emplacement to get</param>
        /// <returns></returns>
        public static EmplacementDto GetEmplacement(int id)
        {
            using (EmplacementRepository _emplacementRepo = new EmplacementRepository())
            {
                return _emplacementRepo.GetEmplacement(id);
            }
        }

        /// <summary>
        /// Adds a new Emplacement to the database
        /// </summary>
        /// <param name="emplacement">The new Emplacement to add</param>
        public static void AddEmplacement(EmplacementDto emplacement)
        {
            if (emplacement != null)
            {
                using (EmplacementRepository _emplacementRepo = new EmplacementRepository())
                {
                    _emplacementRepo.AddEmplacement(emplacement);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(emplacement), "Emplacement ne peut pas être null");
            }
        }

        /// <summary>
        /// Updates an existing Emplacement in the database
        /// </summary>
        /// <param name="emplacement">The Emplacement to update</param>
        public static void UpdateEmplacement(EmplacementDto emplacement)
        {
            if (emplacement != null)
            {
                using (EmplacementRepository _emplacementRepo = new EmplacementRepository())
                {
                    _emplacementRepo.UpdateEmplacement(emplacement);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(emplacement), "Emplacement ne peut pas être null");
            }
        }

        /// <summary>
        /// Deletes the Emplacement with the given id from the database
        /// </summary>
        /// <param name="id">The id of the Empalcement to delete</param>
        public static void DeleteEmplacement(int id)
        {
            if (id > 0)
            {
                using (EmplacementRepository _emplacementRepo = new EmplacementRepository())
                {
                    _emplacementRepo.DeleteEmplacement(id);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(id), "Id ne peut pas être > 0");
            }
        }
    }
}