﻿using System;
using System.Collections.Generic;
using DAL.Repositories;
using DTO;

namespace BL
{
    public class JoueurService
    {
        /// <summary>
        /// Returns the list of all Players in the database
        /// </summary>
        /// <returns></returns>
        public static IList<JoueurDto> GetAllJoueur()
        {
            using (JoueurRepository _joueurRepo = new JoueurRepository())
            {
                return _joueurRepo.GetAllJoueur();
            }
        }

        /// <summary>
        /// Returns the Player with the given id in the database
        /// </summary>
        /// <param name="id">The id of the Player to get</param>
        /// <returns></returns>
        public static JoueurDto GetJoueur(int id)
        {
            using (JoueurRepository _joueurRepo = new JoueurRepository())
            {
                return _joueurRepo.GetJoueur(id);
            }
        }

        /// <summary>
        /// Returns the Player with the given name in the database
        /// </summary>
        /// <param name="name">The name of the Player to get</param>
        /// <returns></returns>
        public static JoueurDto GetJoueurByName(string name)
        {
            using (JoueurRepository _joueurRepo = new JoueurRepository())
            {
                return _joueurRepo.GetJoueurByName(name);
            }
        }

        /// <summary>
        /// Adds a new Player to the database (new game)
        /// </summary>
        /// <param name="joueur">The new Player</param>
        public static void AddJoueur(JoueurDto joueur)
        {
            if (joueur != null)
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    _joueurRepo.AddJoueur(joueur);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(joueur), "Joueur ne peut pas être null");
            }
        }

        /// <summary>
        /// Updates an existing player in the database (battle, getting new items/spells, making progress in the story...)
        /// </summary>
        /// <param name="joueur">The Player to update</param>
        public static void AddChapitre(JoueurDto joueur, ChapitreDto chapitre)
        {
            if (joueur == null)
            {
                throw new ArgumentNullException("joueur");
            }
            else if (chapitre == null)
            {
                throw new ArgumentNullException("chapitre");
            }
            else
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    joueur.Chapitres.Add(chapitre);
                    _joueurRepo.UpdateJoueur(joueur);
                }
            }
        }

        /// <summary>
        /// Adds an Event to the Player's Event list
        /// </summary>
        /// <param name="joueur">The player</param>
        /// <param name="evenement">The Event</param>
        /// <param name="termine">The state of the event, completed = true, not completed = false</param>
        public static void AddEvenement(JoueurDto joueur, EvenementDto evenement, bool termine)
        {
            if (joueur == null)
            {
                throw new ArgumentNullException("joueur");
            }
            else if (evenement == null)
            {
                throw new ArgumentNullException("evenement");
            }
            else
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    _joueurRepo.AddJoueurEvenement(joueur, evenement, termine);
                }
            }
        }

        /// <summary>
        /// Adds an Equipement to the Player's Equipement list
        /// </summary>
        /// <param name="joueur">The player</param>
        /// <param name="equipement">The Equipement</param>
        public static void AddEquipement(JoueurDto joueur, ObjetDto equipement)
        {
            if (joueur == null)
            {
                throw new ArgumentNullException("joueur");
            }
            else if (equipement == null)
            {
                throw new ArgumentNullException("equipement");
            }
            else
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    joueur.Objets.Add(equipement);
                    _joueurRepo.UpdateJoueur(joueur);
                }
            }
        }

        /// <summary>
        /// Updates an existing player in the database (battle, getting new items/spells, making progress in the story...)
        /// </summary>
        /// <param name="joueur">The Player to update</param>
        public static void UpdateJoueur(JoueurDto joueur)
        {
            if (joueur != null)
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    _joueurRepo.UpdateJoueur(joueur);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(joueur), "Joueur ne peut pas être null");
            }
        }

        public static List<IndiceDto> GetIndices(List<int> indicesIds)
        {
            if (indicesIds.Count <= 0)
            {
                return new List<IndiceDto>();
            }
            else
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    return _joueurRepo.GetAllIndices(indicesIds);
                }
            }
        }

        public static void AddIndice(JoueurDto joueur, IndiceDto indice)
        {
            if (joueur == null)
            {
                throw new ArgumentNullException("joueur");
            }
            else if (indice == null)
            {
                throw new ArgumentNullException("indice");
            }
            else
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    joueur.Indices.Add(indice);
                    _joueurRepo.UpdateJoueur(joueur);
                }
            }
        }

        /// <summary>
        /// Deletes a Player with the given id from the database
        /// </summary>
        /// <param name="id">The id of the player to delete</param>
        public static void DeleteJoueur(int id)
        {
            if (id > 0)
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    _joueurRepo.DeleteJoueur(id);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(id), "Id ne peut pas être > 0");
            }
        }

        public static void AddSortilege(JoueurDto joueur, SortilegeDto sortilege)
        {
            if (joueur == null)
            {
                throw new ArgumentNullException("joueur");
            }
            else if (sortilege == null)
            {
                throw new ArgumentNullException("sortilege");
            }
            else
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    joueur.Sortileges.Add(sortilege);
                    _joueurRepo.UpdateJoueur(joueur);
                }
            }
        }

        /// <summary>
        /// Returns the list of all Items the Player possesses
        /// </summary>
        /// <param name="objetIds">The ids of Items the Player possesses</param>
        /// <returns></returns>
        public static List<ObjetDto> GetObjets(List<int> objetIds)
        {
            if (objetIds.Count <= 0)
            {
                return new List<ObjetDto>();
            }
            else
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    return _joueurRepo.GetAllObjets(objetIds);
                }
            }
        }

        /// <summary>
        /// Returns the list of all Spells the Player knows
        /// </summary>
        /// <param name="sortilegeIds">The ids of Spells the Player knows</param>
        /// <returns></returns>
        public static List<SortilegeDto> GetSortileges(List<int> sortilegeIds)
        {
            if (sortilegeIds.Count <= 0)
            {
                return new List<SortilegeDto>();
            }
            else
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    return _joueurRepo.GetAllSortileges(sortilegeIds);
                }
            }
        }

        /// <summary>
        /// Retuens the list of all Chapters the Player visited 
        /// </summary>
        /// <param name="chapitreIds">The ids of Items the Player possesses</param>
        /// <returns></returns>
        public static List<ChapitreDto> GetChapitres(List<int> chapitreIds)
        {
            if (chapitreIds.Count <= 0)
            {
                return new List<ChapitreDto>();
            }
            else
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    return _joueurRepo.GetAllChapitres(chapitreIds);
                }
            }
        }

        /// <summary>
        /// Returns the unfinished event if the player has any
        /// An unfinished event is equivalent to a not completed battle
        /// </summary>
        /// <param name="joueur">The player</param>
        /// <returns></returns>
        public static EvenementDto GetEvenementEnCours(JoueurDto joueur)
        {
            if (joueur == null)
            {
                throw new ArgumentNullException("joueur");
            }
            else
            {
                using (JoueurRepository _joueurRepo = new JoueurRepository())
                {
                    return _joueurRepo.GetEvenementEnCours(joueur);
                }
            }
        }
    }
}
