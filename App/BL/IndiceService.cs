﻿using System;
using System.Collections.Generic;
using DAL.Repositories;
using DTO;

namespace BL
{
    public class IndiceService
    {
        /// <summary>
        /// Returns the list of all Hints in the database
        /// </summary>
        /// <returns></returns>
        public static IList<IndiceDto> GetAllIndice()
        {
            using (IndiceRepository _indiceRepo = new IndiceRepository())
            {
                return _indiceRepo.GetAllIndice();
            }
        }

        /// <summary>
        /// Returns the Hint with the given id in the database
        /// </summary>
        /// <param name="id">The id of the Hint to get</param>
        /// <returns></returns>
        public static IndiceDto GetIndice(int id)
        {
            using (IndiceRepository _indiceRepo = new IndiceRepository())
            {
                return _indiceRepo.GetIndice(id);
            }
        }

        /// <summary>
        /// Adds a new Hint to the database
        /// </summary>
        /// <param name="indice">The new Hint to add</param>
        public static void AddIndice(IndiceDto indice)
        {
            if (indice != null)
            {
                using (IndiceRepository _indiceRepo = new IndiceRepository())
                {
                    _indiceRepo.AddIndice(indice);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(indice), "Indice ne peut pas être null");
            }
        }

        /// <summary>
        /// Updates an existing Hint in the database
        /// </summary>
        /// <param name="indice">The Hint to update</param>
        public static void UpdateIndice(IndiceDto indice)
        {
            if (indice != null)
            {
                using (IndiceRepository _indiceRepo = new IndiceRepository())
                {
                    _indiceRepo.UpdateIndice(indice);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(indice), "Indice ne peut pas être null");
            }
        }

        /// <summary>
        /// Deletes the Hint with the given from the database
        /// </summary>
        /// <param name="id">The id of the Hint to delete</param>
        public static void DeleteIndice(int id)
        {
            if (id > 0)
            {
                using (IndiceRepository _indiceRepo = new IndiceRepository())
                {
                    _indiceRepo.DeleteIndice(id);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(id), "Id ne peut pas être > 0");
            }
        }
    }
}