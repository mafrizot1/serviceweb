﻿using System;
using System.Collections.Generic;
using DAL.Repositories;
using DTO;

namespace BL
{
    public class ObjetService
    {
        /// <summary>
        /// Returns the list of all Items in the database
        /// </summary>
        /// <returns></returns>
        public static IList<ObjetDto> GetAllObjet()
        {
            using (ObjetRepository _objetRepo = new ObjetRepository())
            {
                return _objetRepo.GetAllObjet();
            }
        }

        /// <summary>
        /// Returns the Item with the given id in the database
        /// </summary>
        /// <param name="id">The id of the Item to get</param>
        /// <returns></returns>
        public static ObjetDto GetObjet(int id)
        {
            using (ObjetRepository _objetRepo = new ObjetRepository())
            {
                return _objetRepo.GetObjet(id);
            }
        }

        /// <summary>
        /// Adds a new Item to the database
        /// </summary>
        /// <param name="objet">The new Item to add</param>
        public static void AddObjet(ObjetDto objet)
        {
            if (objet != null)
            {
                using (ObjetRepository _objetRepo = new ObjetRepository())
                {
                    _objetRepo.AddObjet(objet);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(objet), "Objet ne peut pas être null");
            }
        }

        /// <summary>
        /// Updates an existing Item in the database
        /// </summary>
        /// <param name="objet">The item to update</param>
        public static void UpdateObjet(ObjetDto objet)
        {
            if (objet != null)
            {
                using (ObjetRepository _objetRepo = new ObjetRepository())
                {
                    _objetRepo.UpdateObjet(objet);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(objet), "Objet ne peut pas être null");
            }
        }

        /// <summary>
        /// Deletes the Item with the given id from the database
        /// </summary>
        /// <param name="id">The id of the Item to delete</param>
        public static void DeleteObjet(int id)
        {
            if (id > 0)
            {
                using (ObjetRepository _objetRepo = new ObjetRepository())
                {
                    _objetRepo.DeleteObjet(id);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(id), "Id ne peut pas être > 0");
            }
        }
    }
}