﻿using System;
using System.Collections.Generic;
using DAL.Repositories;
using DTO;

namespace BL
{
    public class EvenementService
    {
        /// <summary>
        /// Returns the list of all Events in the database
        /// </summary>
        /// <returns></returns>
        public static IList<EvenementDto> GetAllEvenement()
        {
            using (EvenementRepository _evenementRepo = new EvenementRepository())
            {
                return _evenementRepo.GetAllEvenement();
            }
        }

        /// <summary>
        /// Returns the Event with the given id in the database
        /// </summary>
        /// <param name="id">The id of the Event to get</param>
        /// <returns></returns>
        public static EvenementDto GetEvenement(int id)
        {
            using (EvenementRepository _evenementRepo = new EvenementRepository())
            {
                return _evenementRepo.GetEvenement(id);
            }
        }

        /// <summary>
        /// Adds a new Event to the database
        /// </summary>
        /// <param name="evenement">The new Event to add</param>
        public static void AddEvenement(EvenementDto evenement)
        {
            if (evenement != null)
            {
                using (EvenementRepository _evenementRepo = new EvenementRepository())
                {
                    _evenementRepo.AddEvenement(evenement);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(evenement), "Evenement ne peut pas être null");
            }
        }

        /// <summary>
        /// Updates an existing Event in the database
        /// </summary>
        /// <param name="evenement">The Event to update</param>
        public static void UpdateEvenement(EvenementDto evenement)
        {
            if (evenement != null)
            {
                using (EvenementRepository _evenementRepo = new EvenementRepository())
                {
                    _evenementRepo.UpdateEvenement(evenement);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(evenement), "Evenement ne peut pas être null");
            }
        }

        /// <summary>
        /// Deletes the Event with the given id from the database
        /// </summary>
        /// <param name="id">The id of the Event to delete</param>
        public static void DeleteEvenement(int id)
        {
            if (id > 0)
            {
                using (EvenementRepository _evenementRepo = new EvenementRepository())
                {
                    _evenementRepo.DeleteEvenement(id);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(id), "Id ne peut pas être > 0");
            }
        }

        /// <summary>
        /// Returns the Hint with the given id in the database
        /// </summary>
        /// <param name="indice_Id">The id of the Hint to get</param>
        /// <returns></returns>
        public static ICliquable GetIndice(int indice_Id)
        {
            using (EvenementRepository _evenementRepo = new EvenementRepository())
            {
                return _evenementRepo.GetIndice(indice_Id);
            }
        }

        /// <summary>
        /// Returns the Item with the given id in the database
        /// </summary>
        /// <param name="objet_Id">The if of the Item to get</param>
        /// <returns></returns>
        public static ICliquable GetObjet(int objet_Id)
        {
            using (EvenementRepository _evenementRepo = new EvenementRepository())
            {
                return _evenementRepo.GetObjet(objet_Id);
            }
        }

        /// <summary>
        /// Returns the Monster with the given id in the database
        /// </summary>
        /// <param name="monstre_Id">The id of the Monster to get</param>
        /// <returns></returns>
        public static ICliquable GetMonstre(int monstre_Id)
        {
            using (EvenementRepository _evenementRepo = new EvenementRepository())
            {
                return _evenementRepo.GetMonstre(monstre_Id);
            }
        }

        /// <summary>
        /// Returns the Spell with the given id in the database
        /// </summary>
        /// <param name="sortilege_Id">The id of the Spell to get</param>
        /// <returns></returns>
        public static ICliquable GetSortilege(int sortilege_Id)
        {
            using (EvenementRepository _evenementRepo = new EvenementRepository())
            {
                return _evenementRepo.GetSortilege(sortilege_Id);
            }
        }

        /// <summary>
        /// Returns the Emplacement with the given id in the database
        /// </summary>
        /// <param name="emplacement_Id">The id of the Empalcement to get</param>
        /// <returns></returns>
        public static EmplacementDto GetEmplacement(int emplacement_Id)
        {
            using (EvenementRepository _evenementRepo = new EvenementRepository())
            {
                return _evenementRepo.GetEmplacement(emplacement_Id);
            }
        }

        /// <summary>
        /// Returns the Chapter with the given id in the database
        /// </summary>
        /// <param name="chapitre_Id">The id of the Chapter to get</param>
        /// <returns></returns>
        public static ChapitreDto GetChapitre(int chapitre_Id)
        {
            using (EvenementRepository _evenementRepo = new EvenementRepository())
            {
                return _evenementRepo.GetChapitre(chapitre_Id);
            }
        }
    }
}
