﻿using System;
using System.Collections.Generic;
using DAL.Repositories;
using DTO;

namespace BL
{
    public class SortilegeService
    {
        /// <summary>
        /// Returns the list of all Spells in the database
        /// </summary>
        /// <returns></returns>
        public static IList<SortilegeDto> GetAllSortilege()
        {
            using (SortilegeRepository _sortilegeRepo = new SortilegeRepository())
            {
                return _sortilegeRepo.GetAllSortilege();
            }
        }

        /// <summary>
        /// Returns the Spell with the given id in the database
        /// </summary>
        /// <param name="id">The if of the Spell to get</param>
        /// <returns></returns>
        public static SortilegeDto GetSortilege(int id)
        {
            using (SortilegeRepository _sortilegeRepo = new SortilegeRepository())
            {
                return _sortilegeRepo.GetSortilege(id);
            }
        }

        /// <summary>
        /// Adds a new Spell to the database
        /// </summary>
        /// <param name="sortilege">The new Spell to add</param>
        public static void AddSortilege(SortilegeDto sortilege)
        {
            if (sortilege != null)
            {
                using (SortilegeRepository _sortilegeRepo = new SortilegeRepository())
                {
                    _sortilegeRepo.AddSortilege(sortilege);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(sortilege), "Sortilege ne peut pas être null");
            }
        }

        /// <summary>
        /// Updates an existing Spell in the database
        /// </summary>
        /// <param name="sortilege">The Spell to update</param>
        public static void UpdateSortilege(SortilegeDto sortilege)
        {
            if (sortilege != null)
            {
                using (SortilegeRepository _sortilegeRepo = new SortilegeRepository())
                {
                    _sortilegeRepo.UpdateSortilege(sortilege);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(sortilege), "Sortilege ne peut pas être null");
            }
        }

        /// <summary>
        /// Deletes the Spell with the given id from the database
        /// </summary>
        /// <param name="id">The id of the Spell to delete</param>
        public static void DeleteSortilege(int id)
        {
            if (id > 0)
            {
                using (SortilegeRepository _sortilegeRepo = new SortilegeRepository())
                {
                    _sortilegeRepo.DeleteSortilege(id);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(id), "Id ne peut pas être > 0");
            }
        }
    }
}
