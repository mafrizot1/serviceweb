﻿using System;
using System.Collections.Generic;
using DAL.Repositories;
using DTO;

namespace BL
{
    public class PersonnageService
    {
        /// <summary>
        /// Returns the list of all Characters in the database
        /// </summary>
        /// <returns></returns>
        public static IList<PersonnageDto> GetAllPersonnage()
        {
            using (PersonnageRepository _personnageRepo = new PersonnageRepository())
            {
                return _personnageRepo.GetAllPersonnage();
            }
        }

        /// <summary>
        /// Returns the Character with the given id in the database
        /// </summary>
        /// <param name="id">The id of the Character to get</param>
        /// <returns></returns>
        public static PersonnageDto GetPersonnage(int id)
        {
            using (PersonnageRepository _personnageRepo = new PersonnageRepository())
            {
                return _personnageRepo.GetPersonnage(id);
            }
        }

        /// <summary>
        /// Adds a new Character to the database
        /// </summary>
        /// <param name="personnage">The new Character to add</param>
        public static void AddPersonnage(PersonnageDto personnage)
        {
            if (personnage != null)
            {
                using (PersonnageRepository _personnageRepo = new PersonnageRepository())
                {
                    _personnageRepo.AddPersonnage(personnage);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(personnage), "Personnage ne peut pas être null");
            }
        }

        /// <summary>
        /// Updates an existing Character in the database
        /// </summary>
        /// <param name="personnage">The Character to update</param>
        public static void UpdatePersonnage(PersonnageDto personnage)
        {
            if (personnage != null)
            {
                using (PersonnageRepository _personnageRepo = new PersonnageRepository())
                {
                    _personnageRepo.UpdatePersonnage(personnage);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(personnage), "Personnage ne peut pas être null");
            }
        }

        /// <summary>
        /// Deletes a Character from the datanase
        /// </summary>
        /// <param name="id">The id of the Character to delete</param>
        public static void DeletePersonnage(int id)
        {
            if (id > 0)
            {
                using (PersonnageRepository _personnageRepo = new PersonnageRepository())
                {
                    _personnageRepo.DeletePersonnage(id);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(id), "Id ne peut pas être > 0");
            }
        }

        /// <summary>
        /// Returns the Chapter with the given id tin the database
        /// </summary>
        /// <param name="id">the id of the Chapter to get</param>
        /// <returns></returns>
        public static ChapitreDto GetChapitre(int id)
        {
            using (ChapitreRepository _personnageRepo = new ChapitreRepository())
            {
                return _personnageRepo.GetChapitre(id);
            }
        }

        /// <summary>
        /// Returns the list of Chapters the Character appears in
        /// </summary>
        /// <param name="chapitreIds">The ids of the Chapters to get</param>
        /// <returns></returns>
        public static List<ChapitreDto> GetChapitres(List<int> chapitreIds)
        {
            if (chapitreIds.Count <= 0)
            {
                return new List<ChapitreDto>();
            }
            else
            {
                using (PersonnageRepository _chapitreRepo = new PersonnageRepository())
                {
                    return _chapitreRepo.GetAllChapitres(chapitreIds);
                }
            }
        }

        /// <summary>
        /// Returns the list of Spells the Character knows (gives to the player)
        /// </summary>
        /// <param name="sortilegeIds">The ids of the Spells to get</param>
        /// <returns></returns>
        public static List<SortilegeDto> GetSortileges(List<int> sortilegeIds)
        {
            if (sortilegeIds.Count <= 0)
            {
                return new List<SortilegeDto>();
            }
            else
            {
                using (PersonnageRepository _personnageRepo = new PersonnageRepository())
                {
                    return _personnageRepo.GetAllSortileges(sortilegeIds);
                }
            }
        }

        /// <summary>
        /// Returns the list of Items the Character posesses (gives to the player)
        /// </summary>
        /// <param name="objetIds">The ids of the Items to get</param>
        /// <returns></returns>
        public static List<ObjetDto> GetObjets(List<int> objetIds)
        {
            if (objetIds.Count <= 0)
            {
                return new List<ObjetDto>();
            }
            else
            {
                using (PersonnageRepository _chapitreRepo = new PersonnageRepository())
                {
                    return _chapitreRepo.GetAllObjets(objetIds);
                }
            }
        }
    }
}
