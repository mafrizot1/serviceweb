﻿using System;
using System.Collections.Generic;
using DAL.Repositories;
using DTO;

namespace BL
{
    public class ChapitreService
    {
        /// <summary>
        /// Returns the list of all Chapters in the database
        /// </summary>
        /// <returns></returns>
        public static IList<ChapitreDto> GetAllChapitre()
        {
            using (ChapitreRepository _chapitreRepo = new ChapitreRepository())
            {
                return _chapitreRepo.GetAllChapitre();
            }
        }

        /// <summary>
        /// Returns the Chapter with the given id tin the database
        /// </summary>
        /// <param name="id">the id of the Chapter to get</param>
        /// <returns></returns>
        public static ChapitreDto GetChapitre(int id)
        {
            using (ChapitreRepository _chapitreRepo = new ChapitreRepository())
            {
                return _chapitreRepo.GetChapitre(id);
            }
        }


        /// <summary>
        /// Adds a new Chaptrer to the database
        /// </summary>
        /// <param name="chapitre">The new Chapter to add</param>
        public static void AddChapitre(ChapitreDto chapitre)
        {
            if (chapitre != null)
            {
                using (ChapitreRepository _chapitreRepo = new ChapitreRepository())
                {
                    _chapitreRepo.AddChapitre(chapitre);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(chapitre), "Chapitre ne peut pas être null");
            }
        }

        /// <summary>
        /// Updates an existing Chapter in the databse
        /// </summary>
        /// <param name="chapitre">The Chapter to update</param>
        public static void UpdateChapitre(ChapitreDto chapitre)
        {
            if (chapitre != null)
            {
                using (ChapitreRepository _chapitreRepo = new ChapitreRepository())
                {
                    _chapitreRepo.UpdateChapitre(chapitre);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(chapitre), "Chapitre ne peut pas être null");
            }
        }

        /// <summary>
        /// Deletes the Chapter with the given id from the database 
        /// </summary>
        /// <param name="id">The id of the Chapter to delete</param>
        public static void DeleteChapitre(int id)
        {
            if (id > 0)
            {
                using (ChapitreRepository _chapitreRepo = new ChapitreRepository())
                {
                    _chapitreRepo.DeleteChapitre(id);
                }
            }
            else
            {
                throw new ArgumentNullException(nameof(id), "Id ne peut pas être > 0");
            }
        }
    }
}
