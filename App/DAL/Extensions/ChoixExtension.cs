﻿using DTO;

namespace DAL.Extensions
{
    public static class ChoixExtension
    {
        public static Choix ToEntity(this ChoixDto dto)
        {
            return new Choix
            {
                Id = dto.Id,
                Contenu = dto.Contenu,
                Chapitre_Id = dto.ProchainChapitre.Id,
                ChapitrePrec_Id = dto.ChapitrePrec.Id,
            };
        }

        public static ChoixDto ToDto(this Choix entity)
        {
            if (entity != null)
            {
                return new ChoixDto
                {
                    Id = entity.Id,
                    Contenu = entity.Contenu,
                    ProchainChapitre = new ChapitreDto()
                    {
                        Id = entity.Chapitre.Id,
                        Numero = entity.Chapitre.Numero,
                        Histoire = entity.Chapitre.Histoire,
                    },
                    ChapitrePrec = new ChapitreDto()
                    {
                        Id = entity.Chapitre1.Id,
                        Numero = entity.Chapitre1.Numero,
                        Histoire = entity.Chapitre1.Histoire,
                    }
                };
            }

            return null;
        }
    }
}
