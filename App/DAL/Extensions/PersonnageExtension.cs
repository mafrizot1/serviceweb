﻿using System.Linq;
using DTO;

namespace DAL.Extensions
{
    public static class PersonnageExtension
    {
        public static Personnage ToEntity(this PersonnageDto dto)
        {
            return new Personnage
            {
                Id = dto.Id,
                Nom = dto.Nom,
                Pv = (dto is MonstreDto) ? ((MonstreDto)dto).Pv : 0,
                Attaque = (dto is MonstreDto) ? ((MonstreDto)dto).Attaque : 0,
                Defense = (dto is MonstreDto) ? ((MonstreDto)dto).Defense : 0,
                Dialogue = (dto is PnjDto) ? ((PnjDto)dto).Dialogue : null,
                Type = (dto is PnjDto) ? 1 : 2,
                Probabilite = (dto is MonstreDto) ? ((MonstreDto)dto).Probabilite : 0,
            };
        }

        public static PersonnageDto ToDto(this Personnage entity)
        {
            if (entity != null)
            {
                switch (entity.Type)
                {
                    case 1:
                        return new PnjDto
                        {
                            Id = entity.Id,
                            Nom = entity.Nom,
                            Dialogue = entity.Dialogue,
                            Chapitres = entity.ChapitrePersonnage.Select(x => x.Chapitre.ToDto()).ToList(),
                            Objets = entity.PersonnageObjet.Select(x => x.Objet.ToDto()).ToList(),
                        };
                    case 2:
                        return new MonstreDto
                        {
                            Id = entity.Id,
                            Nom = entity.Nom,
                            Pv = entity.Pv,
                            Attaque = entity.Attaque,
                            Defense = entity.Defense,
                            //Chapitres = entity.ChapitrePersonnage.Select(x => x.Chapitre.ToDto()).ToList(),
                            Chapitres = entity.ChapitrePersonnage.Select(x =>
                            {
                                var c = x.Chapitre;
                                return new ChapitreDto()
                                {
                                    Id = c.Id,
                                    Numero = c.Numero,
                                    Histoire = c.Histoire,
                                };
                            }).ToList(),
                            Objets = entity.PersonnageObjet.Select(x => x.Objet.ToDto()).ToList(),
                            Sortileges = entity.PersonnageSortilege.Select(x => x.Sortilege.ToDto()).ToList(),
                            Probabilite = entity.Probabilite,
                        };
                }
            }

            return null;
        }

    }
}