﻿
using DTO;

namespace DAL.Extensions
{
    public static class EmplacementExtension
    {
        public static Emplacement ToEntity(this EmplacementDto dto)
        {
            return new Emplacement
            {
                Id = dto.Id,
                X = dto.X,
                Y = dto.Y,
                Hauteur = dto.Hauteur,
                Largeur = dto.Largeur,
            };
        }

        public static EmplacementDto ToDto(this Emplacement entity)
        {
            if (entity != null)
            {
                return new EmplacementDto
                {
                    Id = entity.Id,
                    X = entity.X,
                    Y = entity.Y,
                    Hauteur = entity.Hauteur,
                    Largeur = entity.Largeur,
                };
            }

            return null;
        }
    }
}