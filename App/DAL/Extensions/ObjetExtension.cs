﻿using DTO;

namespace DAL.Extensions
{
    public static class ObjetExtension
    {
        public static Objet ToEntity(this ObjetDto dto)
        {
            return new Objet
            {
                Id = dto.Id,
                Nom = dto.Nom,
                Description = dto.Description,
                Type = dto.Type,
                Puissance = dto.Puissance,
                URL = dto.Url,
                TypeEffet = dto.TypeEffet == TypeEffetDto.ATTAQUE ? 0 : 1,
            };
        }

        public static ObjetDto ToDto(this Objet entity)
        {
            if (entity != null)
            {
                return new ObjetDto
                {
                    Id = entity.Id,
                    Nom = entity.Nom,
                    Description = entity.Description,
                    Type = entity.Type,
                    Puissance = entity.Puissance,
                    Url = entity.URL,
                    TypeEffet = entity.TypeEffet == 0 ? TypeEffetDto.ATTAQUE : TypeEffetDto.DEFENSE,
                };
            }

            return null;
        }
    }
}
