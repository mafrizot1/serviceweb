﻿using System.Linq;
using DTO;

namespace DAL.Extensions
{
    public static class JoueurExtension
    {
        public static Joueur ToEntity(this JoueurDto dto)
        {
            return new Joueur
            {
                Id = dto.Id,
                Nom = dto.Nom,
                PV = dto.PV,
                Attaque = dto.Attaque,
                Defense = dto.Defense,
            };
        }

        public static JoueurDto ToDto(this Joueur entity)
        {
            if (entity != null)
            {
                return new JoueurDto
                {
                    Id = entity.Id,
                    Nom = entity.Nom,
                    PV = entity.PV,
                    Attaque = entity.Attaque,
                    Defense = entity.Defense,
                    Objets = entity.Inventaire.Select(x => x.Objet.ToDto()).ToList(),
                    Sortileges = entity.JoueurSortilege.Select(x => x.Sortilege.ToDto()).ToList(),
                    Chapitres = entity.Historique.Select(x => x.Chapitre.ToDto()).ToList(),
                    Evenements = entity.JoueurEvenement.Where(x => x.termine == true).Select(x => x.Evenement.ToDto()).ToList(),
                    Indices = entity.JoueurIndice.Select(x => x.Indice.ToDto()).ToList(),
                };
            }

            return null;
        }
    }
}
