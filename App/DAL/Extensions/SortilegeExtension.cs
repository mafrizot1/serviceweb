﻿using DTO;

namespace DAL.Extensions
{
    public static class SortilegeExtension
    {
        public static Sortilege ToEntity(this SortilegeDto dto)
        {
            return new Sortilege
            {
                Id = dto.Id,
                Nom = dto.Nom,
                Description = dto.Description,
                Effet = (dto is SortilegeUtilitaireDto) ? ((SortilegeUtilitaireDto)dto).Effet : "",
                Puissance = (dto is SortilegeDeCombatDto) ? ((SortilegeDeCombatDto)dto).Puissance : 0,
                Type = GetType(dto),
            };
        }

        private static int GetType(SortilegeDto dto)
        {
            if (dto is SortilegeUtilitaireDto)
                return 1;
            else if (dto is SortilegeOffensifDto)
                return 2;
            else return 3;
        }

        public static SortilegeDto ToDto(this Sortilege entity)
        {
            if (entity != null)
            {
                switch (entity.Type)
                {
                    case 1:
                        return new SortilegeUtilitaireDto
                        {
                            Id = entity.Id,
                            Nom = entity.Nom,
                            Description = entity.Description,
                            Effet = entity.Effet,
                            Type = entity.Type,
                        };
                    case 2:
                        return new SortilegeOffensifDto
                        {
                            Id = entity.Id,
                            Nom = entity.Nom,
                            Description = entity.Description,
                            //Effet = entity.Effet,
                            Type = entity.Type,
                            Puissance = entity.Puissance,
                        };
                    case 3:
                        return new SortilegeDefensifDto
                        {
                            Id = entity.Id,
                            Nom = entity.Nom,
                            Description = entity.Description,
                            //Effet = entity.Effet,
                            Type = entity.Type,
                            Puissance = entity.Puissance,
                        };
                }
            }

            return null;
        }
    }
}