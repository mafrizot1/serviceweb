﻿using System;

using DTO;

namespace DAL.Extensions
{
    public static class EvenementExtension
    {
        public static Evenement ToEntity(this EvenementDto dto)
        {
            return new Evenement
            {
                Id = dto.Id,
                Description = dto.Description,
                Emplacement_Id = dto.Emplacement.Id,
                Chapitre_Id = dto.Chapitre.Id,
                Indice_Id = (dto.Cliquable is IndiceDto) ? (dto.Cliquable as IndiceDto).Id : new Nullable<int>(),
                Objet_Id = (dto.Cliquable is ObjetDto) ? (dto.Cliquable as ObjetDto).Id : new Nullable<int>(),
                Sortilege_Id = (dto.Cliquable is SortilegeDto) ? (dto.Cliquable as SortilegeDto).Id : new Nullable<int>(),
                Monstre_Id = (dto.Cliquable is MonstreDto) ? (dto.Cliquable as MonstreDto).Id : new Nullable<int>(),
            };
        }

        public static EvenementDto ToDto(this Evenement entity)
        {
            if (entity != null)
            {
                int type = 0;
                if (entity.Indice_Id.HasValue)
                {
                    type = 1;
                }
                else if (entity.Objet_Id.HasValue)
                {
                    type = 2;
                }
                else if (entity.Sortilege_Id.HasValue)
                {
                    type = 3;
                }
                else if (entity.Monstre_Id.HasValue)
                {
                    type = 4;
                }


                if (type != 0)
                {
                    var evenementDTO = new EvenementDto
                    {
                        Id = entity.Id,
                        Description = entity.Description,
                        Emplacement = entity.Emplacement.ToDto(),
                        Chapitre = new ChapitreDto()
                        {
                            Id = entity.Chapitre.Id,
                            Numero = entity.Chapitre.Numero,
                            Histoire = entity.Chapitre.Histoire,
                        }
                    };
                    switch (type)
                    {
                        case 1:
                            evenementDTO.Cliquable = entity.Indice.ToDto();
                            break;
                        case 2:
                            evenementDTO.Cliquable = entity.Objet.ToDto();
                            break;
                        case 3:
                            evenementDTO.Cliquable = entity.Sortilege.ToDto();
                            break;
                        default:
                            evenementDTO.Cliquable = entity.Personnage.ToDto() as MonstreDto;
                            break;
                    }

                    return evenementDTO;
                }
            }

            return null;
        }
    }
}