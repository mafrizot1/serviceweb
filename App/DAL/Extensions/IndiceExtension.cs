﻿
using DTO;

namespace DAL.Extensions
{
    public static class IndiceExtension
    {
        public static Indice ToEntity(this IndiceDto dto)
        {
            return new Indice
            {
                Id = dto.Id,
                Nom = dto.Nom,
                Description = dto.Description,
                URL = dto.URL,
            };
        }

        public static IndiceDto ToDto(this Indice entity)
        {
            if (entity != null)
            {
                return new IndiceDto
                {
                    Id = entity.Id,
                    Nom = entity.Nom,
                    Description = entity.Description,
                    URL = entity.URL,
                };
            }

            return null;
        }
    }
}