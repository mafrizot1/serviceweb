﻿using System.Collections.Generic;
using System.Linq;
using DTO;

namespace DAL.Extensions
{
    public static class ChapitreExtension
    {
        public static Chapitre ToEntity(this ChapitreDto dto)
        {
            return new Chapitre
            {
                Id = dto.Id,
                Numero = dto.Numero,
                Histoire = dto.Histoire,
            };
        }

        public static ChapitreDto ToDto(this Chapitre entity)
        {
            if (entity != null)
            {
                var chapDto = new ChapitreDto
                {
                    Id = entity.Id,
                    Numero = entity.Numero,
                    Histoire = entity.Histoire,

                    Choix = new List<ChoixDto>(entity.Choix1.Select(x => new ChoixDto()
                    {
                        Id = x.Id,
                        Contenu = x.Contenu,
                        ProchainChapitre = new ChapitreDto { Id = x.Chapitre.Id, Numero = x.Chapitre.Numero, Histoire = x.Chapitre.Histoire },
                        ChapitrePrec = new ChapitreDto { Id = x.Chapitre1.Id, Numero = x.Chapitre1.Numero, Histoire = x.Chapitre.Histoire },
                    })),
                    ChoixPrec = new List<ChoixDto>(entity.Choix.Select(x => new ChoixDto()
                    {
                        Id = x.Id,
                        Contenu = x.Contenu,
                        ProchainChapitre = new ChapitreDto { Id = x.Chapitre.Id, Numero = x.Chapitre.Numero, Histoire = x.Chapitre.Histoire, },
                        ChapitrePrec = new ChapitreDto { Id = x.Chapitre1.Id, Numero = x.Chapitre1.Numero, Histoire = x.Chapitre.Histoire, },
                    })),
                    Evenements = new List<EvenementDto>(entity.Evenement.Select(x => x.ToDto())).ToList(),
                };

                return chapDto;
            }

            return null;
        }
    }
}
