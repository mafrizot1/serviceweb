﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using DAL.Extensions;
using DTO;

namespace DAL.Repositories
{
    public class JoueurRepository : IDisposable
    {
        private readonly ServiceWebDatabaseEntities _dbcontext = null;

        public JoueurRepository()
        {
            _dbcontext = new ServiceWebDatabaseEntities();
        }

        public JoueurRepository(ServiceWebDatabaseEntities context)
        {
            _dbcontext = context;
        }

        /// <summary>
        /// Returns the list of all Players in the database
        /// </summary>
        /// <returns></returns>
        public List<JoueurDto> GetAllJoueur()
        {
            try
            {
                //Get all student data line from database 
                List<Joueur> joueurEntities = _dbcontext.Joueur.ToList();

                //transform to DTO, and send to upper layer
                return joueurEntities.Select(x => x.ToDto()).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Returns the Player with the given id in the database
        /// </summary>
        /// <param name="id">The id of the Player to get</param>
        /// <returns></returns>
        public JoueurDto GetJoueur(int id)
        {
            try
            {
                //Get all student data line from database 
                Joueur joueurEntity = _dbcontext.Joueur.Find(id);

                //transform to DTO, and send to upper layer
                return joueurEntity.ToDto();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Returns the Player with the given name in the database
        /// </summary>
        /// <param name="name">The name of the Player to get</param>
        /// <returns></returns>
        public JoueurDto GetJoueurByName(string name)
        {
            try
            {
                //Get all student data line from database 
                Joueur joueurEntity = _dbcontext.Joueur.FirstOrDefault(x => x.Nom == name);

                //transform to DTO, and send to upper layer
                return joueurEntity.ToDto();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Adds a new Player to the database (new game)
        /// </summary>
        /// <param name="joueur">The new Player</param>
        public void AddJoueur(JoueurDto joueur)
        {
            Joueur newJoueur = joueur.ToEntity();
            var joueurCreated = _dbcontext.Joueur.Add(newJoueur);
            _dbcontext.SaveChanges();

            joueur.Id = joueurCreated.Id;

            // Pas besoin vu qu'on vient juste de créer le joueur donc il n'a pas d'objets ni de sortilèges.
            // UpdateJoueurObjets(joueur);
            // UpdateJoueurSortileges(joueur);
        }

        /// <summary>
        /// Updates an existing player in the database (battle, getting new items/spells, making progress in the story...)
        /// </summary>
        /// <param name="joueur">The Player to update</param>
        public void UpdateJoueur(JoueurDto joueur)
        {
            Joueur newJoueur = joueur.ToEntity();
            _dbcontext.Entry(newJoueur).State = EntityState.Modified;
            _dbcontext.SaveChanges();
            UpdateJoueurObjets(joueur);
            UpdateJoueurChapitres(joueur);
            UpdateJoueurSortileges(joueur);
            UpdateJoueurIndices(joueur);
        }

        private void UpdateJoueurIndices(JoueurDto joueur)
        {
            // Hints list owned by the player
            List<int> indiceIds = _dbcontext.JoueurIndice.Where(i => i.Joueur_Id == joueur.Id).Select(i => i.Indice_Id).ToList();
            // Hints list to add to the player's inventory
            List<int> add = joueur.Indices.Where(i => !indiceIds.Contains(i.Id)).Select(i => i.Id).ToList();
            // Hints list to delete from the player's inventory
            List<int> delete = indiceIds.Where(i => !joueur.Indices.Select(ind => ind.Id).Contains(i)).ToList();

            add.ForEach(s =>
            {
                JoueurIndice indice = new JoueurIndice()
                {
                    Joueur_Id = joueur.Id,
                    Indice_Id = s,
                };

                _dbcontext.JoueurIndice.Add(indice);
                _dbcontext.SaveChanges();
            });

            delete.ForEach(s =>
            {
                var js = _dbcontext.JoueurSortilege.Where(elt => elt.Joueur_Id == joueur.Id && elt.Sortilege_Id == s).FirstOrDefault();
                if (js != null)
                {
                    _dbcontext.JoueurSortilege.Remove(js);
                }
            });

            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Updates the list of Spells the Players knows 
        /// </summary>
        /// <param name="joueur">The Player</param>
        private void UpdateJoueurSortileges(JoueurDto joueur)
        {
            // Spell list owned by the player
            List<int> sortilegeIds = _dbcontext.JoueurSortilege.Where(s => s.Joueur_Id == joueur.Id).Select(s => s.Sortilege_Id).ToList();
            // Spell list to add to the player's inventory
            List<int> add = joueur.Sortileges.Where(s => !sortilegeIds.Contains(s.Id)).Select(s => s.Id).ToList();
            // Spell list to delete from the player's inventory
            List<int> delete = sortilegeIds.Where(s => !joueur.Sortileges.Select(sort => sort.Id).Contains(s)).ToList();

            add.ForEach(s =>
            {
                JoueurSortilege sortilege = new JoueurSortilege()
                {
                    Joueur_Id = joueur.Id,
                    Sortilege_Id = s,
                };

                _dbcontext.JoueurSortilege.Add(sortilege);
                _dbcontext.SaveChanges();
            });

            delete.ForEach(s =>
            {
                var js = _dbcontext.JoueurSortilege.Where(elt => elt.Joueur_Id == joueur.Id && elt.Sortilege_Id == s).FirstOrDefault();
                if (js != null)
                {
                    _dbcontext.JoueurSortilege.Remove(js);
                }
            });

            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Retuens the list of all Chapters the Player visited 
        /// </summary>
        /// <param name="chapitreIds">The ids of Items the Player possesses</param>
        /// <returns></returns>
        public List<ChapitreDto> GetAllChapitres(List<int> chapitreIds)
        {
            var chapitres = _dbcontext.Chapitre.Where(x => chapitreIds.Contains(x.Id)).ToList();
            var chapitresDto = chapitres.Select(x => x.ToDto()).ToList();
            return chapitresDto;
        }

        public List<IndiceDto> GetAllIndices(List<int> indicesIds)
        {
            var indices = _dbcontext.Indice.Where(x => indicesIds.Contains(x.Id)).ToList();
            var indicesDto = indices.Select(x => x.ToDto()).ToList();
            return indicesDto;
        }

        /// <summary>
        /// Updates the Chapters the Player played (when he makes progress in the story)
        /// </summary>
        /// <param name="joueur">The Player</param>
        private void UpdateJoueurChapitres(JoueurDto joueur)
        {
            // Spell list owned by the player
            List<int> chapitreIds = _dbcontext.Historique.Where(h => h.Joueur_Id == joueur.Id).Select(h => h.Chapitre_Id).ToList();

            // Spell list to add to the player's inventory
            List<int> add = joueur.Chapitres.Where(c => !chapitreIds.Contains(c.Id)).Select(c => c.Id).ToList();

            add.ForEach(c =>
            {
                Historique historique = new Historique()
                {
                    Joueur_Id = joueur.Id,
                    Chapitre_Id = c,
                };

                _dbcontext.Historique.Add(historique);
            });

            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Updates the Events the Player encountered (when he makes progress in the story
        /// The event is set to false if it's a fight he did not complete yet
        /// </summary>
        /// <param name="joueur">The Player</param>
        /// <param name="evenement">The Event the player clicked on</param>
        /// <param name="statut">The status of the event, ended or not</param>
        public void AddJoueurEvenement(JoueurDto joueur, EvenementDto evenement, bool statut)
        {
            JoueurEvenement je = _dbcontext.JoueurEvenement.Where(j => j.Joueur_Id == joueur.Id && j.Evenement_Id == evenement.Id).SingleOrDefault();
            if (je == null)
            {
                JoueurEvenement joueurEvenement = new JoueurEvenement()
                {
                    Joueur_Id = joueur.Id,
                    Evenement_Id = evenement.Id,
                    termine = statut
                };

                _dbcontext.JoueurEvenement.Add(joueurEvenement);
            }
            else
            {
                je.termine = statut;
                _dbcontext.JoueurEvenement.AddOrUpdate(je);
            }

            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Updates the list of items the Player possesses (when on battle or when he is given one)
        /// </summary>
        /// <param name="joueur">The Player</param>
        public void UpdateJoueurObjets(JoueurDto joueur)
        {
            // Object list owned by the player
            List<int> objetIds = _dbcontext.Inventaire.Where(i => i.Joueur_Id == joueur.Id).Select(i => i.Objet_Id).ToList();
            // Object list to add to the player's inventory
            List<int> add = joueur.Objets.Where(o => !objetIds.Contains(o.Id)).Select(o => o.Id).ToList();
            // Object list to delete from the player's inventory
            List<int> delete = objetIds.Where(elt => !joueur.Objets.Select(o => o.Id).Contains(elt)).ToList();
            add.ForEach(o =>
            {
                Inventaire inventaire = new Inventaire()
                {
                    Joueur_Id = joueur.Id,
                    Objet_Id = o,
                };

                _dbcontext.Inventaire.Add(inventaire);
            });

            delete.ForEach(o =>
            {
                var inventaire = _dbcontext.Inventaire.Where(elt => elt.Joueur_Id == joueur.Id && elt.Objet_Id == o).FirstOrDefault();
                if (inventaire != null)
                {
                    _dbcontext.Inventaire.Remove(inventaire);
                }
            });

            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes a Player with the given id from the database
        /// </summary>
        /// <param name="id">The id of the player to delete</param>
        public void DeleteJoueur(int id)
        {
            var joueur = _dbcontext.Joueur.Find(id);
            if (joueur != null)
            {
                _dbcontext.Joueur.Remove(joueur);
                _dbcontext.SaveChanges();
            }
        }

        /// <summary>
        /// Returns the list of all Items the Player possesses
        /// </summary>
        /// <param name="objetIds">The ids of Items the Player possesses</param>
        /// <returns></returns>
        public List<ObjetDto> GetAllObjets(List<int> objetIds)
        {
            var objets = _dbcontext.Objet.Where(x => objetIds.Contains(x.Id)).ToList();
            var objetsDto = objets.Select(x => x.ToDto()).ToList();
            return objetsDto;
        }

        /// <summary>
        /// Returns the list of all Spells the Player knows
        /// </summary>
        /// <param name="sortilegeIds">The ids of Spells the Player knows</param>
        /// <returns></returns>
        public List<SortilegeDto> GetAllSortileges(List<int> sortilegeIds)
        {
            var sortileges = _dbcontext.Sortilege.Where(x => sortilegeIds.Contains(x.Id)).ToList();
            var sortilesgesDto = sortileges.Select(x => x.ToDto()).ToList();
            return sortilesgesDto;
        }

        /// <summary>
        /// Returns the unfinished event if the player has any
        /// An unfinished event is equivalent to a not completed battle
        /// </summary>
        /// <param name="joueur">The player</param>
        /// <returns></returns>
        public EvenementDto GetEvenementEnCours(JoueurDto joueur)
        {
            JoueurEvenement je = _dbcontext.JoueurEvenement.Where(j => j.Joueur_Id == joueur.Id && j.termine == false).FirstOrDefault();
            EvenementDto e = null;
            if (je != null)
            {
                e = je.Evenement.ToDto();

            }

            return e;
        }

        public void Dispose()
        {
            _dbcontext.Dispose();
        }
    }
}
