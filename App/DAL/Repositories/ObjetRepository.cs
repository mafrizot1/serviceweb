﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Extensions;
using DTO;

namespace DAL.Repositories
{
    public class ObjetRepository : IDisposable
    {
        private readonly ServiceWebDatabaseEntities _dbcontext = null;

        public ObjetRepository()
        {
            _dbcontext = new ServiceWebDatabaseEntities();
        }

        public ObjetRepository(ServiceWebDatabaseEntities context)
        {
            _dbcontext = context;
        }

        /// <summary>
        /// Returns the list of all Items in the database
        /// </summary>
        /// <returns></returns>
        public List<ObjetDto> GetAllObjet()
        {
            try
            {
                //Get all student data line from database 
                List<Objet> objetEntities = _dbcontext.Objet.ToList();

                //transform to DTO, and send to upper layer
                return objetEntities.Select(x => new ObjetDto
                {
                    Id = x.Id,
                    Nom = x.Nom,
                    Description = x.Description,
                    Type = x.Type,
                    Puissance = x.Puissance,
                    //TypeEffet = x.TypeEffet,
                }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Returns the Item with the given id in the database
        /// </summary>
        /// <param name="id">The id of the Item to get</param>
        /// <returns></returns>
        public ObjetDto GetObjet(int id)
        {
            try
            {
                //Get all student data line from database 
                Objet objetEntity = _dbcontext.Objet.Find(id);

                //transform to DTO, and send to upper layer
                return objetEntity.ToDto();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Adds a new Item to the database
        /// </summary>
        /// <param name="objet">The new Item to add</param>
        public void AddObjet(ObjetDto objet)
        {
            Objet newObjet = objet.ToEntity();
            _dbcontext.Objet.Add(newObjet);
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Updates an existing Item in the database
        /// </summary>
        /// <param name="objet">The item to update</param>
        public void UpdateObjet(ObjetDto objet)
        {
            Objet newObjet = objet.ToEntity();
            _dbcontext.Entry(newObjet).State = EntityState.Modified;
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes the Item with the given id from the database
        /// </summary>
        /// <param name="id">The id of the Item to delete</param>
        public void DeleteObjet(int id)
        {
            var objet = _dbcontext.Objet.Find(id);
            if (objet != null)
            {
                _dbcontext.Objet.Remove(objet);
                _dbcontext.SaveChanges();
            }
        }

        public void Dispose()
        {
            _dbcontext.Dispose();
        }
    }
}
