﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Extensions;
using DTO;

namespace DAL.Repositories
{
    public class EvenementRepository : IDisposable
    {
        private readonly ServiceWebDatabaseEntities _dbcontext = null;

        public EvenementRepository()
        {
            _dbcontext = new ServiceWebDatabaseEntities();
        }

        public EvenementRepository(ServiceWebDatabaseEntities context)
        {
            _dbcontext = context;
        }

        /// <summary>
        /// Returns the list of all Events in the database
        /// </summary>
        /// <returns></returns>
        public List<EvenementDto> GetAllEvenement()
        {
            try
            {
                //Get all student data line from database 
                List<Evenement> evenementEntities = _dbcontext.Evenement.ToList();

                //transform to DTO, and send to upper layer
                return evenementEntities.Select(x => x.ToDto()).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Returns the Event with the given id in the database
        /// </summary>
        /// <param name="id">The id of the Event to get</param>
        /// <returns></returns>
        public EvenementDto GetEvenement(int id)
        {
            try
            {
                //Get all student data line from database 
                Evenement evenement = _dbcontext.Evenement.Find(id);

                //transform to DTO, and send to upper layer
                return evenement.ToDto();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        /// <summary>
        /// Adds a new Event to the database
        /// </summary>
        /// <param name="evenement">The new Event to add</param>
        public void AddEvenement(EvenementDto evenement)
        {
            // D'abord ajouter l'emplacement dans la base
            Emplacement emplacement = evenement.Emplacement.ToEntity();
            _dbcontext.Emplacement.Add(emplacement);

            // Ensuite ajouter l'évênement avec le bon emplacement
            Evenement newEvenement = evenement.ToEntity();
            _dbcontext.Evenement.Add(newEvenement);
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Updates an existing Event in the database
        /// </summary>
        /// <param name="evenement">The Event to update</param>
        public void UpdateEvenement(EvenementDto evenement)
        {
            Evenement newEvenement = evenement.ToEntity();
            _dbcontext.Entry(newEvenement).State = EntityState.Modified;
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes the Event with the given id from the database
        /// </summary>
        /// <param name="id">The id of the Event to delete</param>
        public void DeleteEvenement(int id)
        {
            var evenement = _dbcontext.Evenement.Find(id);
            if (evenement != null)
            {
                _dbcontext.Evenement.Remove(evenement);
                _dbcontext.SaveChanges();
            }
        }

        /// <summary>
        /// Returns the Hint with the given id in the database
        /// </summary>
        /// <param name="indice_Id">The id of the Hint to get</param>
        /// <returns></returns>
        public ICliquable GetIndice(int indice_Id)
        {
            return _dbcontext.Indice.Find(indice_Id).ToDto();
        }

        /// <summary>
        /// Returns the Item with the given id in the database
        /// </summary>
        /// <param name="objet_Id">The if of the Item to get</param>
        /// <returns></returns>
        public ICliquable GetObjet(int objet_Id)
        {
            return _dbcontext.Objet.Find(objet_Id).ToDto();
        }

        /// <summary>
        /// Returns the Monster with the given id in the database
        /// </summary>
        /// <param name="monstre_Id">The id of the Monster to get</param>
        /// <returns></returns>
        public ICliquable GetMonstre(int monstre_Id)
        {
            return _dbcontext.Personnage.Find(monstre_Id).ToDto() as MonstreDto;
        }

        /// <summary>
        /// Returns the Spell with the given id in the database
        /// </summary>
        /// <param name="sortilege_Id">The id of the Spell to get</param>
        /// <returns></returns>
        public ICliquable GetSortilege(int sortilege_Id)
        {
            return _dbcontext.Sortilege.Find(sortilege_Id).ToDto();
        }

        /// <summary>
        /// Returns the Emplacement with the given id in the database
        /// </summary>
        /// <param name="emplacement_Id">The id of the Empalcement to get</param>
        /// <returns></returns>
        public EmplacementDto GetEmplacement(int emplacement_Id)
        {
            return _dbcontext.Emplacement.Find(emplacement_Id).ToDto();
        }

        /// <summary>
        /// Returns the Chapter with the given id in the database
        /// </summary>
        /// <param name="chapitre_Id">The id of the Chapter to get</param>
        /// <returns></returns>
        public ChapitreDto GetChapitre(int chapitre_Id)
        {
            return _dbcontext.Chapitre.Find(chapitre_Id).ToDto();
        }

        public void Dispose()
        {
            _dbcontext.Dispose();
        }
    }
}
