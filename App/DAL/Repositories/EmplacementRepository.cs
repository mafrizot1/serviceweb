﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Extensions;
using DTO;

namespace DAL.Repositories
{
    public class EmplacementRepository : IDisposable
    {
        private readonly ServiceWebDatabaseEntities _dbcontext = null;

        public EmplacementRepository()
        {
            _dbcontext = new ServiceWebDatabaseEntities();
        }

        public EmplacementRepository(ServiceWebDatabaseEntities context)
        {
            _dbcontext = context;
        }

        /// <summary>
        /// Returns the list of all Emplacements in the database
        /// </summary>
        /// <returns></returns>
        public List<EmplacementDto> GetAllEmplacement()
        {
            try
            {
                //Get all student data line from database 
                List<Emplacement> emplacementEntities = _dbcontext.Emplacement.ToList();

                //transform to DTO, and send to upper layer
                return emplacementEntities.Select(e => e.ToDto()).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Return the Emplacement witht the given id in the database
        /// </summary>
        /// <param name="id">The id of the Emplacement to get</param>
        /// <returns></returns>
        public EmplacementDto GetEmplacement(int id)
        {
            try
            {
                //Get all student data line from database 
                Emplacement emplacementEntity = _dbcontext.Emplacement.Find(id);

                //transform to DTO, and send to upper layer
                return emplacementEntity.ToDto();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Adds a new Emplacement to the database
        /// </summary>
        /// <param name="emplacement">The new Emplacement to add</param>
        public void AddEmplacement(EmplacementDto emplacement)
        {
            Emplacement newEmplacement = emplacement.ToEntity();
            _dbcontext.Emplacement.Add(newEmplacement);
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Updates an existing Emplacement in the database
        /// </summary>
        /// <param name="emplacement">The Emplacement to update</param>
        public void UpdateEmplacement(EmplacementDto emplacement)
        {
            Emplacement newEmplacement = emplacement.ToEntity();
            _dbcontext.Entry(newEmplacement).State = EntityState.Modified;
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes the Emplacement with the given id from the database
        /// </summary>
        /// <param name="id">The id of the Empalcement to delete</param>
        public void DeleteEmplacement(int id)
        {
            var emplacement = _dbcontext.Emplacement.Find(id);
            if (emplacement != null)
            {
                _dbcontext.Emplacement.Remove(emplacement);
                _dbcontext.SaveChanges();
            }
        }

        public void Dispose()
        {
            _dbcontext.Dispose();
        }
    }
}
