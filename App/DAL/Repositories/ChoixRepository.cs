﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Extensions;
using DTO;

namespace DAL.Repositories
{
    public class ChoixRepository : IDisposable
    {
        private readonly ServiceWebDatabaseEntities _dbcontext = null;

        public ChoixRepository()
        {
            _dbcontext = new ServiceWebDatabaseEntities();
        }

        public ChoixRepository(ServiceWebDatabaseEntities context)
        {
            _dbcontext = context;
        }

        /// <summary>
        /// Returns the list of all Choices in the database
        /// </summary>
        /// <returns></returns>
        public List<ChoixDto> GetAllChoix()
        {
            try
            {
                //Get all student data line from database 
                List<Choix> choixEntities = _dbcontext.Choix.ToList();

                //transform to DTO, and send to upper layer
                return choixEntities.Select(x => new ChoixDto
                {
                    Id = x.Id,
                    Contenu = x.Contenu,
                    ProchainChapitre = x.Chapitre.ToDto(),
                    ChapitrePrec = x.Chapitre1.ToDto(),
                }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Returns the Choice with the given id the database
        /// </summary>
        /// <param name="id">The id of the Choice to get</param>
        /// <returns></returns>
        public ChoixDto GetChoix(int id)
        {
            try
            {
                //Get all student data line from database 
                Choix choixEntity = _dbcontext.Choix.Find(id);

                //transform to DTO, and send to upper layer
                return choixEntity.ToDto();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Adds a new Choice to the database
        /// </summary>
        /// <param name="choix">The new Choice to add</param>
        public void AddChoix(ChoixDto choix)
        {
            Choix newChoix = choix.ToEntity();
            _dbcontext.Choix.Add(newChoix);
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Updates an existing Choice in the database
        /// </summary>
        /// <param name="choix">The Choice to update</param>
        public void UpdateChoix(ChoixDto choix)
        {
            Choix newChoix = choix.ToEntity();
            _dbcontext.Entry(newChoix).State = EntityState.Modified;
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes the Choice with the given id from the database
        /// </summary>
        /// <param name="id">The id of the Choice to delete</param>
        public void DeleteChoix(int id)
        {
            var choix = _dbcontext.Choix.Find(id);
            if (choix != null)
            {
                _dbcontext.Choix.Remove(choix);
                _dbcontext.SaveChanges();
            }
        }

        public void Dispose()
        {
            _dbcontext.Dispose();
        }
    }
}
