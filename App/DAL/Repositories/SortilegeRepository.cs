﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Extensions;
using DTO;

namespace DAL.Repositories
{
    public class SortilegeRepository : IDisposable
    {
        private readonly ServiceWebDatabaseEntities _dbcontext = null;

        public SortilegeRepository()
        {
            _dbcontext = new ServiceWebDatabaseEntities();
        }

        public SortilegeRepository(ServiceWebDatabaseEntities context)
        {
            _dbcontext = context;
        }

        /// <summary>
        /// Returns the list of all Spells in the database
        /// </summary>
        /// <returns></returns>
        public List<SortilegeDto> GetAllSortilege()
        {
            try
            {
                List<Sortilege> sortilegeEntities = _dbcontext.Sortilege.ToList();
                return sortilegeEntities.Select(s => s.ToDto()).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Returns the Spell with the given id in the database
        /// </summary>
        /// <param name="id">The if of the Spell to get</param>
        /// <returns></returns>
        public SortilegeDto GetSortilege(int id)
        {
            try
            {
                Sortilege sortilegeEntity = _dbcontext.Sortilege.Find(id);
                return sortilegeEntity.ToDto();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Adds a new Spell to the database
        /// </summary>
        /// <param name="sortilege">The new Spell to add</param>
        public void AddSortilege(SortilegeDto sortilege)
        {
            Sortilege newSortilege = sortilege.ToEntity();
            _dbcontext.Sortilege.Add(newSortilege);
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Updates an existing Spell in the database
        /// </summary>
        /// <param name="sortilege">The Spell to update</param>
        public void UpdateSortilege(SortilegeDto sortilege)
        {
            Sortilege newSortilege = sortilege.ToEntity();
            _dbcontext.Entry(newSortilege).State = EntityState.Modified;
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes the Spell with the given id from the database
        /// </summary>
        /// <param name="id">The id of the Spell to delete</param>
        public void DeleteSortilege(int id)
        {
            var sortilege = _dbcontext.Sortilege.Find(id);
            if (sortilege != null)
            {
                _dbcontext.Sortilege.Remove(sortilege);
                _dbcontext.SaveChanges();
            }
        }

        public void Dispose()
        {
            _dbcontext.Dispose();
        }
    }
}
