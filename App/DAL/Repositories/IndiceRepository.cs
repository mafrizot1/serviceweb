﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Extensions;
using DTO;

namespace DAL.Repositories
{
    public class IndiceRepository : IDisposable
    {
        private readonly ServiceWebDatabaseEntities _dbcontext = null;

        public IndiceRepository()
        {
            _dbcontext = new ServiceWebDatabaseEntities();
        }

        public IndiceRepository(ServiceWebDatabaseEntities context)
        {
            _dbcontext = context;
        }

        /// <summary>
        /// Returns the list of all Hints in the database
        /// </summary>
        /// <returns></returns>
        public List<IndiceDto> GetAllIndice()
        {
            try
            {
                //Get all student data line from database 
                List<Indice> indiceEntities = _dbcontext.Indice.ToList();

                //transform to DTO, and send to upper layer
                return indiceEntities.Select(x => x.ToDto()).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Returns the Hint with the given id in the database
        /// </summary>
        /// <param name="id">The id of the Hint to get</param>
        /// <returns></returns>
        public IndiceDto GetIndice(int id)
        {
            try
            {
                //Get all student data line from database 
                Indice indiceEntity = _dbcontext.Indice.Find(id);

                //transform to DTO, and send to upper layer
                return indiceEntity.ToDto();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Adds a new Hint to the database
        /// </summary>
        /// <param name="indice">The new Hint to add</param>
        public void AddIndice(IndiceDto indice)
        {
            Indice newIndice = indice.ToEntity();
            _dbcontext.Indice.Add(newIndice);
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Updates an existing Hint in the database
        /// </summary>
        /// <param name="indice">The Hint to update</param>
        public void UpdateIndice(IndiceDto indice)
        {
            Indice newIndice = indice.ToEntity();
            _dbcontext.Entry(newIndice).State = EntityState.Modified;
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes the Hint with the given from the database
        /// </summary>
        /// <param name="id">The id of the Hint to delete</param>
        public void DeleteIndice(int id)
        {
            var indice = _dbcontext.Indice.Find(id);
            if (indice != null)
            {
                _dbcontext.Indice.Remove(indice);
                _dbcontext.SaveChanges();
            }
        }

        public void Dispose()
        {
            _dbcontext.Dispose();
        }
    }
}
