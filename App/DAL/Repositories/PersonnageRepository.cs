﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Extensions;
using DTO;

namespace DAL.Repositories
{
    public class PersonnageRepository : IDisposable
    {
        private readonly ServiceWebDatabaseEntities _dbcontext = null;

        public PersonnageRepository()
        {
            _dbcontext = new ServiceWebDatabaseEntities();
        }

        public PersonnageRepository(ServiceWebDatabaseEntities context)
        {
            _dbcontext = context;
        }

        /// <summary>
        /// Returns the list of all Characters in the database
        /// </summary>
        /// <returns></returns>
        public List<PersonnageDto> GetAllPersonnage()
        {
            try
            {
                //Get all student data line from database 
                List<Personnage> personnageEntities = _dbcontext.Personnage.ToList();

                //transform to DTO, and send to upper layer
                return personnageEntities.Select(x => x.ToDto()).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Returns the Character with the given id in the database
        /// </summary>
        /// <param name="id">The id of the Character to get</param>
        /// <returns></returns>
        public PersonnageDto GetPersonnage(int id)
        {
            try
            {
                //Get all student data line from database 
                Personnage personnageEntity = _dbcontext.Personnage.Find(id);

                //transform to DTO, and send to upper layer
                return personnageEntity.ToDto();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Adds a new Character to the database
        /// </summary>
        /// <param name="personnage">The new Character to add</param>
        public void AddPersonnage(PersonnageDto personnage)
        {
            Personnage newPersonnage = personnage.ToEntity();
            var personnageCreated = _dbcontext.Personnage.Add(newPersonnage);
            _dbcontext.SaveChanges();

            personnage.Id = personnageCreated.Id;
            UpdatePersonnageObjets(personnage);
            UpdatePersonnageChapitres(personnage);

            if (personnage is MonstreDto)
            {
                UpdatePersonnageSortileges(personnage as MonstreDto);
            }
        }

        /// <summary>
        /// Updates an existing Character in the database
        /// </summary>
        /// <param name="personnage">The Character to update</param>
        public void UpdatePersonnage(PersonnageDto personnage)
        {
            Personnage newPersonnage = personnage.ToEntity();
            _dbcontext.Entry(newPersonnage).State = EntityState.Modified;
            _dbcontext.SaveChanges();
            UpdatePersonnageObjets(personnage);
            UpdatePersonnageChapitres(personnage);
            if (personnage is MonstreDto)
            {
                UpdatePersonnageSortileges(personnage as MonstreDto);
            }
        }

        /// <summary>
        /// Updates the Chapters the Character appears in
        /// </summary>
        /// <param name="personnage">The Character to update</param>
        private void UpdatePersonnageChapitres(PersonnageDto personnage)
        {
            // List of chapitre in which the personnage appears
            List<int> chapitreIds = _dbcontext.ChapitrePersonnage.Where(s => s.Personnage_Id == personnage.Id).Select(s => s.Chapitre_Id).ToList();
            // List of chapitre in which to add the personnage
            List<int> add = personnage.Chapitres.Where(c => !chapitreIds.Contains(c.Id)).Select(c => c.Id).ToList();
            // List of chapitre in which to delete the personnage
            List<int> delete = chapitreIds.Where(c => !personnage.Chapitres.Select(chap => chap.Id).Contains(c)).ToList();

            add.ForEach(c =>
            {
                ChapitrePersonnage sortilege = new ChapitrePersonnage()
                {
                    Personnage_Id = personnage.Id,
                    Chapitre_Id = c,
                };

                _dbcontext.ChapitrePersonnage.Add(sortilege);
                _dbcontext.SaveChanges();
            });

            delete.ForEach(s =>
            {
                var js = _dbcontext.ChapitrePersonnage.Where(elt => elt.Personnage_Id == personnage.Id && elt.Chapitre_Id == s).FirstOrDefault();
                if (js != null)
                {
                    _dbcontext.ChapitrePersonnage.Remove(js);
                }
            });

            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Updates the Spells the Character knows (gives to the player)
        /// </summary>
        /// <param name="monstre">The Monster to add new Spells</param>
        private void UpdatePersonnageSortileges(MonstreDto monstre)
        {
            // Spell list owned by the player
            List<int> sortilegeIds = _dbcontext.PersonnageSortilege.Where(s => s.Personnage_Id == monstre.Id).Select(s => s.Sortilege_Id).ToList();
            // Spell list to add to the player's inventory
            List<int> add = monstre.Sortileges.Where(s => !sortilegeIds.Contains(s.Id)).Select(s => s.Id).ToList();
            // Spell list to delete from the player's inventory
            List<int> delete = sortilegeIds.Where(s => !monstre.Sortileges.Select(sort => sort.Id).Contains(s)).ToList();

            add.ForEach(s =>
            {
                PersonnageSortilege sortilege = new PersonnageSortilege()
                {
                    Personnage_Id = monstre.Id,
                    Sortilege_Id = s,
                };

                _dbcontext.PersonnageSortilege.Add(sortilege);
                _dbcontext.SaveChanges();
            });

            delete.ForEach(s =>
            {
                var js = _dbcontext.PersonnageSortilege.Where(elt => elt.Personnage_Id == monstre.Id && elt.Sortilege_Id == s).FirstOrDefault();
                if (js != null)
                {
                    _dbcontext.PersonnageSortilege.Remove(js);
                }
            });

            _dbcontext.SaveChanges();
        }


        /// <summary>
        /// Updates the Items the Character possesses (gives to the player)
        /// </summary>
        /// <param name="personnage">The Character to update his Items</param>
        public void UpdatePersonnageObjets(PersonnageDto personnage)
        {
            // Object list owned by the player
            List<int> objetIds = _dbcontext.PersonnageObjet.Where(po => po.Personnage_Id == personnage.Id).Select(po => po.Objet_Id).ToList();
            // Object list to add to the player's inventory
            List<int> add = personnage.Objets.Where(o => !objetIds.Contains(o.Id)).Select(o => o.Id).ToList();
            // Object list to delete from the player's inventory
            List<int> delete = objetIds.Where(elt => !personnage.Objets.Select(o => o.Id).Contains(elt)).ToList();
            add.ForEach(o =>
            {
                PersonnageObjet inventaire = new PersonnageObjet()
                {
                    Personnage_Id = personnage.Id,
                    Objet_Id = o,
                };

                _dbcontext.PersonnageObjet.Add(inventaire);
            });

            delete.ForEach(o =>
            {
                var personnageObjet = _dbcontext.PersonnageObjet.Where(elt => elt.Personnage_Id == personnage.Id && elt.Objet_Id == o).FirstOrDefault();
                if (personnageObjet != null)
                {
                    _dbcontext.PersonnageObjet.Remove(personnageObjet);
                }
            });

            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes a Character from the datanase
        /// </summary>
        /// <param name="id">The id of the Character to delete</param>
        public void DeletePersonnage(int id)
        {
            var personnage = _dbcontext.Personnage.Find(id);
            if (personnage != null)
            {
                _dbcontext.Personnage.Remove(personnage);
                _dbcontext.SaveChanges();
            }
        }

        /// <summary>
        /// Returns the list of Items the Character posesses (gives to the player)
        /// </summary>
        /// <param name="objetIds">The ids of the Items to get</param>
        /// <returns></returns>
        public List<ObjetDto> GetAllObjets(List<int> objetIds)
        {
            var objets = _dbcontext.Objet.Where(x => objetIds.Contains(x.Id)).ToList();
            var objetsDto = objets.Select(x => x.ToDto()).ToList();
            return objetsDto;
        }

        /// <summary>
        /// Returns the list of Chapters the Character appears in
        /// </summary>
        /// <param name="chapitreIds">The ids of the Chapters to get</param>
        /// <returns></returns>
        public List<ChapitreDto> GetAllChapitres(List<int> chapitreIds)
        {
            var chapitres = _dbcontext.Chapitre.Where(x => chapitreIds.Contains(x.Id)).ToList();
            var chapitresDto = chapitres.Select(x => x.ToDto()).ToList();
            return chapitresDto;
        }

        /// <summary>
        /// Returns the list of Spells the Character knows (gives to the player)
        /// </summary>
        /// <param name="sortilegeIds">The ids of the Spells to get</param>
        /// <returns></returns>
        public List<SortilegeDto> GetAllSortileges(List<int> sortilegeIds)
        {
            var sortileges = _dbcontext.Sortilege.Where(x => sortilegeIds.Contains(x.Id)).ToList();
            var sortilesgesDto = sortileges.Select(x => x.ToDto()).ToList();
            return sortilesgesDto;
        }

        public void Dispose()
        {
            _dbcontext.Dispose();
        }
    }
}
