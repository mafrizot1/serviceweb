﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Extensions;
using DTO;

namespace DAL.Repositories
{
    public class ChapitreRepository : IDisposable
    {
        private readonly ServiceWebDatabaseEntities _dbcontext = null;

        public ChapitreRepository()
        {
            _dbcontext = new ServiceWebDatabaseEntities();
        }

        public ChapitreRepository(ServiceWebDatabaseEntities context)
        {
            _dbcontext = context;
        }

        /// <summary>
        /// Returns the list of all Chapters in the database
        /// </summary>
        /// <returns></returns>
        public List<ChapitreDto> GetAllChapitre()
        {
            try
            {
                List<Chapitre> chapitreEntities = _dbcontext.Chapitre.ToList();
                return chapitreEntities.Select(x => x.ToDto()).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Returns the Chapter with the given id tin the database
        /// </summary>
        /// <param name="id">the id of the Chapter to get</param>
        /// <returns></returns>
        public ChapitreDto GetChapitre(int id)
        {
            try
            {
                //Get all student data line from database 
                Chapitre chapitreEntity = _dbcontext.Chapitre.Find(id);

                //transform to DTO, and send to upper layer
                return chapitreEntity.ToDto();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                throw;
            }
        }

        /// <summary>
        /// Adds a new Chaptrer to the database
        /// </summary>
        /// <param name="chapitre">The new Chapter to add</param>
        public void AddChapitre(ChapitreDto chapitre)
        {
            Chapitre newChapitre = chapitre.ToEntity();
            _dbcontext.Chapitre.Add(newChapitre);
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Updates an existing Chapter in the databse
        /// </summary>
        /// <param name="chapitre">The Chapter to update</param>
        public void UpdateChapitre(ChapitreDto chapitre)
        {
            Chapitre newChapitre = chapitre.ToEntity();

            _dbcontext.Entry(newChapitre).State = EntityState.Modified;
            _dbcontext.SaveChanges();
        }

        /// <summary>
        /// Deletes the Chapter with the given id from the database 
        /// </summary>
        /// <param name="id">The id of the Chapter to delete</param>
        public void DeleteChapitre(int id)
        {
            var chapitre = _dbcontext.Chapitre.Find(id);
            if (chapitre != null)
            {
                _dbcontext.Chapitre.Remove(chapitre);
                _dbcontext.SaveChanges();
            }
        }

        public void Dispose()
        {
            _dbcontext.Dispose();
        }
    }
}
