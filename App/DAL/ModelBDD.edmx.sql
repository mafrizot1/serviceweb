
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 01/31/2020 23:52:27
-- Generated from EDMX file: C:\Users\flavi\Documents\GitHub\serviceweb\App\DAL\ModelBDD.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ServiceWebDatabase];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ChapitreChoix_Chapitre]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChapitreChoix] DROP CONSTRAINT [FK_ChapitreChoix_Chapitre];
GO
IF OBJECT_ID(N'[dbo].[FK_ChapitreEvenement_Chapitre]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChapitreEvenement] DROP CONSTRAINT [FK_ChapitreEvenement_Chapitre];
GO
IF OBJECT_ID(N'[dbo].[FK_ChapitrePersonnage_Chapitre]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChapitrePersonnage] DROP CONSTRAINT [FK_ChapitrePersonnage_Chapitre];
GO
IF OBJECT_ID(N'[dbo].[FK_Choix_ProchainChapitre]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Choix] DROP CONSTRAINT [FK_Choix_ProchainChapitre];
GO
IF OBJECT_ID(N'[dbo].[FK_ChapitreChoix_Choix]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChapitreChoix] DROP CONSTRAINT [FK_ChapitreChoix_Choix];
GO
IF OBJECT_ID(N'[dbo].[FK_ChapitreEvenement_Evenement]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChapitreEvenement] DROP CONSTRAINT [FK_ChapitreEvenement_Evenement];
GO
IF OBJECT_ID(N'[dbo].[FK_ChapitrePersonnage_Personnage]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChapitrePersonnage] DROP CONSTRAINT [FK_ChapitrePersonnage_Personnage];
GO
IF OBJECT_ID(N'[dbo].[FK_Evenement_Emplacement]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Evenement] DROP CONSTRAINT [FK_Evenement_Emplacement];
GO
IF OBJECT_ID(N'[dbo].[FK_Evenement_Indice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Evenement] DROP CONSTRAINT [FK_Evenement_Indice];
GO
IF OBJECT_ID(N'[dbo].[FK_Evenement_Objet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Evenement] DROP CONSTRAINT [FK_Evenement_Objet];
GO
IF OBJECT_ID(N'[dbo].[FK_Evenement_Sortilege]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Evenement] DROP CONSTRAINT [FK_Evenement_Sortilege];
GO
IF OBJECT_ID(N'[dbo].[FK_Inventaire_Joueur]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inventaire] DROP CONSTRAINT [FK_Inventaire_Joueur];
GO
IF OBJECT_ID(N'[dbo].[FK_Inventaire_Objet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Inventaire] DROP CONSTRAINT [FK_Inventaire_Objet];
GO
IF OBJECT_ID(N'[dbo].[FK_JoueurSortilege_Joueur]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JoueurSortilege] DROP CONSTRAINT [FK_JoueurSortilege_Joueur];
GO
IF OBJECT_ID(N'[dbo].[FK_JoueurSortilege_Sortilege]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[JoueurSortilege] DROP CONSTRAINT [FK_JoueurSortilege_Sortilege];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonnageObjet_Objet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PersonnageObjet] DROP CONSTRAINT [FK_PersonnageObjet_Objet];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonnageObjet_Personnage]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PersonnageObjet] DROP CONSTRAINT [FK_PersonnageObjet_Personnage];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonnageSortilege_Personnage]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PersonnageSortilege] DROP CONSTRAINT [FK_PersonnageSortilege_Personnage];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonnageSortilege_Sortilege]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PersonnageSortilege] DROP CONSTRAINT [FK_PersonnageSortilege_Sortilege];
GO
IF OBJECT_ID(N'[dbo].[FK_Historique1_Chapitre]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Historique1] DROP CONSTRAINT [FK_Historique1_Chapitre];
GO
IF OBJECT_ID(N'[dbo].[FK_Historique1_Joueur]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Historique1] DROP CONSTRAINT [FK_Historique1_Joueur];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Historique]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Historique];
GO
IF OBJECT_ID(N'[dbo].[Chapitre]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Chapitre];
GO
IF OBJECT_ID(N'[dbo].[ChapitreChoix]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ChapitreChoix];
GO
IF OBJECT_ID(N'[dbo].[ChapitreEvenement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ChapitreEvenement];
GO
IF OBJECT_ID(N'[dbo].[ChapitrePersonnage]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ChapitrePersonnage];
GO
IF OBJECT_ID(N'[dbo].[Choix]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Choix];
GO
IF OBJECT_ID(N'[dbo].[Emplacement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Emplacement];
GO
IF OBJECT_ID(N'[dbo].[Evenement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Evenement];
GO
IF OBJECT_ID(N'[dbo].[Indice]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Indice];
GO
IF OBJECT_ID(N'[dbo].[Inventaire]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Inventaire];
GO
IF OBJECT_ID(N'[dbo].[Joueur]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Joueur];
GO
IF OBJECT_ID(N'[dbo].[JoueurSortilege]', 'U') IS NOT NULL
    DROP TABLE [dbo].[JoueurSortilege];
GO
IF OBJECT_ID(N'[dbo].[Objet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Objet];
GO
IF OBJECT_ID(N'[dbo].[Personnage]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Personnage];
GO
IF OBJECT_ID(N'[dbo].[PersonnageObjet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PersonnageObjet];
GO
IF OBJECT_ID(N'[dbo].[PersonnageSortilege]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PersonnageSortilege];
GO
IF OBJECT_ID(N'[dbo].[Sortilege]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sortilege];
GO
IF OBJECT_ID(N'[dbo].[Historique1]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Historique1];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Historique'
CREATE TABLE [dbo].[Historique] (
    [JoueurId] int  NOT NULL,
    [ChapitreId] int  NOT NULL
);
GO

-- Creating table 'Chapitre'
CREATE TABLE [dbo].[Chapitre] (
    [Id] int  NOT NULL,
    [Numero] int  NOT NULL,
    [Histoire] nchar(200)  NULL
);
GO

-- Creating table 'ChapitreChoix'
CREATE TABLE [dbo].[ChapitreChoix] (
    [Id] int  NOT NULL,
    [Chapitre_Id] int  NOT NULL,
    [Choix_Id] int  NOT NULL
);
GO

-- Creating table 'ChapitreEvenement'
CREATE TABLE [dbo].[ChapitreEvenement] (
    [Id] int  NOT NULL,
    [Chapitre_Id] int  NOT NULL,
    [Evenement_Id] int  NOT NULL
);
GO

-- Creating table 'ChapitrePersonnage'
CREATE TABLE [dbo].[ChapitrePersonnage] (
    [Id] int  NOT NULL,
    [Chapitre_Id] int  NOT NULL,
    [Personnage_Id] int  NOT NULL
);
GO

-- Creating table 'Choix'
CREATE TABLE [dbo].[Choix] (
    [Id] int  NOT NULL,
    [Contenu] nchar(200)  NOT NULL,
    [Chapitre_Id] int  NULL
);
GO

-- Creating table 'Emplacement'
CREATE TABLE [dbo].[Emplacement] (
    [Id] int  NOT NULL,
    [X] int  NOT NULL,
    [Y] int  NOT NULL,
    [Hauteur] int  NOT NULL,
    [Largeur] int  NOT NULL
);
GO

-- Creating table 'Evenement'
CREATE TABLE [dbo].[Evenement] (
    [Id] int  NOT NULL,
    [Description] nchar(200)  NULL,
    [Emplacement_Id] int  NOT NULL,
    [Indice_Id] int  NOT NULL,
    [Objet_Id] int  NOT NULL,
    [Sortilege_Id] int  NOT NULL
);
GO

-- Creating table 'Indice'
CREATE TABLE [dbo].[Indice] (
    [Id] int  NOT NULL,
    [Nom] nchar(50)  NOT NULL,
    [Description] nchar(200)  NULL
);
GO

-- Creating table 'Inventaire'
CREATE TABLE [dbo].[Inventaire] (
    [Id] int  NOT NULL,
    [Joueur_Id] int  NOT NULL,
    [Objet_Id] int  NOT NULL
);
GO

-- Creating table 'Joueur'
CREATE TABLE [dbo].[Joueur] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nom] nvarchar(50)  NOT NULL,
    [PV] int  NOT NULL,
    [Attaque] int  NOT NULL,
    [Defense] int  NOT NULL
);
GO

-- Creating table 'JoueurSortilege'
CREATE TABLE [dbo].[JoueurSortilege] (
    [Id] int  NOT NULL,
    [Joueur_Id] int  NOT NULL,
    [Sortilege_Id] int  NOT NULL
);
GO

-- Creating table 'Objet'
CREATE TABLE [dbo].[Objet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nom] nvarchar(50)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Valeur] int  NOT NULL,
    [Type] nvarchar(20)  NOT NULL
);
GO

-- Creating table 'Personnage'
CREATE TABLE [dbo].[Personnage] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nom] nchar(50)  NOT NULL,
    [Pv] int  NULL,
    [Attaque] int  NULL,
    [Defense] int  NULL,
    [Dialogue] nchar(200)  NULL,
    [Type] int  NOT NULL
);
GO

-- Creating table 'PersonnageObjet'
CREATE TABLE [dbo].[PersonnageObjet] (
    [Id] int  NOT NULL,
    [Objet_Id] int  NOT NULL,
    [Personnage_Id] int  NOT NULL
);
GO

-- Creating table 'PersonnageSortilege'
CREATE TABLE [dbo].[PersonnageSortilege] (
    [Id] int  NOT NULL,
    [Personnage_Id] int  NOT NULL,
    [Sortilege_Id] int  NOT NULL
);
GO

-- Creating table 'Sortilege'
CREATE TABLE [dbo].[Sortilege] (
    [Id] int  NOT NULL,
    [Nom] nchar(50)  NOT NULL,
    [Description] nchar(200)  NULL,
    [Effet] nchar(200)  NULL,
    [Puissance] int  NULL,
    [Type] int  NOT NULL
);
GO

-- Creating table 'Historique1'
CREATE TABLE [dbo].[Historique1] (
    [Chapitre_Id] int  NOT NULL,
    [Joueur_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [JoueurId], [ChapitreId] in table 'Historique'
ALTER TABLE [dbo].[Historique]
ADD CONSTRAINT [PK_Historique]
    PRIMARY KEY CLUSTERED ([JoueurId], [ChapitreId] ASC);
GO

-- Creating primary key on [Id] in table 'Chapitre'
ALTER TABLE [dbo].[Chapitre]
ADD CONSTRAINT [PK_Chapitre]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ChapitreChoix'
ALTER TABLE [dbo].[ChapitreChoix]
ADD CONSTRAINT [PK_ChapitreChoix]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ChapitreEvenement'
ALTER TABLE [dbo].[ChapitreEvenement]
ADD CONSTRAINT [PK_ChapitreEvenement]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ChapitrePersonnage'
ALTER TABLE [dbo].[ChapitrePersonnage]
ADD CONSTRAINT [PK_ChapitrePersonnage]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Choix'
ALTER TABLE [dbo].[Choix]
ADD CONSTRAINT [PK_Choix]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Emplacement'
ALTER TABLE [dbo].[Emplacement]
ADD CONSTRAINT [PK_Emplacement]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Evenement'
ALTER TABLE [dbo].[Evenement]
ADD CONSTRAINT [PK_Evenement]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Indice'
ALTER TABLE [dbo].[Indice]
ADD CONSTRAINT [PK_Indice]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Inventaire'
ALTER TABLE [dbo].[Inventaire]
ADD CONSTRAINT [PK_Inventaire]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Joueur'
ALTER TABLE [dbo].[Joueur]
ADD CONSTRAINT [PK_Joueur]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'JoueurSortilege'
ALTER TABLE [dbo].[JoueurSortilege]
ADD CONSTRAINT [PK_JoueurSortilege]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Objet'
ALTER TABLE [dbo].[Objet]
ADD CONSTRAINT [PK_Objet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Personnage'
ALTER TABLE [dbo].[Personnage]
ADD CONSTRAINT [PK_Personnage]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PersonnageObjet'
ALTER TABLE [dbo].[PersonnageObjet]
ADD CONSTRAINT [PK_PersonnageObjet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PersonnageSortilege'
ALTER TABLE [dbo].[PersonnageSortilege]
ADD CONSTRAINT [PK_PersonnageSortilege]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Sortilege'
ALTER TABLE [dbo].[Sortilege]
ADD CONSTRAINT [PK_Sortilege]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Chapitre_Id], [Joueur_Id] in table 'Historique1'
ALTER TABLE [dbo].[Historique1]
ADD CONSTRAINT [PK_Historique1]
    PRIMARY KEY CLUSTERED ([Chapitre_Id], [Joueur_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Chapitre_Id] in table 'ChapitreChoix'
ALTER TABLE [dbo].[ChapitreChoix]
ADD CONSTRAINT [FK_ChapitreChoix_Chapitre]
    FOREIGN KEY ([Chapitre_Id])
    REFERENCES [dbo].[Chapitre]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ChapitreChoix_Chapitre'
CREATE INDEX [IX_FK_ChapitreChoix_Chapitre]
ON [dbo].[ChapitreChoix]
    ([Chapitre_Id]);
GO

-- Creating foreign key on [Chapitre_Id] in table 'ChapitreEvenement'
ALTER TABLE [dbo].[ChapitreEvenement]
ADD CONSTRAINT [FK_ChapitreEvenement_Chapitre]
    FOREIGN KEY ([Chapitre_Id])
    REFERENCES [dbo].[Chapitre]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ChapitreEvenement_Chapitre'
CREATE INDEX [IX_FK_ChapitreEvenement_Chapitre]
ON [dbo].[ChapitreEvenement]
    ([Chapitre_Id]);
GO

-- Creating foreign key on [Chapitre_Id] in table 'ChapitrePersonnage'
ALTER TABLE [dbo].[ChapitrePersonnage]
ADD CONSTRAINT [FK_ChapitrePersonnage_Chapitre]
    FOREIGN KEY ([Chapitre_Id])
    REFERENCES [dbo].[Chapitre]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ChapitrePersonnage_Chapitre'
CREATE INDEX [IX_FK_ChapitrePersonnage_Chapitre]
ON [dbo].[ChapitrePersonnage]
    ([Chapitre_Id]);
GO

-- Creating foreign key on [Chapitre_Id] in table 'Choix'
ALTER TABLE [dbo].[Choix]
ADD CONSTRAINT [FK_Choix_ProchainChapitre]
    FOREIGN KEY ([Chapitre_Id])
    REFERENCES [dbo].[Chapitre]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Choix_ProchainChapitre'
CREATE INDEX [IX_FK_Choix_ProchainChapitre]
ON [dbo].[Choix]
    ([Chapitre_Id]);
GO

-- Creating foreign key on [Choix_Id] in table 'ChapitreChoix'
ALTER TABLE [dbo].[ChapitreChoix]
ADD CONSTRAINT [FK_ChapitreChoix_Choix]
    FOREIGN KEY ([Choix_Id])
    REFERENCES [dbo].[Choix]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ChapitreChoix_Choix'
CREATE INDEX [IX_FK_ChapitreChoix_Choix]
ON [dbo].[ChapitreChoix]
    ([Choix_Id]);
GO

-- Creating foreign key on [Evenement_Id] in table 'ChapitreEvenement'
ALTER TABLE [dbo].[ChapitreEvenement]
ADD CONSTRAINT [FK_ChapitreEvenement_Evenement]
    FOREIGN KEY ([Evenement_Id])
    REFERENCES [dbo].[Evenement]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ChapitreEvenement_Evenement'
CREATE INDEX [IX_FK_ChapitreEvenement_Evenement]
ON [dbo].[ChapitreEvenement]
    ([Evenement_Id]);
GO

-- Creating foreign key on [Personnage_Id] in table 'ChapitrePersonnage'
ALTER TABLE [dbo].[ChapitrePersonnage]
ADD CONSTRAINT [FK_ChapitrePersonnage_Personnage]
    FOREIGN KEY ([Personnage_Id])
    REFERENCES [dbo].[Personnage]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ChapitrePersonnage_Personnage'
CREATE INDEX [IX_FK_ChapitrePersonnage_Personnage]
ON [dbo].[ChapitrePersonnage]
    ([Personnage_Id]);
GO

-- Creating foreign key on [Emplacement_Id] in table 'Evenement'
ALTER TABLE [dbo].[Evenement]
ADD CONSTRAINT [FK_Evenement_Emplacement]
    FOREIGN KEY ([Emplacement_Id])
    REFERENCES [dbo].[Emplacement]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Evenement_Emplacement'
CREATE INDEX [IX_FK_Evenement_Emplacement]
ON [dbo].[Evenement]
    ([Emplacement_Id]);
GO

-- Creating foreign key on [Indice_Id] in table 'Evenement'
ALTER TABLE [dbo].[Evenement]
ADD CONSTRAINT [FK_Evenement_Indice]
    FOREIGN KEY ([Indice_Id])
    REFERENCES [dbo].[Indice]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Evenement_Indice'
CREATE INDEX [IX_FK_Evenement_Indice]
ON [dbo].[Evenement]
    ([Indice_Id]);
GO

-- Creating foreign key on [Objet_Id] in table 'Evenement'
ALTER TABLE [dbo].[Evenement]
ADD CONSTRAINT [FK_Evenement_Objet]
    FOREIGN KEY ([Objet_Id])
    REFERENCES [dbo].[Objet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Evenement_Objet'
CREATE INDEX [IX_FK_Evenement_Objet]
ON [dbo].[Evenement]
    ([Objet_Id]);
GO

-- Creating foreign key on [Sortilege_Id] in table 'Evenement'
ALTER TABLE [dbo].[Evenement]
ADD CONSTRAINT [FK_Evenement_Sortilege]
    FOREIGN KEY ([Sortilege_Id])
    REFERENCES [dbo].[Sortilege]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Evenement_Sortilege'
CREATE INDEX [IX_FK_Evenement_Sortilege]
ON [dbo].[Evenement]
    ([Sortilege_Id]);
GO

-- Creating foreign key on [Joueur_Id] in table 'Inventaire'
ALTER TABLE [dbo].[Inventaire]
ADD CONSTRAINT [FK_Inventaire_Joueur]
    FOREIGN KEY ([Joueur_Id])
    REFERENCES [dbo].[Joueur]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inventaire_Joueur'
CREATE INDEX [IX_FK_Inventaire_Joueur]
ON [dbo].[Inventaire]
    ([Joueur_Id]);
GO

-- Creating foreign key on [Objet_Id] in table 'Inventaire'
ALTER TABLE [dbo].[Inventaire]
ADD CONSTRAINT [FK_Inventaire_Objet]
    FOREIGN KEY ([Objet_Id])
    REFERENCES [dbo].[Objet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Inventaire_Objet'
CREATE INDEX [IX_FK_Inventaire_Objet]
ON [dbo].[Inventaire]
    ([Objet_Id]);
GO

-- Creating foreign key on [Joueur_Id] in table 'JoueurSortilege'
ALTER TABLE [dbo].[JoueurSortilege]
ADD CONSTRAINT [FK_JoueurSortilege_Joueur]
    FOREIGN KEY ([Joueur_Id])
    REFERENCES [dbo].[Joueur]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JoueurSortilege_Joueur'
CREATE INDEX [IX_FK_JoueurSortilege_Joueur]
ON [dbo].[JoueurSortilege]
    ([Joueur_Id]);
GO

-- Creating foreign key on [Sortilege_Id] in table 'JoueurSortilege'
ALTER TABLE [dbo].[JoueurSortilege]
ADD CONSTRAINT [FK_JoueurSortilege_Sortilege]
    FOREIGN KEY ([Sortilege_Id])
    REFERENCES [dbo].[Sortilege]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_JoueurSortilege_Sortilege'
CREATE INDEX [IX_FK_JoueurSortilege_Sortilege]
ON [dbo].[JoueurSortilege]
    ([Sortilege_Id]);
GO

-- Creating foreign key on [Objet_Id] in table 'PersonnageObjet'
ALTER TABLE [dbo].[PersonnageObjet]
ADD CONSTRAINT [FK_PersonnageObjet_Objet]
    FOREIGN KEY ([Objet_Id])
    REFERENCES [dbo].[Objet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonnageObjet_Objet'
CREATE INDEX [IX_FK_PersonnageObjet_Objet]
ON [dbo].[PersonnageObjet]
    ([Objet_Id]);
GO

-- Creating foreign key on [Personnage_Id] in table 'PersonnageObjet'
ALTER TABLE [dbo].[PersonnageObjet]
ADD CONSTRAINT [FK_PersonnageObjet_Personnage]
    FOREIGN KEY ([Personnage_Id])
    REFERENCES [dbo].[Personnage]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonnageObjet_Personnage'
CREATE INDEX [IX_FK_PersonnageObjet_Personnage]
ON [dbo].[PersonnageObjet]
    ([Personnage_Id]);
GO

-- Creating foreign key on [Personnage_Id] in table 'PersonnageSortilege'
ALTER TABLE [dbo].[PersonnageSortilege]
ADD CONSTRAINT [FK_PersonnageSortilege_Personnage]
    FOREIGN KEY ([Personnage_Id])
    REFERENCES [dbo].[Personnage]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonnageSortilege_Personnage'
CREATE INDEX [IX_FK_PersonnageSortilege_Personnage]
ON [dbo].[PersonnageSortilege]
    ([Personnage_Id]);
GO

-- Creating foreign key on [Sortilege_Id] in table 'PersonnageSortilege'
ALTER TABLE [dbo].[PersonnageSortilege]
ADD CONSTRAINT [FK_PersonnageSortilege_Sortilege]
    FOREIGN KEY ([Sortilege_Id])
    REFERENCES [dbo].[Sortilege]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonnageSortilege_Sortilege'
CREATE INDEX [IX_FK_PersonnageSortilege_Sortilege]
ON [dbo].[PersonnageSortilege]
    ([Sortilege_Id]);
GO

-- Creating foreign key on [Chapitre_Id] in table 'Historique1'
ALTER TABLE [dbo].[Historique1]
ADD CONSTRAINT [FK_Historique1_Chapitre]
    FOREIGN KEY ([Chapitre_Id])
    REFERENCES [dbo].[Chapitre]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Joueur_Id] in table 'Historique1'
ALTER TABLE [dbo].[Historique1]
ADD CONSTRAINT [FK_Historique1_Joueur]
    FOREIGN KEY ([Joueur_Id])
    REFERENCES [dbo].[Joueur]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Historique1_Joueur'
CREATE INDEX [IX_FK_Historique1_Joueur]
ON [dbo].[Historique1]
    ([Joueur_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------