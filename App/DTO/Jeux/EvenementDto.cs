﻿using System;

namespace DTO
{
    public sealed class EvenementDto : IEquatable<EvenementDto>
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public EmplacementDto Emplacement { get; set; }
        public ChapitreDto Chapitre { get; set; }
        public ICliquable Cliquable { get; set; }

        public bool Equals(EvenementDto other)
        {
            return other != null && this.Id == other.Id;
        }
    }
}
