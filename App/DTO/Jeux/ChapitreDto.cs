﻿using System;
using System.Collections.Generic;

namespace DTO
{
    public sealed class ChapitreDto : IEquatable<ChapitreDto>
    {
        public int Id { get; set; }
        public int Numero { get; set; }
        public string Histoire { get; set; }

        public List<ChoixDto> Choix { get; set; } = new List<ChoixDto>();
        public List<ChoixDto> ChoixPrec { get; set; } = new List<ChoixDto>();
        public List<EvenementDto> Evenements { get; set; } = new List<EvenementDto>();

        public bool Equals(ChapitreDto other)
        {
            if (this.Id == other.Id)
            {
                return true;
            }
            return false;
        }
    }
}
