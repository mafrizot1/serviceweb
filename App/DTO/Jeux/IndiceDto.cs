﻿namespace DTO
{
    public class IndiceDto : ICliquable
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
    }
}
