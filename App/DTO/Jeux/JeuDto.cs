﻿using System.Collections.Generic;

namespace DTO
{
    public class JeuDto
    {
        public JoueurDto Joueur { get; set; }

        public List<ChapitreDto> Histoire { get; set; }

        public List<ChapitreDto> Historique { get; set; } = new List<ChapitreDto>();
    }
}
