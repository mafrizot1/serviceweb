﻿namespace DTO
{
    public class ChoixDto
    {
        public int Id { get; set; }
        public string Contenu { get; set; }

        public ChapitreDto ProchainChapitre { get; set; } = new ChapitreDto();
        public ChapitreDto ChapitrePrec { get; set; } = new ChapitreDto();
    }
}