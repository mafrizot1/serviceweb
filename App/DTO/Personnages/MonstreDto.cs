﻿using System;
using System.Collections.Generic;

namespace DTO
{
    public class MonstreDto : PersonnageDto, ICliquable
    {
        public int Pv { get; set; }
        public int Attaque { get; set; }
        public int Defense { get; set; }
        public int Probabilite { get; set; }

        public List<SortilegeDto> Sortileges { get; set; } = new List<SortilegeDto>();

        public String ContreAttaquer(JoueurDto joueur)
        {
            int degats = Math.Max(this.Attaque - joueur.Defense, 0);
            joueur.PV -= degats;

            return $"{Nom.Trim()} contre attaque ! {joueur.Nom.Trim()} prend {degats} points de dégâts !\n";
        }

        public bool EstMort()
        {
            return Pv <= 0;
        }
    }
}
