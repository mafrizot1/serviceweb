﻿using System.Collections.Generic;

namespace DTO
{
    public class JoueurDto : CombattantDto
    {
        public List<EvenementDto> Evenements { get; set; } = new List<EvenementDto>();

        public List<IndiceDto> Indices { get; set; } = new List<IndiceDto>();
    }
}
