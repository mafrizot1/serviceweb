﻿using System;
using System.Collections.Generic;

namespace DTO
{
    public abstract class CombattantDto : PersonnageDto
    {
        public int PV { get; set; }
        public int Attaque { get; set; }
        public int Defense { get; set; }

        public List<SortilegeDto> Sortileges { get; set; } = new List<SortilegeDto>();

        public string Attaquer(MonstreDto monstre)
        {
            int degats = Math.Max(this.Attaque - monstre.Defense, 0);
            monstre.Pv -= degats;

            return $"{Nom.Trim()} a infligé {degats} points de dégâts à {monstre.Nom.Trim()} !\n";
        }

        public string Utiliser(ObjetDto objet, MonstreDto monstre)
        {
            string message = $"{Nom} jette {objet.Nom}... Rien ne se passe !\n";

            if (objet.Type == "Consommable")
            {
                switch (objet.TypeEffet)
                {
                    case TypeEffetDto.ATTAQUE:
                        monstre.Pv -= objet.Puissance;

                        message = $"{Nom.Trim()} jette {objet.Nom.Trim()} et inflige {objet.Puissance} points de dégâts à {monstre.Nom.Trim()} !\n";
                        break;
                    case TypeEffetDto.DEFENSE:
                        PV += objet.Puissance;

                        message = $"{Nom.Trim()} jette {objet.Nom.Trim()} et récupère {objet.Puissance} points de vie !\n";
                        break;
                }
            }
            // objet.Type == "Equipement"
            else
            {
                switch (objet.TypeEffet)
                {
                    case TypeEffetDto.ATTAQUE:
                        Attaque -= objet.Puissance;
                        break;
                    case TypeEffetDto.DEFENSE:
                        Defense -= objet.Puissance;
                        break;
                }
            }

            return message;
        }

        public string Lancer(SortilegeDto sortilege, MonstreDto monstre)
        {
            string message = $"{Nom.Trim()} lance {sortilege.Nom.Trim()}... Rien ne se passe !\n";

            switch (sortilege)
            {
                case SortilegeOffensifDto sortilegeOffensif:
                    int degats = Math.Max(this.Attaque + sortilegeOffensif.Puissance - monstre.Defense, 0);
                    monstre.Pv -= degats;

                    message =  $"{Nom.Trim()} lance {sortilegeOffensif.Nom.Trim()} et inflige {sortilegeOffensif.Puissance + this.Attaque} points de dégâts à {monstre.Nom.Trim()} !\n";
                    break;
                case SortilegeDefensifDto sortilegeDefensif:
                    PV += sortilegeDefensif.Puissance;

                    message = $"{Nom.Trim()} lance {sortilegeDefensif.Nom.Trim()} et récupère {sortilegeDefensif.Puissance} points de vie !\n";
                    break;
                case SortilegeUtilitaireDto sortilegeUtilitaire:
                    message = $"{Nom.Trim()} lance {sortilegeUtilitaire.Nom.Trim()}, celà n'a absolument aucun effet !\n";
                    break;
            }

            return message;
        }

        public bool EstMort()
        {
            return PV <= 0;
        }
    }
}
