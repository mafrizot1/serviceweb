﻿using System.Collections.Generic;

namespace DTO
{
    public abstract class PersonnageDto
    {
        public int Id { get; set; }
        public string Nom { get; set; }

        public List<ObjetDto> Objets { get; set; } = new List<ObjetDto>();
        public List<ChapitreDto> Chapitres { get; set; } = new List<ChapitreDto>();
    }
}
