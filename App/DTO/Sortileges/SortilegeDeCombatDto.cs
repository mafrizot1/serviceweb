﻿namespace DTO
{
    public abstract class SortilegeDeCombatDto : SortilegeDto
    {
        public int Puissance { get; set; }
    }
}
