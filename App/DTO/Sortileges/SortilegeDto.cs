﻿using System;

namespace DTO
{
    public class SortilegeDto : ICliquable, IEquatable<SortilegeDto>
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }

        public bool Equals(SortilegeDto other)
        {
            return other != null && this.Id == other.Id;
        }
    }
}
