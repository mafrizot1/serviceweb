﻿namespace DTO
{
    public class SortilegeUtilitaireDto : SortilegeDto
    {
        public string Effet { get; set; }
    }
}
