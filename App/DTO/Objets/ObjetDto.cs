﻿using System;

namespace DTO
{
    public class ObjetDto : ICliquable, IEquatable<ObjetDto>
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public int Puissance { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public TypeEffetDto TypeEffet { get; set; }

        public bool Equals(ObjetDto other)
        {
            return other != null && this.Id == other.Id;
        }
    }
}
