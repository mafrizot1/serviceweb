﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DTO;
using System.Collections.Generic;

namespace UnitTest
{
    // !ATTENTION: Les tests devront tester les méthodes (comme si un joueur jouer au jeu) et pas la BDD --> Vérifier que lorsqu'un monstre est tué, il drop bien un objet dans certains cas.
    [TestClass]
    public class BasicTest
    {
        //FabriqueJeuDto _fj = new FabriqueJeuDto();
        JeuDto jeu;

        [TestMethod]
        public void GameCreation()
        {
            //jeu = (JeuDto)_fj.Create();
            jeu = new JeuDto();

            Assert.IsNotNull(jeu);
            Assert.AreEqual(1, jeu.Histoire.Count);
            Assert.IsTrue(jeu.Histoire[0].Choix.Count > 0);
        }


        [TestMethod]
        public void JoueurDto()
        {
            //jeu = (JeuDto)_fj.Create();
            jeu = new JeuDto();
            JoueurDto joueur = jeu.Joueur;

            // Nom du joueur
            Assert.IsNotNull(joueur.Nom);
            Assert.AreEqual("Harry Potter", jeu.Joueur.Nom);
        }
    }
}