﻿using System.Collections.Generic;

namespace API.Models
{
    public class ChapitreViewModel : ChapitreLittleViewModel
    {
        public string Histoire { get; set; }
        public List<ChoixLittleViewModel> Choix { get; set; } = new List<ChoixLittleViewModel>();
        public List<ChoixLittleViewModel> ChoixPrec { get; set; } = new List<ChoixLittleViewModel>();
        public List<EvenementLittleViewModel> Evenements { get; set; } = new List<EvenementLittleViewModel>();

    }
}