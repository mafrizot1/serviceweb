﻿using System.Collections.Generic;
using System.Linq;
using DTO;

namespace API.Models
{
    public class SortilegeViewModel : SortilegeLittleViewModel
    {
        public string Description { get; set; }
        public string Effet { get; set; }
        public int Puissance { get; set; }
        public int Type { get; set; }

        public static IEnumerable<SortilegeViewModel> ConvertSortileges(IEnumerable<SortilegeDto> sortileges)
        {
            return sortileges.Select(s => new SortilegeViewModel
            {
                Id = s.Id,
                Nom = s.Nom,
                Description = s.Description,
                Effet = (s is SortilegeUtilitaireDto) ? ((SortilegeUtilitaireDto)s).Effet : "",
                Puissance = (s is SortilegeDeCombatDto) ? ((SortilegeDeCombatDto)s).Puissance : 0,
                Type = s.Type
            });
        }
    }
}