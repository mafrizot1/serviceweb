﻿namespace API.Models
{
    public class EmplacementViewModel
    {
        public int Id { get; set; }
        public int Hauteur { get; set; }
        public int Largeur { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}