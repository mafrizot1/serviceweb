﻿namespace API.Models
{
    public class EvenementLittleViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
    }
}