﻿using System.Collections.Generic;

using DTO;

namespace API.Models
{
    public class JoueurViewModel
    {
        public int Id { get; set; }

        public string Nom { get; set; }

        public int PV { get; set; }

        public int Attaque { get; set; }

        public int Defense { get; set; }

        public List<int> ObjetIds { get; set; } = new List<int>();
        public List<ObjetDto> Objets { get; set; } = new List<ObjetDto>();

        public List<int> SortilegeIds { get; set; } = new List<int>();
        public List<SortilegeDto> Sortileges { get; set; } = new List<SortilegeDto>();

        public List<int> ChapitreIds { get; set; } = new List<int>();
        public List<ChapitreDto> Chapitres { get; set; } = new List<ChapitreDto>();

        public List<int> IndicesIds { get; set; } = new List<int>();
        public List<IndiceDto> Indices { get; set; } = new List<IndiceDto>();
    }
}