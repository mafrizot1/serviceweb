﻿namespace API.Models
{
    public class EquipementViewModel : ObjetLittleViewModel
    {
        public string Description { get; set; }
        public string Url { get; set; }
        public int Puissance { get; set; }
        public string Type { get; set; }
    }
}