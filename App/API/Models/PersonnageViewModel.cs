﻿using System.Collections.Generic;

namespace API.Models
{
    public class PersonnageViewModel : PersonnageLittleViewModel
    {
        public int Pv { get; set; }
        public int Attaque { get; set; }
        public int Defense { get; set; }
        public string Dialogue { get; set; }
        public int Type { get; set; }
        public int Probabilite { get; set; }

        public List<int> ObjetIds { get; set; } = new List<int>();
        public List<ObjetLittleViewModel> Objets { get; set; } = new List<ObjetLittleViewModel>();

        public List<int> ChapitreIds { get; set; } = new List<int>();
        public List<ChapitreLittleViewModel> Chapitres { get; set; } = new List<ChapitreLittleViewModel>();


        public List<int> SortilegeIds { get; set; } = new List<int>();
        public List<SortilegeLittleViewModel> Sortileges { get; set; } = new List<SortilegeLittleViewModel>();
    }
}