﻿namespace API.Models
{
    public class IndiceViewModel : ObjetLittleViewModel
    {
        public string Description { get; set; }
        public string URL { get; set; }
    }
}