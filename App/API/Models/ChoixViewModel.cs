﻿namespace API.Models
{
    public class ChoixViewModel : ChoixLittleViewModel
    {
        public int ChapitreId { get; set; }
        public int ChapitrePrecId { get; set; }
        public ChapitreLittleViewModel Chapitre { get; set; }
        public ChapitreLittleViewModel ChapitrePrec { get; set; }
    }
}