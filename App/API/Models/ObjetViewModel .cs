﻿using DTO;

namespace API.Models
{
    public class ObjetViewModel : ObjetLittleViewModel
    {
        public string Description { get; set; }
        public string URL { get; set; }
        public int Puissance { get; set; }
        public string Type { get; set; }
        public TypeEffetDto TypeEffet { get; set; }
    }
}