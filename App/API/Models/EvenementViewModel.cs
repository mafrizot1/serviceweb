﻿using System;
using DTO;

namespace API.Models
{
    public class EvenementViewModel : EvenementLittleViewModel
    {
        public int Emplacement_Id { get; set; }
        public EmplacementDto Emplacement { get; set; }
        public int Chapitre_Id { get; set; }
        public ChapitreLittleViewModel Chapitre { get; set; }

        public Nullable<int> Indice_Id { get; set; }
        public Nullable<int> Objet_Id { get; set; }
        public Nullable<int> Monstre_Id { get; set; }
        public Nullable<int> Sortilege_Id { get; set; }
        public ObjetLittleViewModel Indice { get; set; }
        public PersonnageLittleViewModel Monstre { get; set; }
        public ObjetLittleViewModel Objet { get; set; }
        public SortilegeLittleViewModel Sortilege { get; set; }
    }
}