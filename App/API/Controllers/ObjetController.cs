﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using BL;
using DTO;

namespace API.Controllers
{
    public class ObjetController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ObjetController"/> class.
        /// </summary>
        public ObjetController()
        {
        }

        /// <summary>
        /// Get list of all objects
        /// </summary>
        /// <remarks>
        /// Get list of all objects from the database no filtered
        /// </remarks>
        /// <response code="200">List of objects</response>
        [ResponseType(typeof(IEnumerable<ObjetViewModel>))]
        public IHttpActionResult Get()
        {
            IList<ObjetDto> objets = ObjetService.GetAllObjet();
            IEnumerable<ObjetViewModel> objetList = objets.Select(st => ConvertToVM(st)).ToList();

            return Ok(objetList);
        }

        /// <summary>
        /// Get an object
        /// </summary>
        /// <remarks>
        /// Get an object from the database via its id
        /// </remarks>
        /// <param name="id">The id of the object.</param>
        /// <response code="200">The object found</response>
        /// <response code="400"> parameter issue</response>
        public IHttpActionResult Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Objet Id is required");
            }
            var objet = ObjetService.GetObjet(id);
            var objetVM = ConvertToVM(objet);

            return Ok(objetVM);
        }

        /// <summary>
        /// Posts the specified object.
        /// </summary>
        /// <param name="objet">The object</param>
        /// <response code="200">Created</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Post([FromBody]ObjetViewModel objet)
        {
            if (objet == null)
            {
                return BadRequest("Objet Id is required");
            }

            try
            {
                ObjetDto newObjet = ConvertToDto(objet);
                ObjetService.AddObjet(newObjet);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Update an object
        /// </summary>
        /// <remarks>
        /// Update an object from the database via its id
        /// </remarks>
        /// <param name="id">The id of the object</param>
        /// <param name="objet">The object</param>
        /// <response code="200">Updated</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Put(int id, ObjetViewModel objet)
        {
            if (id <= 0)
            {
                return BadRequest("Objet Id is required");
            }

            try
            {
                ObjetDto newObjet = ConvertToDto(objet);
                newObjet.Id = id;

                ObjetService.UpdateObjet(newObjet);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Delete an object
        /// </summary>
        /// <remarks>
        /// Delete an object from the database via its id
        /// </remarks>
        /// <param name="id">The id of the object</param>
        /// <response code="200">Deleted</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Objet Id is required");
            }

            try
            {
                ObjetService.DeleteObjet(id);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Convert a view model object to a DTO object
        /// </summary>
        /// <param name="chapitre">View model of the object to convert to DTO</param>
        /// <returns>The object converted to DTO</returns>
        private ObjetDto ConvertToDto(ObjetViewModel objet)
        {
            return new ObjetDto
            {
                Id = objet.Id,
                Nom = objet.Nom,
                Description = objet.Description,
                Puissance = objet.Puissance,
                Type = objet.Type,
                Url = objet.URL,
                TypeEffet = objet.TypeEffet,
            };
        }

        private ObjetViewModel ConvertToVM(ObjetDto objet)
        {
            return new ObjetViewModel()
            {
                Id = objet.Id,
                Nom = objet.Nom,
                Description = objet.Description,
                Puissance = objet.Puissance,
                Type = objet.Type,
                URL = objet.Url,
                TypeEffet = objet.TypeEffet,
            };
        }
    }
}