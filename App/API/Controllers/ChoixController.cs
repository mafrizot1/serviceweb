﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using BL;
using DTO;

namespace API.Controllers
{
    public class ChoixController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChoixController"/> class.
        /// </summary>
        public ChoixController()
        {
        }

        /// <summary>
        /// Get list of all choices
        /// </summary>
        /// <remarks>
        /// Get list of all choices from the database (no filter)
        /// </remarks>
        /// <response code="200">List of choix</response>
        [ResponseType(typeof(IEnumerable<ChoixViewModel>))]
        public IHttpActionResult Get()
        {
            IList<ChoixDto> choix = ChoixService.GetAllChoix();
            IEnumerable<ChoixViewModel> choixList = choix.Select(st => new ChoixViewModel
            {
                Id = st.Id,
                Contenu = st.Contenu,
                ChapitreId = st.ProchainChapitre.Id,
                Chapitre = new ChapitreLittleViewModel
                {
                    Id = st.ProchainChapitre.Id,
                    Numero = st.ProchainChapitre.Numero
                },
                ChapitrePrecId = st.ChapitrePrec.Id,
                ChapitrePrec = new ChapitreLittleViewModel
                {
                    Id = st.ChapitrePrec.Id,
                    Numero = st.ChapitrePrec.Numero
                }
            });

            return Ok(choixList);
        }

        /// <summary>
        /// Get a choice
        /// </summary>
        /// <remarks>
        /// Get a choice from the database by its id
        /// </remarks>
        /// <param name="id">The id of the choice.</param>
        /// <response code="200">The choix found</response>
        /// <response code="400">Parameter issue</response>
        public IHttpActionResult Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Choix Id is required");
            }
            var choix = ChoixService.GetChoix(id);

            return Ok(choix);
        }

        /// <summary>
        /// Posts the specified choice.
        /// </summary>
        /// <param name="choix">The choice</param>
        /// <response code="200">Created</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Post([FromBody]ChoixViewModel choix)
        {
            if (choix == null)
            {
                return BadRequest("Choix Id is required");
            }

            try
            {
                ChoixDto newChoix = ConvertToDto(choix);
                ChoixService.AddChoix(newChoix);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Update a choices
        /// </summary>
        /// <remarks>
        /// Update a choice from the database via its id
        /// </remarks>
        /// <param name="id">The id of the choice</param>
        /// <param name="choix">The choice</param>
        /// <response code="200">Updated</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Put(int id, ChoixViewModel choix)
        {
            if (id <= 0)
            {
                return BadRequest("Choix Id is required");
            }

            try
            {
                ChoixDto newChoix = ConvertToDto(choix);
                newChoix.Id = id;

                ChoixService.UpdateChoix(newChoix);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Delete a choice
        /// </summary>
        /// <remarks>
        /// Delete a choice from the database via its id
        /// </remarks>
        /// <param name="id">The id of the choice</param>
        /// <response code="200">Deleted</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Choix Id is required");
            }

            try
            {
                ChoixService.DeleteChoix(id);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Convert a view model choice to a DTO choice
        /// </summary>
        /// <param name="choix">View model of the choice to convert to DTO</param>
        /// <returns>The choice converted to DTO</returns>
        private ChoixDto ConvertToDto(ChoixViewModel choix)
        {
            return new ChoixDto
            {
                Contenu = choix.Contenu,
                ProchainChapitre = ChoixService.GetChapitre(choix.ChapitreId),
                ChapitrePrec = ChoixService.GetChapitre(choix.ChapitrePrecId),
            };
        }
    }
}
