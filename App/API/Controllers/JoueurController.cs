﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using BL;
using DTO;

namespace API.Controllers
{
    public class JoueurController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JoueurController"/> class.
        /// </summary>
        public JoueurController()
        {
        }

        /// <summary>
        /// Get list of all players 
        /// </summary>
        /// <remarks>
        /// Get list of all players from the database no filtered
        /// </remarks>
        /// <response code="200">List of players</response>
        [ResponseType(typeof(IEnumerable<JoueurViewModel>))]
        public IHttpActionResult Get()
        {
            IList<JoueurDto> joueurs = JoueurService.GetAllJoueur();
            IEnumerable<JoueurViewModel> joueurList = joueurs.Select(st => ConvertToVM(st));

            return Ok(joueurList);
        }

        /// <summary>
        /// Get a player
        /// </summary>
        /// <remarks>
        /// Get a player from the database via its id
        /// </remarks>
        /// <param name="id">The id of the player</param>
        /// <response code="200">The player found</response>
        /// <response code="400">Parameter issue</response>
        public IHttpActionResult Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Joueur Id is required");
            }
            var joueur = JoueurService.GetJoueur(id);
            var joueurVM = ConvertToVM(joueur);

            return Ok(joueurVM);
        }

        /// <summary>
        /// Posts the specified player.
        /// </summary>
        /// <param name="joueur">The player</param>
        /// <response code="200">Created</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Post([FromBody]JoueurViewModel joueur)
        {
            if (joueur == null)
            {
                return BadRequest("Joueur Id is required");
            }

            try
            {
                JoueurDto newJoueur = ConvertToDto(joueur);
                JoueurService.AddJoueur(newJoueur);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Update a player
        /// </summary>
        /// <remarks>
        /// Update a player from the database via its id
        /// </remarks>
        /// <param name="id">The id of the player</param>
        /// <param name="joueur">The joueur</param>
        /// <response code="200">Updated</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Put(int id, JoueurViewModel joueur)
        {
            if (id <= 0)
            {
                return BadRequest("Joueur Id is required");
            }

            try
            {
                joueur.Id = id;
                JoueurDto newJoueur = ConvertToDto(joueur);

                JoueurService.UpdateJoueur(newJoueur);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Delete a player
        /// </summary>
        /// <remarks>
        /// Delete a player from the database via its id
        /// </remarks>
        /// <param name="id">The id of the player</param>
        /// <response code="200">Deleted</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Joueur Id is required");
            }

            try
            {
                JoueurService.DeleteJoueur(id);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Convert a view model player to a DTO player
        /// </summary>
        /// <param name="chapitre">View model of the player to convert to DTO</param>
        /// <returns>The player converted to DTO</returns>
        private static JoueurDto ConvertToDto(JoueurViewModel joueur)
        {
            return new JoueurDto
            {
                Id = joueur.Id,
                Nom = joueur.Nom,
                PV = joueur.PV,
                Attaque = joueur.Attaque,
                Defense = joueur.Defense,
                Objets = JoueurService.GetObjets(joueur.ObjetIds),
                Sortileges = JoueurService.GetSortileges(joueur.SortilegeIds),
                Chapitres = JoueurService.GetChapitres(joueur.ChapitreIds),
                Indices = JoueurService.GetIndices(joueur.IndicesIds),
            };
        }

        private static JoueurViewModel ConvertToVM(JoueurDto st)
        {
            return new JoueurViewModel()
            {
                Id = st.Id,
                Nom = st.Nom,
                PV = st.PV,
                Attaque = st.Attaque,
                Defense = st.Defense,
                Objets = st.Objets,
                Sortileges = st.Sortileges,
                Indices = st.Indices,
                ObjetIds = st.Objets.Select(o => o.Id).ToList(),
                SortilegeIds = st.Sortileges.Select(s => s.Id).ToList(),
                IndicesIds = st.Indices.Select(i => i.Id).ToList(),
            };
        }
    }
}