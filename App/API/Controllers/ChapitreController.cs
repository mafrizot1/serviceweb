﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using BL;
using DTO;

namespace API.Controllers
{
    public class ChapitreController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChapitreController"/> class.
        /// </summary>
        public ChapitreController()
        {
        }

        /// <summary>
        /// Get list of all chapters
        /// </summary>
        /// <remarks>
        /// Get list of all chapters from the database (no filter)
        /// </remarks>
        /// <response code="200">List of chapters</response>
        [ResponseType(typeof(IEnumerable<ChapitreViewModel>))]
        public IHttpActionResult Get()
        {
            List<ChapitreDto> chapitres = ChapitreService.GetAllChapitre().ToList();
            List<ChapitreViewModel> chapitresVM = chapitres.Select(c => ConvertToVM(c)).ToList();

            return Ok(chapitresVM);
        }

        /// <summary>
        /// Get a chapter
        /// </summary>
        /// <remarks>
        /// Get a chapter from the database via its id
        /// </remarks>
        /// <param name="id">The id of the chapter</param>
        /// <response code="200">The chapter found</response>
        /// <response code="400">Parameter issue</response>
        [ResponseType(typeof(ChapitreViewModel))]
        public IHttpActionResult Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Chapitre Id is required");
            }
            var chapitre = ChapitreService.GetChapitre(id);
            var chapitreVM = ConvertToVM(chapitre);

            return Ok(chapitreVM);
        }

        /// <summary>
        /// Posts the specified chapter
        /// </summary>
        /// <param name="chapitre">The chapter</param>
        /// <response code="200">Created</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Post([FromBody]ChapitreViewModel chapitre)
        {
            if (chapitre == null)
            {
                return BadRequest("Chapitre Id is required");
            }

            try
            {
                ChapitreDto newChapitre = ConvertToDto(chapitre);
                ChapitreService.AddChapitre(newChapitre);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Update a chapter
        /// </summary>
        /// <remarks>
        /// Update a chapter from the database via its id
        /// </remarks>
        /// <param name="id">The id of the chapter</param>
        /// <param name="chapitre">The chapter</param>
        /// <response code="200">Updated</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Put(int id, ChapitreViewModel chapitre)
        {
            if (id <= 0)
            {
                return BadRequest("Chapitre Id is required");
            }

            try
            {
                ChapitreDto newChapitre = ConvertToDto(chapitre);
                newChapitre.Id = id;

                ChapitreService.UpdateChapitre(newChapitre);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Delete a chapter
        /// </summary>
        /// <remarks>
        /// Delete a chapter from the database via its id
        /// </remarks>
        /// <param name="id">The id of the chapter.</param>
        /// <response code="200"> Deleted</response>
        /// <response code="400"> parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Chapitre Id is required");
            }

            try
            {
                ChapitreService.DeleteChapitre(id);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Convert a view model chapter to a DTO chapter
        /// </summary>
        /// <param name="chapitre">View model of the chapter to convert to DTO</param>
        /// <returns>The chapter converted to DTO</returns>
        private ChapitreDto ConvertToDto(ChapitreViewModel chapitre)
        {
            List<EvenementDto> list = new List<EvenementDto>();
            return new ChapitreDto
            {
                Numero = chapitre.Numero,
                Histoire = chapitre.Histoire,
                Evenements = list,
            };
        }

        /// <summary>
        /// Convert a DTO chapter to a view model chapter
        /// </summary>
        /// <param name="chapitre">DTO of the chapter to convert to view model</param>
        /// <returns>The chapter converted to view model</returns>
        private ChapitreViewModel ConvertToVM(ChapitreDto chapitre)
        {
            var chapitreVM = new ChapitreViewModel
            {
                Id = chapitre.Id,
                Numero = chapitre.Numero,
                Histoire = chapitre.Histoire,
                Choix = chapitre.Choix.Select(c => new ChoixLittleViewModel()
                {
                    Id = c.Id,
                    Contenu = c.Contenu,
                }).ToList(),
                ChoixPrec = chapitre.ChoixPrec.Select(c => new ChoixLittleViewModel()
                {
                    Id = c.Id,
                    Contenu = c.Contenu,
                }).ToList(),
                Evenements = chapitre.Evenements.Select(e => new EvenementLittleViewModel()
                {
                    Id = e.Id,
                    Description = e.Description,
                    Type = e.Cliquable.ToString(),
                }).ToList(),
            };

            return chapitreVM;
        }
    }
}