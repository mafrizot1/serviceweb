﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using BL;
using DTO;

namespace API.Controllers
{
    public class PersonnageController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonnageController"/> class.
        /// </summary>
        public PersonnageController()
        {
        }

        /// <summary>
        /// Get list of all characters
        /// </summary>
        /// <remarks>
        /// Get list of all characters from the database no filtered
        /// </remarks>
        /// <response code="200">List of characters</response>
        [ResponseType(typeof(IEnumerable<PersonnageViewModel>))]
        public IHttpActionResult Get()
        {
            IList<PersonnageDto> personnages = PersonnageService.GetAllPersonnage();
            var personnagesVM = personnages.Select(p => ConvertToVM(p));
            return Ok(personnagesVM);
        }

        /// <summary>
        /// Get a character
        /// </summary>
        /// <remarks>
        /// Get a character from the database via its id
        /// </remarks>
        /// <param name="id">The id of the character</param>
        /// <response code="200">The character found</response>
        /// <response code="400">Parameter issue</response>
        public IHttpActionResult Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Personnage Id is required");
            }
            var personnage = PersonnageService.GetPersonnage(id);
            var personnageVM = ConvertToVM(personnage);
            return Ok(personnageVM);
        }

        /// <summary>
        /// Get list  
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <response code="200"></response>
        [Route("api/Monstre")]
        [ResponseType(typeof(IEnumerable<PersonnageViewModel>))]
        public IHttpActionResult GetMonstres()
        {
            IList<PersonnageDto> personnages = PersonnageService.GetAllPersonnage();
            var personnagesVM = personnages.Where(p => p is MonstreDto).Select(p => ConvertToVM(p));

            return Ok(personnagesVM);
        }

        /// <summary>
        /// Posts the specified character.
        /// </summary>
        /// <param name="personnage">The character.</param>
        /// <response code="200">Created</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Post([FromBody]PersonnageViewModel personnage)
        {
            if (personnage == null)
            {
                return BadRequest("Personnage Id is required");
            }

            try
            {
                PersonnageDto newPersonnage = ConvertToDto(personnage);
                PersonnageService.AddPersonnage(newPersonnage);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Update a character
        /// </summary>
        /// <remarks>
        /// Update a character from the database via its id
        /// </remarks>
        /// <param name="id">The id of the character</param>
        /// <param name="personnage">The character</param>
        /// <response code="200">Updated</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Put(int id, PersonnageViewModel personnage)
        {
            if (id <= 0)
            {
                return BadRequest("Personnage Id is required");
            }

            try
            {
                PersonnageDto newPersonnage = ConvertToDto(personnage);
                newPersonnage.Id = id;

                PersonnageService.UpdatePersonnage(newPersonnage);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Delete a character
        /// </summary>
        /// <remarks>
        /// Delete a character from the database via its id
        /// </remarks>
        /// <param name="id">The id of the character</param>
        /// <response code="200">Deleted</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Personnage Id is required");
            }

            try
            {
                PersonnageService.DeletePersonnage(id);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Convert a view model character to a DTO character
        /// </summary>
        /// <param name="chapitre">View model of the character to convert to DTO</param>
        /// <returns>The character converted to DTO</returns>
        private PersonnageDto ConvertToDto(PersonnageViewModel personnage)
        {
            switch (personnage.Type)
            {
                case 1:
                    return new PnjDto()
                    {
                        Id = personnage.Id,
                        Nom = personnage.Nom,
                        Dialogue = personnage.Dialogue,
                        Objets = PersonnageService.GetObjets(personnage.ObjetIds),
                        Chapitres = PersonnageService.GetChapitres(personnage.ChapitreIds),
                    };

                default:
                    return new MonstreDto()
                    {
                        Id = personnage.Id,
                        Nom = personnage.Nom,
                        Pv = personnage.Pv,
                        Attaque = personnage.Attaque,
                        Defense = personnage.Defense,
                        Probabilite = personnage.Probabilite,
                        Objets = PersonnageService.GetObjets(personnage.ObjetIds),
                        Sortileges = PersonnageService.GetSortileges(personnage.SortilegeIds),
                        Chapitres = PersonnageService.GetChapitres(personnage.ChapitreIds),
                    };
            }
        }

        /// <summary>
        /// Convert a DTO character to a view model character
        /// </summary>
        /// <param name="chapitre">DTO of the character to convert to view model</param>
        /// <returns>The character converted to view model</returns>
        private PersonnageViewModel ConvertToVM(PersonnageDto personnage)
        {
            var personnageVM = new PersonnageViewModel()
            {
                Id = personnage.Id,
                Type = personnage is PnjDto ? 1 : 0,
                Nom = personnage.Nom,
                Chapitres = personnage.Chapitres.Select(c => new ChapitreLittleViewModel()
                {
                    Id = c.Id,
                    Numero = c.Numero,
                }).ToList(),
                Objets = personnage.Objets.Select(o => new ObjetLittleViewModel()
                {
                    Id = o.Id,
                    Nom = o.Nom,
                }).ToList(),
            };


            if (personnageVM.Type == 1)
            {
                personnageVM.Dialogue = (personnage as PnjDto).Dialogue;
                personnageVM.Sortileges = new List<SortilegeLittleViewModel>();
            }
            else
            {
                personnageVM.Attaque = (personnage as MonstreDto).Attaque;
                personnageVM.Defense = (personnage as MonstreDto).Defense;
                personnageVM.Probabilite = (personnage as MonstreDto).Probabilite;
                personnageVM.Pv = (personnage as MonstreDto).Pv;
                personnageVM.Sortileges = (personnage as MonstreDto).Sortileges.Select(s => new SortilegeLittleViewModel()
                {
                    Id = s.Id,
                    Nom = s.Nom,
                }).ToList();
            }

            return personnageVM;
        }
    }
}
