﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using BL;
using DTO;

namespace API.Controllers
{
    public class IndiceController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IndiceController"/> class.
        /// </summary>
        public IndiceController()
        {
        }

        /// <summary>
        /// Get list of all hints
        /// </summary>
        /// <remarks>
        /// Get list of all hints from the database no filtered
        /// </remarks>
        /// <response code="200">List of hints</response>
        [ResponseType(typeof(IEnumerable<IndiceViewModel>))]
        public IHttpActionResult Get()
        {
            IList<IndiceDto> indices = IndiceService.GetAllIndice();
            IEnumerable<IndiceViewModel> indiceList = indices.Select(st => ConvertToVM(st));

            return Ok(indiceList);
        }

        /// <summary>
        /// Get a hint
        /// </summary>
        /// <remarks>
        /// Get a hint from the database via its id
        /// </remarks>
        /// <param name="id">The id of the hint</param>
        /// <response code="200">The hint found</response>
        /// <response code="400">Parameter issue</response>
        public IHttpActionResult Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Indice Id is required");
            }
            var indice = IndiceService.GetIndice(id);
            var indiceVM = ConvertToVM(indice);

            return Ok(indiceVM);
        }

        /// <summary>
        /// Posts the specified hint.
        /// </summary>
        /// <param name="indice">The hint</param>
        /// <response code="200">Created</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Post([FromBody]IndiceViewModel indice)
        {
            if (indice == null)
            {
                return BadRequest("Indice Id is required");
            }

            try
            {
                IndiceDto newIndice = ConvertToDto(indice);
                IndiceService.AddIndice(newIndice);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Update a hint
        /// </summary>
        /// <remarks>
        /// Update a hint from the database via its id
        /// </remarks>
        /// <param name="id">The id of the hint</param>
        /// <param name="indice">The hint</param>
        /// <response code="200">Updated</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Put(int id, IndiceViewModel indice)
        {
            if (id <= 0)
            {
                return BadRequest("Indice Id is required");
            }

            try
            {
                IndiceDto newIndice = ConvertToDto(indice);
                newIndice.Id = id;

                IndiceService.UpdateIndice(newIndice);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Delete a hint
        /// </summary>
        /// <remarks>
        /// Delete a hint from the database via its id
        /// </remarks>
        /// <param name="id">The id of the hint</param>
        /// <response code="200">Deleted</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Indice Id is required");
            }

            try
            {
                IndiceService.DeleteIndice(id);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Convert a view model hint to a DTO chapter
        /// </summary>
        /// <param name="indice">View model of the hint to convert to DTO</param>
        /// <returns>The hint converted to DTO</returns>
        private IndiceDto ConvertToDto(IndiceViewModel indice)
        {
            return new IndiceDto
            {
                Id = indice.Id,
                Nom = indice.Nom,
                Description = indice.Description,
                URL = indice.URL,
            };
        }

        private IndiceViewModel ConvertToVM(IndiceDto indice)
        {
            return new IndiceViewModel()
            {
                Id = indice.Id,
                Nom = indice.Nom,
                Description = indice.Description,
                URL = indice.URL,
            };
        }
    }
}