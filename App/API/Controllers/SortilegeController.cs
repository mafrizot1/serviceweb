﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

using API.Models;
using BL;
using DTO;

namespace API.Controllers
{
    public class SortilegeController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SortilegeController"/> class.
        /// </summary>
        public SortilegeController()
        {
        }

        /// <summary>
        /// Get list of all spells
        /// </summary>
        /// <remarks>
        /// Get list of all spells from the database no filtered
        /// </remarks>
        /// <response code="200">List of spells</response>
        [ResponseType(typeof(IEnumerable<SortilegeViewModel>))]
        public IHttpActionResult Get()
        {
            IList<SortilegeDto> sortileges = SortilegeService.GetAllSortilege();
            IEnumerable<SortilegeViewModel> sortilegeList = sortileges.Select(st => new SortilegeViewModel
            {
                Id = st.Id,
                Nom = st.Nom,
                Description = st.Description,
                //Puissance = st.Puissance,
                Type = st.Type,
            });

            return Ok(sortilegeList);
        }

        /// <summary>
        /// Get a spell
        /// </summary>
        /// <remarks>
        /// Get a spell from the database via its id
        /// </remarks>
        /// <param name="id">The id of the spell</param>
        /// <response code="200">The spell found</response>
        /// <response code="400">Parameter issue</response>
        public IHttpActionResult Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Sortilege Id is required");
            }
            var sortilege = SortilegeService.GetSortilege(id);

            return Ok(sortilege);
        }

        /// <summary>
        /// Posts the specified spell.
        /// </summary>
        /// <param name="sortilege">The spell</param>
        /// <response code="200">Created</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Post([FromBody]SortilegeViewModel sortilege)
        {
            if (sortilege == null)
            {
                return BadRequest("Sortilege Id is required");
            }

            try
            {
                SortilegeDto newSortilege = ConvertToDto(sortilege);
                SortilegeService.AddSortilege(newSortilege);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Update a spell
        /// </summary>
        /// <remarks>
        /// Update a spell from the database via its id
        /// </remarks>
        /// <param name="id">The id of the spell</param>
        /// <param name="sortilege">The spell</param>
        /// <response code="200">Updated</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Put(int id, SortilegeViewModel sortilege)
        {
            if (id <= 0)
            {
                return BadRequest("Sortilege Id is required");
            }

            try
            {
                SortilegeDto newSortilege = ConvertToDto(sortilege);
                newSortilege.Id = id;

                SortilegeService.UpdateSortilege(newSortilege);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Delete a spell
        /// </summary>
        /// <remarks>
        /// Delete a spell from the database via its id
        /// </remarks>
        /// <param name="id">The id of the spell</param>
        /// <response code="200">Deleted</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Sortilege Id is required");
            }

            try
            {
                SortilegeService.DeleteSortilege(id);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Convert a view model spell to a DTO spell
        /// </summary>
        /// <param name="chapitre">View model of the spell to convert to DTO</param>
        /// <returns>The spell converted to DTO</returns>
        private SortilegeDto ConvertToDto(SortilegeViewModel sortilege)
        {
            if (sortilege != null)
            {
                switch (sortilege.Type)
                {
                    case 1:
                        return new SortilegeUtilitaireDto
                        {
                            Id = sortilege.Id,
                            Nom = sortilege.Nom,
                            Description = sortilege.Description,
                            Effet = sortilege.Effet,
                            Type = sortilege.Type,
                            //Puissance = sortilege.Puissance,
                        };
                    case 2:
                        return new SortilegeOffensifDto
                        {
                            Id = sortilege.Id,
                            Nom = sortilege.Nom,
                            Description = sortilege.Description,
                            //Effet = sortilege.Effet,
                            Type = sortilege.Type,
                            Puissance = sortilege.Puissance,
                        };
                    case 3:
                        return new SortilegeDefensifDto
                        {
                            Id = sortilege.Id,
                            Nom = sortilege.Nom,
                            Description = sortilege.Description,
                            //Effet = sortilege.Effet,
                            Type = sortilege.Type,
                            Puissance = sortilege.Puissance,
                        };
                }
            }

            return null;
        }
    }
}
