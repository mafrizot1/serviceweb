﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using BL;
using DTO;

namespace API.Controllers
{
    /// <summary>
    /// Evenement Endpoint
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class EvenementController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EvenementController"/> class.
        /// </summary>
        public EvenementController()
        {
        }

        /// <summary>
        /// Get list of all events 
        /// </summary>
        /// <remarks>
        /// Get list of all events from the database no filtered
        /// </remarks>
        /// <response code="200">List of events</response>
        [ResponseType(typeof(IEnumerable<EvenementViewModel>))]
        public IHttpActionResult Get()
        {
            IList<EvenementDto> evenements = EvenementService.GetAllEvenement();
            IEnumerable<EvenementViewModel> evenementList = evenements.Select(st => ConvertToVM(st));

            return Ok(evenementList);
        }

        /// <summary>
        /// Get an event
        /// </summary>
        /// <remarks>
        /// Get an event from the database via its id
        /// </remarks>
        /// <param name="id">The id of the event</param>
        /// <response code="200">The event found</response>
        /// <response code="400">Parameter issue</response>
        public IHttpActionResult Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Evenement Id is required");
            }
            var evenement = ConvertToVM(EvenementService.GetEvenement(id));

            return Ok(evenement);
        }

        /// <summary>
        /// Posts the specified event.
        /// </summary>
        /// <param name="evenement">The event</param>
        /// <response code="200">Created</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Post([FromBody]EvenementViewModel evenement)
        {
            if (evenement == null)
            {
                return BadRequest("Evenement Id is required");
            }

            try
            {
                EvenementDto newEvenement = ConvertToDto(evenement);
                newEvenement.Emplacement = evenement.Emplacement;
                EvenementService.AddEvenement(newEvenement);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Update an event
        /// </summary>
        /// <remarks>
        /// Update an event from the database by his id
        /// </remarks>
        /// <param name="id">The id of the event</param>
        /// <param name="evenement">The event</param>
        /// <response code="200">Updated</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Put(int id, EvenementViewModel evenement)
        {
            if (id <= 0)
            {
                return BadRequest("Evenement Id is required");
            }

            try
            {
                evenement.Id = id;
                EvenementDto newEvenement = ConvertToDto(evenement);

                EvenementService.UpdateEvenement(newEvenement);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Delete an event
        /// </summary>
        /// <remarks>
        /// Delete an event from the database via its id
        /// </remarks>
        /// <param name="id">The id of the event</param>
        /// <response code="200">Deleted</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Evenement Id is required");
            }

            try
            {
                EvenementService.DeleteEvenement(id);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Convert a view model event to a DTO event
        /// </summary>
        /// <param name="evenement">View model of the event to convert to DTO</param>
        /// <returns>The event converted to DTO</returns>
        private EvenementDto ConvertToDto(EvenementViewModel evenement)
        {
            int type = 0;
            if (evenement.Indice_Id != null)
            {
                type = 1;
            }
            else if (evenement.Objet_Id != null)
            {
                type = 2;
            }
            else if (evenement.Sortilege_Id != null)
            {
                type = 3;
            }
            else if (evenement.Monstre_Id != null)
            {
                type = 4;
            }

            var evenementDTO = new EvenementDto
            {
                Id = evenement.Id,
                Description = evenement.Description,
                Emplacement = EvenementService.GetEmplacement(evenement.Emplacement_Id),
                Chapitre = EvenementService.GetChapitre(evenement.Chapitre_Id),
            };

            switch (type)
            {
                case 1:
                    evenementDTO.Cliquable = EvenementService.GetIndice(evenement.Indice_Id.Value);
                    break;
                case 2:
                    evenementDTO.Cliquable = EvenementService.GetObjet(evenement.Objet_Id.Value);
                    break;
                case 3:
                    evenementDTO.Cliquable = EvenementService.GetSortilege(evenement.Sortilege_Id.Value);
                    break;
                case 4:
                    evenementDTO.Cliquable = EvenementService.GetMonstre(evenement.Monstre_Id.Value);
                    break;
            }

            return evenementDTO;
        }

        /// <summary>
        /// Convert a DTO event to a view model event
        /// </summary>
        /// <param name="evenement">DTO of the event to convert to view model</param>
        /// <returns>The event converted to view model</returns>
        private EvenementViewModel ConvertToVM(EvenementDto evenement)
        {
            return new EvenementViewModel()
            {
                Id = evenement.Id,
                Description = evenement.Description,
                Emplacement_Id = evenement.Emplacement.Id,
                Emplacement = evenement.Emplacement,
                Chapitre = evenement.Chapitre != null ? new ChapitreLittleViewModel
                {
                    Id = evenement.Chapitre.Id,
                    Numero = evenement.Chapitre.Numero,
                } : null,
                Indice = (evenement.Cliquable is IndiceDto) ? new ObjetLittleViewModel
                {
                    Id = (evenement.Cliquable as IndiceDto).Id,
                    Nom = (evenement.Cliquable as IndiceDto).Nom,
                } : null,
                Objet = (evenement.Cliquable is ObjetDto) ? new ObjetLittleViewModel
                {
                    Id = (evenement.Cliquable as ObjetDto).Id,
                    Nom = (evenement.Cliquable as ObjetDto).Nom,
                } : null,
                Monstre = (evenement.Cliquable is MonstreDto) ? new PersonnageLittleViewModel
                {
                    Id = (evenement.Cliquable as MonstreDto).Id,
                    Nom = (evenement.Cliquable as MonstreDto).Nom,
                } : null,
                Sortilege = (evenement.Cliquable is SortilegeDto) ? new SortilegeLittleViewModel
                {
                    Id = (evenement.Cliquable as SortilegeDto).Id,
                    Nom = (evenement.Cliquable as SortilegeDto).Nom,
                } : null,
            };
        }
    }
}
