﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using BL;
using DTO;

namespace API.Controllers
{
    public class EmplacementController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmplacementController"/> class.
        /// </summary>
        public EmplacementController()
        {
        }

        /// <summary>
        /// Get list of all locations
        /// </summary>
        /// <remarks>
        /// Get list of all locations from the database (no filter)
        /// </remarks>
        /// <response code="200">List of locations</response>
        [ResponseType(typeof(IEnumerable<EmplacementViewModel>))]
        public IHttpActionResult Get()
        {
            IList<EmplacementDto> emplacements = EmplacementService.GetAllEmplacement();
            IEnumerable<EmplacementViewModel> emplacementList = emplacements.Select(st => new EmplacementViewModel
            {
                Id = st.Id,
                Hauteur = st.Hauteur,
                Largeur = st.Largeur,
                X = st.X,
                Y = st.Y,
            });

            return Ok(emplacementList);
        }

        /// <summary>
        /// Get a location
        /// </summary>
        /// <remarks>
        /// Get a location from the database via its id
        /// </remarks>
        /// <param name="id">The id of the location.</param>
        /// <response code="200">The emplacement found</response>
        /// <response code="400">Parameter issue</response>
        public IHttpActionResult Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Emplacement Id is required");
            }
            var emplacement = EmplacementService.GetEmplacement(id);

            return Ok(emplacement);
        }

        /// <summary>
        /// Posts the specified location.
        /// </summary>
        /// <param name="emplacement">The location</param>
        /// <response code="200">Created</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Post([FromBody]EmplacementViewModel emplacement)
        {
            if (emplacement == null)
            {
                return BadRequest("Emplacement Id is required");
            }

            try
            {
                EmplacementDto newEmplacement = ConvertToDto(emplacement);
                EmplacementService.AddEmplacement(newEmplacement);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Update a location
        /// </summary>
        /// <remarks>
        /// Update a location from the database via its id
        /// </remarks>
        /// <param name="id">The id of the location</param>
        /// <param name="emplacement">The location</param>
        /// <response code="200">Updated</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Put(int id, EmplacementViewModel emplacement)
        {
            if (id <= 0)
            {
                return BadRequest("Emplacement Id is required");
            }

            try
            {
                EmplacementDto newEmplacement = ConvertToDto(emplacement);
                newEmplacement.Id = id;

                EmplacementService.UpdateEmplacement(newEmplacement);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Delete a location
        /// </summary>
        /// <remarks>
        /// Delete a location from the database via its id
        /// </remarks>
        /// <param name="id">The id of the location</param>
        /// <response code="200">Deleted</response>
        /// <response code="400">Parameter issue</response>
        /// <response code="500">Other issues, see message included</response>
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Emplacement Id is required");
            }

            try
            {
                EmplacementService.DeleteEmplacement(id);

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Convert a view model location to a DTO location
        /// </summary>
        /// <param name="emplacement">View model of the location to convert to DTO</param>
        /// <returns>The location converted to DTO</returns>
        private EmplacementDto ConvertToDto(EmplacementViewModel emplacement)
        {
            return new EmplacementDto
            {
                Id = emplacement.Id,
                Hauteur = emplacement.Hauteur,
                Largeur = emplacement.Largeur,
                X = emplacement.X,
                Y = emplacement.Y,
            };
        }
    }
}