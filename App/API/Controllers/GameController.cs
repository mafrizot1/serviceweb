using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using API.Models;
using BL;
using DTO;

namespace API.Controllers
{
    public class GameController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameController"/> class.
        /// </summary>
        public GameController()
        {
        }

        /// <summary>
        /// Create a new game
        /// </summary>
        /// <param name="joueurVM">The player (containing its name)</param>
        /// <returns></returns>
        [Route("api/game/new")]
        public IHttpActionResult PostNewGame([FromBody]JoueurViewModel joueurVM)
        {
            if (joueurVM == null)
            {
                throw new ArgumentNullException(nameof(joueurVM));
            }

            bool exists = JoueurService.GetJoueurByName(joueurVM.Nom) != null;
            if (exists)
            {
                return BadRequest("Erreur, un joueur poss�de d�j� ce nom !");
            }
            else
            {
                JoueurDto joueur = new JoueurDto()
                {
                    Attaque = 10,
                    Defense = 10,
                    PV = 15,
                    Nom = joueurVM.Nom,
                    Chapitres = new List<ChapitreDto>(),
                    Objets = new List<ObjetDto>(),
                    Sortileges = new List<SortilegeDto>(),
                };
                JoueurService.AddJoueur(joueur);
                ChapitreDto chapitreVM = ChapitreService.GetAllChapitre().SingleOrDefault(c => c.Numero == 1);

                JoueurService.AddChapitre(joueur, chapitreVM);

                return Json(new { joueur, chapitreVM });
            }

        }

        /// <summary>
        /// Continue a previously started game
        /// </summary>
        /// <param name="joueurId">The id of the player</param>
        /// <returns></returns>
        [Route("api/game/continue/joueur/{joueurName}")]
        public IHttpActionResult GetContinue(string joueurName)
        {
            JoueurDto joueur = JoueurService.GetJoueurByName(joueurName);
            if (joueur == null)
            {
                return NotFound();
            }
            JoueurService.UpdateJoueur(joueur);
            ChapitreDto chapitre = joueur.Chapitres.Last();
            var chapitreVM = ConvertChapitreToVM(chapitre);

            // Recherche si un combat �tait en cours (Evenement non termine)
            EvenementDto evenement = JoueurService.GetEvenementEnCours(joueur);

            if (evenement != null)
            {
                // Retourner l'�v�nement non termin� et le combat
                MonstreDto monstre = (MonstreDto)evenement.Cliquable;
                return Json(new { joueur, chapitreVM, evenement, monstre });
            }


            // Si le chapitre n'a pas de chapitre suivant
            if (chapitre.Choix.Count == 0)
            {
                if (chapitre.Histoire.Trim() == "FINAL")
                {
                    var Victoire = "Vous avez gagn�, bravo !!!";
                    return Json(new { joueur, chapitreVM, Victoire });
                }
                else
                {
                    var GameOver = "Vous avez perdu, sacr�ment mauvais ;)";
                    return Json(new { joueur, chapitreVM, GameOver });
                }
            }

            return Json(new { joueur, chapitreVM });
        }

        /// <summary>
        /// Process a click on an event
        /// </summary>
        /// <param name="joueurId">The id of the player</param>
        /// <param name="eventId">The id of the event</param>
        /// <returns></returns>
        [Route("api/game/joueur/{joueurId}/evenement/{eventId}")]
        public IHttpActionResult GetClickedEvent(int joueurId, int eventId)
        {
            JoueurDto joueur = JoueurService.GetJoueur(joueurId);
            ChapitreDto chapitreVM = joueur.Chapitres.Last();
            EvenementDto evenement = JoueurService.GetEvenementEnCours(joueur);
            string messageErreur = "L'evenement demand� n'appartient pas au chapitre actuel !";

            // v�rification combat termin�
            if (evenement != null)
            {
                // Retourner l'�v�nement non termin� et le combat
                MonstreDto monstre = (MonstreDto)evenement.Cliquable;
                string message = $"{monstre.Nom} vous poursuit ! Il n'y a pas d'�chappatoire, il va falloir se battre !";
                return Json(new { joueur, chapitreVM, evenement, monstre, message });
            }

            evenement = chapitreVM.Evenements.Find(e => e.Id == eventId);

            // SI le chapitre demand� OU l'�v�nement d�mand� n'existe pas dans le chapitre
            if (evenement == null)
            {
                chapitreVM = joueur.Chapitres.Last();
                messageErreur = "L'evenement demand� n'appartient pas au chapitre actuel !";
                return Json(new { joueur, chapitreVM, messageErreur });
            }

            // Si le joueur n'a pas d�j� termin� l'�v�nement demand�
            if (!joueur.Evenements.Contains(evenement))
            {
                // Ajouter l'�v�nement au joueur et le mettre � termin�
                JoueurService.AddEvenement(joueur, evenement, true);
                switch (evenement.Cliquable)
                {
                    case MonstreDto monstre:
                        // Mettre l'�v�nement � non termin� (lancer un combat)
                        JoueurService.AddEvenement(joueur, evenement, false);
                        return Json(new { joueur, chapitreVM, evenement, monstre });

                    case PnjDto pnj:
                        return Json(new { joueur, chapitreVM, evenement, pnj });

                    case ObjetDto objet:
                        AjouterObjetJoueur(joueur, objet);
                        JoueurService.UpdateJoueur(joueur);
                        return Json(new { joueur, chapitreVM, evenement });

                    case SortilegeDto sortilege:
                        joueur.Sortileges.Add(sortilege);
                        JoueurService.UpdateJoueur(joueur);
                        return Json(new { joueur, chapitreVM, evenement });

                    case IndiceDto indice:
                        joueur.Indices.Add(indice);
                        JoueurService.UpdateJoueur(joueur);
                        return Json(new { joueur, chapitreVM, evenement, indice });
                }
            }
            else
            {
                messageErreur = "Vous avez d�j� compl�t� cet �v�nement !";
            }

            return Json(new { joueur, chapitreVM, messageErreur });
        }

        /// <summary>
        /// Process with the choice of the next chapter
        /// </summary>
        /// <param name="joueurId">The id of the player</param>
        /// <param name="choixId">The id of the choice chosen</param>
        /// <returns></returns>
        [Route("api/game/joueur/{joueurId}/choix/{choixId}")]
        public IHttpActionResult GetNextChapitre(int joueurId, int choixId)
        {
            JoueurDto joueur = JoueurService.GetJoueur(joueurId);
            var chapitreGet = ChapitreService.GetChapitre(joueur.Chapitres.Last().Id);
            var choix = chapitreGet.Choix.FirstOrDefault(elt => elt.Id == choixId);

            ChapitreDto prochainChapitre;
            ChapitreViewModel chapitreVM;

            // Si le choix demand� n'existe pas
            if (choix == null)
            {
                prochainChapitre = joueur.Chapitres.Last();
                chapitreVM = ConvertChapitreToVM(prochainChapitre);
                string messageErreur = "Une erreur est survenue le choix sp�cifi� n'existe pas !";
                return Json(new { joueur, chapitreVM, messageErreur });
            }

            // Si le choix existe on passe au chapitre suivant
            prochainChapitre = choix.ProchainChapitre;
            JoueurService.AddChapitre(joueur, prochainChapitre);
            chapitreVM = ConvertChapitreToVM(prochainChapitre);

            return Json(new { joueur, chapitreVM });
        }

        /// <summary>
        /// Deal with the player's attack during a fight against a monster
        /// </summary>
        /// <param name="joueurId">Id of the player</param>
        /// <param name="monstreViewModel">Monster against whom the player is fighting</param>
        /// <returns></returns>
        [Route("api/game/joueur/{joueurId}/combat/attaquer")]
        public IHttpActionResult PostAttaque(int joueurId, [FromBody]PersonnageViewModel monstreViewModel)
        {
            if (monstreViewModel == null)
            {
                throw new ArgumentNullException(nameof(monstreViewModel));
            }

            JoueurDto joueur = JoueurService.GetJoueur(joueurId);
            ChapitreDto chapitreVM = joueur.Chapitres.Last();
            MonstreDto monstre = ConvertMonstreToDto(monstreViewModel);
            EvenementDto evenement = JoueurService.GetEvenementEnCours(joueur);

            if (evenement == null)
            {
                string messageErreur = "Vous avez d�j� vaincu ce monstre ou effectu� cet �v�nement !";
                return Json(new { joueur, chapitreVM, messageErreur });
            }

            // COMBAT !!
            string message = Attaquer(joueur, monstre);
            var tuple = FinPhaseCombat(joueur, monstre, evenement, message);
            message = tuple.Item2;

            if (tuple.Item1)
            {
                JoueurService.UpdateJoueur(joueur);
                return Json(new { joueur, chapitreVM, evenement, message });
            }
            else
            {
                JoueurService.UpdateJoueur(joueur);
                if (joueur.EstMort())
                {
                    var GameOver = "Vous �tes mort !";
                    return Json(new { joueur, chapitreVM, evenement, monstre, message, GameOver });
                }
                return Json(new { joueur, chapitreVM, evenement, monstre, message });
            }
        }

        /// <summary>
        /// Deal with the use of an object by the player during a fight against a monster
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <param name="joueurId">The joueur id.</param>
        /// <param name="objetId">The object being used by the player</param>
        /// <param name="monstreViewModel">The monster against whom the player is fighting</param>
        /// <response code="200"></response>
        [Route("api/game/joueur/{joueurId}/combat/utiliser/objet/{objetId}")]
        public IHttpActionResult PostUtiliser(int joueurId, int objetId, [FromBody]PersonnageViewModel monstreViewModel)
        {
            if (monstreViewModel == null)
            {
                throw new ArgumentNullException(nameof(monstreViewModel));
            }

            JoueurDto joueur = JoueurService.GetJoueur(joueurId);
            ChapitreDto chapitreVM = joueur.Chapitres.Last();
            MonstreDto monstre = ConvertMonstreToDto(monstreViewModel);
            EvenementDto evenement = JoueurService.GetEvenementEnCours(joueur);
            ObjetDto objet = joueur.Objets.Find(o => o.Id == objetId);

            if (evenement == null)
            {
                string messageErreur = "Vous avez d�j� vaincu ce monstre ou effectu� cet �v�nement !";
                return Json(new { joueur, chapitreVM, messageErreur });
            }
            if (objet == null)
            {
                string messageErreur = "Vous ne poss�dez pas cet objet!";
                return Json(new { joueur, chapitreVM, evenement, monstre, messageErreur });
            }

            // V�rification conformit� de l'objet
            if (!joueur.Objets.Contains(objet))
            {
                string messageErreur = "Vous ne poss�dez pas ce sortil�ge !";
                return Json(new { joueur, chapitreVM, evenement, monstre, messageErreur });

            }

            // COMBAT !!
            string message = Utiliser(joueur, monstre, objet);
            var tuple = FinPhaseCombat(joueur, monstre, evenement, message);
            message = tuple.Item2;

            if (tuple.Item1)
            {
                JoueurService.UpdateJoueur(joueur);
                return Json(new { joueur, chapitreVM, evenement, message });

            }
            else
            {
                JoueurService.UpdateJoueur(joueur);
                if (joueur.EstMort())
                {
                    var GameOver = "Vous �tes mort !";
                    return Json(new { joueur, chapitreVM, evenement, monstre, message, GameOver });
                }
                return Json(new { joueur, chapitreVM, evenement, monstre, message });
            }
        }

        /// <summary>
        /// Deal with the use of a spell by the player during a fight against a monster
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <param name="joueurId">The joueur id.</param>
        /// <param name="sortilegeId">The spell used by the player</param>
        /// <param name="monstreViewModel">The monster against whom the player is fighting</param>
        /// <response code="200"></response>
        [Route("api/game/joueur/{joueurId}/combat/utiliser/sortilege/{sortilegeId}")]
        public IHttpActionResult PostLancer(int joueurId, int sortilegeId, [FromBody]PersonnageViewModel monstreViewModel)
        {
            if (monstreViewModel == null)
            {
                throw new ArgumentNullException(nameof(monstreViewModel));
            }

            JoueurDto joueur = JoueurService.GetJoueur(joueurId);
            ChapitreDto chapitreVM = joueur.Chapitres.Last();
            EvenementDto evenement = JoueurService.GetEvenementEnCours(joueur);
            MonstreDto monstre = ConvertMonstreToDto(monstreViewModel);
            SortilegeDto sortilege = joueur.Sortileges.Find(s => s.Id == sortilegeId);

            if (evenement == null)
            {
                string messageErreur = "Vous avez d�j� vaincu ce monstre ou effectu� cet �v�nement !";
                return Json(new { joueur, chapitreVM, messageErreur });
            }
            if (sortilege == null)
            {
                string messageErreur = "Vous ne connaissez pas ce sortil�ge !";
                return Json(new { joueur, chapitreVM, evenement, monstre, messageErreur });
            }

            // V�rification conformit� du sortil�ge
            if (sortilege.Type == 1)
            {
                string messageErreur = "Vous ne pouvez pas lancer de sortil�ges utilitaires en Combat !";
                return Json(new { joueur, chapitreVM, evenement, monstre, messageErreur });
            }
            if (!joueur.Sortileges.Contains(sortilege))
            {
                string messageErreur = "Vous ne poss�dez pas ce sortil�ge !";
                return Json(new { joueur, chapitreVM, evenement, monstre, messageErreur });

            }

            // COMBAT !!
            string message = Lancer(joueur, monstre, sortilege);

            var tuple = FinPhaseCombat(joueur, monstre, evenement, message);
            message = tuple.Item2;

            if (tuple.Item1)
            {
                JoueurService.UpdateJoueur(joueur);
                return Json(new { joueur, chapitreVM, evenement, message });

            }
            else
            {
                JoueurService.UpdateJoueur(joueur);
                if (joueur.EstMort())
                {
                    var GameOver = "Vous �tes mort";
                    return Json(new { joueur, chapitreVM, evenement, monstre, message, GameOver });
                }
                return Json(new { joueur, chapitreVM, evenement, monstre, message });
            }
        }

        /// <summary>
        /// Generate a message during a fight when the player attacks
        /// </summary>
        /// <param name="joueur">Player fighting</param>
        /// <param name="monstre">Monster against whom the player is fighting</param>
        /// <returns></returns>
        private string Attaquer(JoueurDto joueur, MonstreDto monstre)
        {
            string message = joueur.Attaquer(monstre);
            if (!monstre.EstMort())
            {
                message += monstre.ContreAttaquer(joueur);
            }

            return message;
        }

        /// <summary>
        /// Generate a message during a fight when the player uses an object
        /// </summary>
        /// <param name="joueur">Player fighting</param>
        /// <param name="monstre">Monster against whom the player is fighting</param>
        /// <param name="objet">Object used by the player</param>
        /// <returns></returns>
        private string Utiliser(JoueurDto joueur, MonstreDto monstre, ObjetDto objet)
        {
            string message = joueur.Utiliser(objet, monstre);
            if (!monstre.EstMort())
            {
                message += monstre.ContreAttaquer(joueur);
            }

            joueur.Objets.Remove(objet);

            return message;
        }

        /// <summary>
        /// Generate a message during a fight when the player uses a spell
        /// </summary>
        /// <param name="joueur">Player fighting</param>
        /// <param name="monstre">Monster against whom the player is fighting</param>
        /// <param name="sortilege">Spell used by the player</param>
        private string Lancer(JoueurDto joueur, MonstreDto monstre, SortilegeDto sortilege)
        {
            string message = joueur.Lancer(sortilege, monstre);
            if (!monstre.EstMort())
            {
                message += monstre.ContreAttaquer(joueur);
            }

            return message;
        }

        /// <summary>
        /// Generate a message at the end of an attack phase by the player
        /// </summary>
        /// <param name="joueur">Player fighting</param>
        /// <param name="monstre">Monster against whom the player is fighting</param>
        /// <param name="evenement">Event chosen by the player</param>
        /// <returns></returns>
        private Tuple<bool, string> FinPhaseCombat(JoueurDto joueur, MonstreDto monstre, EvenementDto evenement, string message)
        {
            bool victoire = false;
            // Si le joueur a vaincu le monstre
            if (monstre.EstMort())
            {
                victoire = true;
                JoueurService.AddEvenement(joueur, evenement, true);
                joueur.Evenements.Add(evenement);

                message = $"{message}{joueur.Nom.Trim()} a vaincu {monstre.Nom.Trim()} !\n";

                foreach (ObjetDto objet in monstre.Objets)
                {
                    AjouterObjetJoueur(joueur, objet);
                    message = $"{message}{joueur.Nom.Trim()} obtient {objet.Nom.Trim()} !\n";
                }

                foreach (SortilegeDto sortilege in monstre.Sortileges)
                {
                    joueur.Sortileges.Add(sortilege);
                    message = $"{message}{joueur.Nom.Trim()} apprend {sortilege.Nom.Trim()} !\n";
                }

            }
            // Si le monstre est toujours vivant
            else
            {
                if (joueur.EstMort())
                {
                    // Si le joueur n'a pas surv�cu au combat
                    if (joueur.EstMort())
                    {
                        message = $"{message}{joueur.Nom.Trim()} succombe � ses blessures...\n";
                    }
                    // Si le joueur est encore en vie
                    else
                    {
                        message = $"{message}{monstre.Nom.Trim()} pr�pare une attaque !\n";
                    }
                }
            }

            return new Tuple<bool, string>(victoire, message);
        }

        /// <summary>
        /// Convert a view model monster to a DTO monster
        /// </summary>
        /// <param name="chapitre">View model of the monster to convert to DTO</param>
        /// <returns>The monster converted to DTO</returns>
        private MonstreDto ConvertMonstreToDto(PersonnageViewModel viewModel)
        {
            return new MonstreDto
            {
                Attaque = viewModel.Attaque,
                Defense = viewModel.Defense,
                Pv = viewModel.Pv,
                Nom = viewModel.Nom,
                Objets = PersonnageService.GetObjets(viewModel.ObjetIds),
                Sortileges = PersonnageService.GetSortileges(viewModel.SortilegeIds),
            };
        }

        /// <summary>
        /// Convert a view model spell to a DTO spell
        /// </summary>
        /// <param name="chapitre">View model of the spell to convert to DTO</param>
        /// <returns>The spell converted to DTO</returns>
        private SortilegeDeCombatDto ConvertSortilegeToDto(SortilegeViewModel viewModel)
        {
            if (viewModel.Type == 2)
            {
                return new SortilegeDefensifDto
                {
                    Id = viewModel.Id,
                    Nom = viewModel.Nom,
                    Description = viewModel.Description,
                    Puissance = viewModel.Puissance,
                    Type = viewModel.Type,
                };
            }
            else
            {
                return new SortilegeOffensifDto
                {
                    Id = viewModel.Id,
                    Nom = viewModel.Nom,
                    Description = viewModel.Description,
                    Puissance = viewModel.Puissance,
                    Type = viewModel.Type,
                };
            }
        }

        /// <summary>
        /// Adds an Item to the Item list of the Player
        /// </summary>
        /// <param name="joueur">The Player</param>
        /// <param name="objet">The Item to Add</param>
        private void AjouterObjetJoueur(JoueurDto joueur, ObjetDto objet)
        {
            if (objet.Type == "Equipement")
            {
                switch (objet.TypeEffet)
                {
                    case TypeEffetDto.ATTAQUE:
                        joueur.Attaque += objet.Puissance;
                        break;
                    case TypeEffetDto.DEFENSE:
                        joueur.Defense += objet.Puissance;
                        break;
                }
                JoueurService.AddEquipement(joueur, objet);
            }
            else if (objet.Type == "Consommable")
            {
                joueur.Objets.Add(objet);
            }
        }

        /// <summary>
        /// Convert a DTO chapter to a view model chapter
        /// </summary>
        /// <param name="chapitre">DTO of the chapter to convert to view model</param>
        /// <returns>The chapter converted to view model</returns>
        private ChapitreViewModel ConvertChapitreToVM(ChapitreDto chapitre)
        {
            var chapitreVM = new ChapitreViewModel
            {
                Id = chapitre.Id,
                Numero = chapitre.Numero,
                Histoire = chapitre.Histoire,
                Choix = chapitre.Choix.Select(c => new ChoixLittleViewModel()
                {
                    Id = c.Id,
                    Contenu = c.Contenu,
                }).ToList(),
                ChoixPrec = chapitre.ChoixPrec.Select(c => new ChoixLittleViewModel()
                {
                    Id = c.Id,
                    Contenu = c.Contenu,
                }).ToList(),
                Evenements = chapitre.Evenements.Select(e => new EvenementLittleViewModel()
                {
                    Id = e.Id,
                    Description = e.Description,
                    Type = e.Cliquable.ToString().Replace("DTO.", "").Replace("Dto", ""),
                }).ToList(),
            };

            return chapitreVM;
        }
    }
}
