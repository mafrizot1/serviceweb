﻿using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;

namespace API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuration et services API Web
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());

            // Itinéraires de l'API Web
            config.MapHttpAttributeRoutes();

            // Cross Origin Resource Sharing
            var cors = new EnableCorsAttribute("http://localhost:4200", "*", "*");
            config.EnableCors(cors);

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
