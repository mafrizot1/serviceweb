using System.Web.Http;
using WebActivatorEx;
using API;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace API
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "API");
                    })
                .EnableSwaggerUi(c =>
                    {
                    });
        }
    }
}
